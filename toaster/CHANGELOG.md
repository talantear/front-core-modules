# @shared/toaster

## 2.0.0

### Minor Changes

- Update packages

### Patch Changes

- Updated dependencies
  - @shared/message@1.1.0
  - @shared/base-client@1.1.0
  - @shared/types@1.1.0

## 1.0.2

### Patch Changes

- Fixed build

## 1.0.1

### Patch Changes

- Updaated exports
- Updated dependencies
  - @shared/message@1.0.2
  - @shared/base-client@1.0.1
