import { inject, injectable } from 'inversify';
import { StreamHandler } from '@shared/stream/service';
import GqlClient from '@shared/gql-client/client';
import {
  InfoSectionCountDocument,
  InfoSectionCountQuery,
  InfoSectionCountQueryVariables,
  InfoSectionCreateDocument,
  InfoSectionCreateMutation,
  InfoSectionCreateMutationVariables,
  InfoSectionDeleteDocument,
  InfoSectionDeleteMutation,
  InfoSectionDeleteMutationVariables,
  InfoSectionGetByIdDocument,
  InfoSectionGetByIdQuery,
  InfoSectionGetByIdQueryVariables,
  InfoSectionListDocument,
  InfoSectionListPlainDocument, InfoSectionListPlainQuery, InfoSectionListPlainQueryVariables,
  InfoSectionListQuery,
  InfoSectionListQueryVariables,
  InfoSectionUpdateDocument,
  InfoSectionUpdateMutation,
  InfoSectionUpdateMutationVariables,
  ListSectionReq,
} from '../../../gql/schemas/info-gql';
import { EMPTY, from, map, Observable } from 'rxjs';
import { ObjectWithId } from '@shared/types/dist/src/string';
import { mergeMap } from 'rxjs/operators';
import { ApiGqlClientKey } from '@shared/gql-client/constants';
import { IInfoSectionService } from '../types';
import { IListSection, ISectionModel } from '../../dto';
import { ListOptions } from '@shared/stream/types';

@injectable()
class InfoSectionGqlService extends StreamHandler<ISectionModel> implements IInfoSectionService {
  private api: GqlClient;

  public constructor(
    @inject(ApiGqlClientKey) api: GqlClient,
  ) {
    super();
    this.add("default", this.streamDefault)
    this.add("plain", this.plainStream)
    this.api = api
  }

  count(): Observable<number> {
    throw new Error('Method not implemented.');
  }

  private streamDefault = {
    stream: (opts: ListOptions, method?: string, query?: ListSectionReq): Observable<ISectionModel & ObjectWithId> => {
      const input: ListSectionReq = {
        adminShow: query?.adminShow,
        slug: query?.slug as string,
        opts
      }

      return this.api.query$<InfoSectionListQuery, InfoSectionListQueryVariables>(
        InfoSectionListDocument,
        { input },
        {
          noCache: true,
        },
      ).pipe(mergeMap(res => from(res.infoSectionList as ISectionModel[])))
    },
    loadCount: (): Observable<number> => {
      return this.api.query$<InfoSectionCountQuery, InfoSectionCountQueryVariables>(
        InfoSectionCountDocument,
      ).pipe(
        map(res => res.infoSectionCount as number),
      )
    },
  }

  private plainStream = {
    stream: (opts: ListOptions, method?: string, query?: object): Observable<ISectionModel & ObjectWithId> => {
      return this.api.query$<InfoSectionListPlainQuery, InfoSectionListPlainQueryVariables>(
        InfoSectionListPlainDocument,
        { input: opts },
        {
          noCache: true,
        },
      ).pipe(mergeMap(res => from(res.infoSectionListPlain as ISectionModel[])));
    },
    loadCount: (): Observable<number> => this.streamDefault.loadCount(),
  }

  plainList(opts: ListOptions): Observable<ISectionModel> {
    return EMPTY
  }

  list(value: IListSection): Observable<ISectionModel> {
    return EMPTY
  }

  create(input: ISectionModel): Observable<ISectionModel> {
    return this.api.mutation$<InfoSectionCreateMutation, InfoSectionCreateMutationVariables>(
      InfoSectionCreateDocument,
      { input },
    ).pipe(map(res => res.infoSectionCreate as ISectionModel));
  }

  update(input: ISectionModel): Observable<ISectionModel> {
    return this.api.mutation$<InfoSectionUpdateMutation, InfoSectionUpdateMutationVariables>(
      InfoSectionUpdateDocument,
      { input },
    ).pipe(map(res => res.infoSectionUpdate as ISectionModel));
  }

  getById(id: number): Observable<ISectionModel> {
    return this.api.query$<InfoSectionGetByIdQuery, InfoSectionGetByIdQueryVariables>(
      InfoSectionGetByIdDocument,
      { input: id },
    ).pipe(map(res => res.infoSectionGetById as ISectionModel));
  }

  delete(id: number): Observable<void> {
    return this.api.mutation$<InfoSectionDeleteMutation, InfoSectionDeleteMutationVariables>(
      InfoSectionDeleteDocument,
      { input: id },
    ).pipe(map(res => res.infoSectionDelete));
  }
}

export default InfoSectionGqlService;