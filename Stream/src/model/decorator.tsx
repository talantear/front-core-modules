import 'reflect-metadata'

interface FieldMetadata {
  key: string
}

export function ObservableField(): PropertyDecorator {
  return (target, key) => {
    const fields = Reflect.getOwnMetadata('fields', target) || {};
    if (!fields[key]) {
      fields[key] = key
    }

    Reflect.defineMetadata('fields', fields, target)
  }
}


export abstract class ObjectObserverImp {
  get fields() {
    let fields: { [key: string]: FieldMetadata } = {}
    let target = Object.getPrototypeOf(this);
    while (target != Object.prototype) {
      let childFields = Reflect.getOwnMetadata('fields', target) || {};
      if (childFields) {
        fields = {
          ...fields,
          ...childFields,
        };
      }

      target = Object.getPrototypeOf(target);
    }
    return fields;
  }
}
