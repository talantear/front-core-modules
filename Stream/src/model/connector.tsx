import React, { useEffect } from 'react';
import { observer } from 'mobx-react-lite';

export interface Loadable {
  loading: boolean
}

type Obj = { [key: string]: any };

const cObserver = <T extends object>(BaseComponent: React.FunctionComponent<T & Loadable>): React.FunctionComponent<any> => {
  return observer(({ vm, ...props }) => {
    const loading = vm.loading.getItem();

    const extra: { [key: string]: any } = {};
    for (const key of Object.keys(vm.fields)) {
      extra[key] = vm[key].getItem()
    }

    return (
      <BaseComponent
        {...vm}
        {...props}
        {...extra}
        loading={loading}
      />
    )
  });
}

export default cObserver;