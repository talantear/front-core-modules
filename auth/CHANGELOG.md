# @shared/auth

## 2.0.0

### Minor Changes

- Update packages

### Patch Changes

- Updated dependencies
  - @shared/grpc-client@1.1.0
  - @shared/stream@1.1.0
  - @shared/base-client@1.1.0
  - @shared/cookie-storage@1.1.0
  - @shared/tools@1.1.0
  - @shared/types@1.1.0
  - @shared/app@1.1.0

## 1.0.20

### Patch Changes

- Fixed base user UC

## 1.0.19

### Patch Changes

- Updated user interface

## 1.0.18

### Patch Changes

- Updated verify email method

## 1.0.17

### Patch Changes

- added optional injection to useSession hook

## 1.0.16

### Patch Changes

- Updated application grpc service

## 1.0.15

### Patch Changes

- Updated user service

## 1.0.14

### Patch Changes

- Updated package json

## 1.0.13

### Patch Changes

- Updated package json

## 1.0.12

### Patch Changes

- Updated package json

## 1.0.11

### Patch Changes

- Updated package json

## 1.0.10

### Patch Changes

- Updated exported modules
- Updated dependencies
  - @shared/grpc-client@1.0.3
  - @shared/stream@1.0.3
  - @shared/types@1.0.2

## 1.0.9

### Patch Changes

- Updated build
- Updated dependencies
  - @shared/grpc-client@1.0.2
  - @shared/app@1.0.2

## 1.0.8

### Patch Changes

- Updated build

## 1.0.7

### Patch Changes

- Cjs test

## 1.0.6

### Patch Changes

- Updaated exports
- Updated dependencies
  - @shared/cache-repo@1.0.1
  - @shared/grpc-client@1.0.1
  - @shared/stream@1.0.1
  - @shared/base-client@1.0.1
  - @shared/cookie-storage@1.0.1
  - @shared/tools@1.0.1
  - @shared/types@1.0.1
  - @shared/app@1.0.1

## 1.0.5

### Patch Changes

- Updated name of di key

## 1.0.4

### Patch Changes

- Updated package json

## 1.0.3

### Patch Changes

- Updated package json

## 1.0.2

### Patch Changes

- Updated build

## 1.0.1

### Patch Changes

- Updated paths
