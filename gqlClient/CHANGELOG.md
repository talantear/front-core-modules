# @shared/gql-client

## 1.1.0

### Minor Changes

- Update packages

### Patch Changes

- Updated dependencies
  - @shared/base-client@1.1.0
  - @shared/cookie-storage@1.1.0
  - @shared/app@1.1.0

## 1.0.1

### Patch Changes

- Updaated exports
- Updated dependencies
  - @shared/base-client@1.0.1
  - @shared/cookie-storage@1.0.1
  - @shared/app@1.0.1
