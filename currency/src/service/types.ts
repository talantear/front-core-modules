import { Observable } from 'rxjs';

export interface ICurrencyService {
  getRate(currency: string): Observable<number>
  subscriber(currency: string): Observable<number>
}