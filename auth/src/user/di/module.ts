import { AsyncContainerModule, interfaces } from 'inversify';
import BaseUserUsecase, { AdminUserUsecase, IAdminUserUsecase, IBaseUserUsecase } from '../usecase';
import { IAdminUserService, IUserService } from '../service/types';
import { adminUserServiceDiKey, userServiceDiKey } from '../constants';

export const UserModule = <T extends interfaces.Newable<IUserService>>(service: T) => new AsyncContainerModule(async (bind: interfaces.Bind, unbind: interfaces.Unbind) => {
  bind<IUserService>(userServiceDiKey).to(service);
  bind<IBaseUserUsecase>(BaseUserUsecase.diKey).to(BaseUserUsecase).inSingletonScope();
});

export const UserAdminModule = <USER extends interfaces.Newable<IUserService>, ADMIN extends interfaces.Newable<IAdminUserService>>(user: USER, admin: ADMIN) => new AsyncContainerModule(async (bind: interfaces.Bind, unbind: interfaces.Unbind) => {
  bind<IAdminUserService>(adminUserServiceDiKey).to(admin);
  bind<IAdminUserUsecase>(AdminUserUsecase.diKey).to(AdminUserUsecase).inSingletonScope();
  bind<IUserService>(userServiceDiKey).to(user);
  bind<IBaseUserUsecase>(BaseUserUsecase.diKey).to(BaseUserUsecase).inSingletonScope();
});
