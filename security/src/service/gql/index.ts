import { map, Observable } from 'rxjs';
import { inject, injectable } from 'inversify';
import { GqlClient } from '@shared/gql-client/client';
import { ApiGqlClientKey } from '@shared/gql-client/constants';
import {
  SecurityCreateDocument,
  SecurityCreateInput,
  SecurityCreateMutation,
  SecurityCreateMutationVariables,
} from '../../gql/schemas/security-gql';
import { ISecurityService } from '../types';

@injectable()
class SecurityGqlService implements ISecurityService {
  private api: GqlClient;

  constructor(
    @inject(ApiGqlClientKey) api: GqlClient,
  ) {
    this.api = api;
  }

  create(input: SecurityCreateInput): Observable<void> {
    return this.api.mutation$<SecurityCreateMutation, SecurityCreateMutationVariables>(
      SecurityCreateDocument,
      { input },
      {
        noCache: true,
      }
    ).pipe(map(() => {}));
  }
}

export default SecurityGqlService;