import { useState } from 'react';
import { useInjection } from 'inversify-react';
import { useLoadableVM } from '@shared/stream/hooks';
import LoadingUsecase from '../usecase';
import LoadingVM from '../LoadingVM';

const useLoading = (context: string, defaultValue?: boolean): [boolean] => {
  const [loading, setLoading] = useState(!!defaultValue)
  const loadingUC = useInjection<LoadingUsecase>(LoadingUsecase.diKey)

  const [vm] = useState(new LoadingVM(loadingUC, context, {
    defaultValue,
    changeHandler: (value) => {
      setLoading(value)
    },
  }))

  useLoadableVM(vm)
  return [loading]
}

export default useLoading;