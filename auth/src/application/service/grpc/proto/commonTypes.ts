/* eslint-disable */
import { messageTypeRegistry } from "./typeRegistry";
import Long from "long";
import _m0 from "protobufjs/minimal";

export const protobufPackage = "aquix.types";

export interface IdRequest {
  $type: "aquix.types.IdRequest";
  id: number;
}

export interface Id64Request {
  $type: "aquix.types.Id64Request";
  id: number;
}

export interface StringRequest {
  $type: "aquix.types.StringRequest";
  value: string;
}

export interface BoolValue {
  $type: "aquix.types.BoolValue";
  value: boolean;
}

export interface Int64Value {
  $type: "aquix.types.Int64Value";
  value: number;
}

export interface UInt64Value {
  $type: "aquix.types.UInt64Value";
  value: number;
}

export interface Int32Value {
  $type: "aquix.types.Int32Value";
  value: number;
}

export interface UInt32Value {
  $type: "aquix.types.UInt32Value";
  value: number;
}

const baseIdRequest: object = { $type: "aquix.types.IdRequest", id: 0 };

export const IdRequest = {
  $type: "aquix.types.IdRequest" as const,

  encode(
    message: IdRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.id !== 0) {
      writer.uint32(8).uint32(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): IdRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseIdRequest } as IdRequest;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.uint32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): IdRequest {
    const message = { ...baseIdRequest } as IdRequest;
    message.id =
      object.id !== undefined && object.id !== null ? Number(object.id) : 0;
    return message;
  },

  toJSON(message: IdRequest): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial(object: DeepPartial<IdRequest>): IdRequest {
    const message = { ...baseIdRequest } as IdRequest;
    message.id = object.id ?? 0;
    return message;
  },
};

messageTypeRegistry.set(IdRequest.$type, IdRequest);

const baseId64Request: object = { $type: "aquix.types.Id64Request", id: 0 };

export const Id64Request = {
  $type: "aquix.types.Id64Request" as const,

  encode(
    message: Id64Request,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.id !== 0) {
      writer.uint32(8).uint64(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): Id64Request {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseId64Request } as Id64Request;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = longToNumber(reader.uint64() as Long);
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Id64Request {
    const message = { ...baseId64Request } as Id64Request;
    message.id =
      object.id !== undefined && object.id !== null ? Number(object.id) : 0;
    return message;
  },

  toJSON(message: Id64Request): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial(object: DeepPartial<Id64Request>): Id64Request {
    const message = { ...baseId64Request } as Id64Request;
    message.id = object.id ?? 0;
    return message;
  },
};

messageTypeRegistry.set(Id64Request.$type, Id64Request);

const baseStringRequest: object = {
  $type: "aquix.types.StringRequest",
  value: "",
};

export const StringRequest = {
  $type: "aquix.types.StringRequest" as const,

  encode(
    message: StringRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.value !== "") {
      writer.uint32(10).string(message.value);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): StringRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseStringRequest } as StringRequest;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.value = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): StringRequest {
    const message = { ...baseStringRequest } as StringRequest;
    message.value =
      object.value !== undefined && object.value !== null
        ? String(object.value)
        : "";
    return message;
  },

  toJSON(message: StringRequest): unknown {
    const obj: any = {};
    message.value !== undefined && (obj.value = message.value);
    return obj;
  },

  fromPartial(object: DeepPartial<StringRequest>): StringRequest {
    const message = { ...baseStringRequest } as StringRequest;
    message.value = object.value ?? "";
    return message;
  },
};

messageTypeRegistry.set(StringRequest.$type, StringRequest);

const baseBoolValue: object = { $type: "aquix.types.BoolValue", value: false };

export const BoolValue = {
  $type: "aquix.types.BoolValue" as const,

  encode(
    message: BoolValue,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.value === true) {
      writer.uint32(8).bool(message.value);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): BoolValue {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseBoolValue } as BoolValue;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.value = reader.bool();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): BoolValue {
    const message = { ...baseBoolValue } as BoolValue;
    message.value =
      object.value !== undefined && object.value !== null
        ? Boolean(object.value)
        : false;
    return message;
  },

  toJSON(message: BoolValue): unknown {
    const obj: any = {};
    message.value !== undefined && (obj.value = message.value);
    return obj;
  },

  fromPartial(object: DeepPartial<BoolValue>): BoolValue {
    const message = { ...baseBoolValue } as BoolValue;
    message.value = object.value ?? false;
    return message;
  },
};

messageTypeRegistry.set(BoolValue.$type, BoolValue);

const baseInt64Value: object = { $type: "aquix.types.Int64Value", value: 0 };

export const Int64Value = {
  $type: "aquix.types.Int64Value" as const,

  encode(
    message: Int64Value,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.value !== 0) {
      writer.uint32(8).int64(message.value);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): Int64Value {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseInt64Value } as Int64Value;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.value = longToNumber(reader.int64() as Long);
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Int64Value {
    const message = { ...baseInt64Value } as Int64Value;
    message.value =
      object.value !== undefined && object.value !== null
        ? Number(object.value)
        : 0;
    return message;
  },

  toJSON(message: Int64Value): unknown {
    const obj: any = {};
    message.value !== undefined && (obj.value = message.value);
    return obj;
  },

  fromPartial(object: DeepPartial<Int64Value>): Int64Value {
    const message = { ...baseInt64Value } as Int64Value;
    message.value = object.value ?? 0;
    return message;
  },
};

messageTypeRegistry.set(Int64Value.$type, Int64Value);

const baseUInt64Value: object = { $type: "aquix.types.UInt64Value", value: 0 };

export const UInt64Value = {
  $type: "aquix.types.UInt64Value" as const,

  encode(
    message: UInt64Value,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.value !== 0) {
      writer.uint32(8).uint64(message.value);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): UInt64Value {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseUInt64Value } as UInt64Value;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.value = longToNumber(reader.uint64() as Long);
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): UInt64Value {
    const message = { ...baseUInt64Value } as UInt64Value;
    message.value =
      object.value !== undefined && object.value !== null
        ? Number(object.value)
        : 0;
    return message;
  },

  toJSON(message: UInt64Value): unknown {
    const obj: any = {};
    message.value !== undefined && (obj.value = message.value);
    return obj;
  },

  fromPartial(object: DeepPartial<UInt64Value>): UInt64Value {
    const message = { ...baseUInt64Value } as UInt64Value;
    message.value = object.value ?? 0;
    return message;
  },
};

messageTypeRegistry.set(UInt64Value.$type, UInt64Value);

const baseInt32Value: object = { $type: "aquix.types.Int32Value", value: 0 };

export const Int32Value = {
  $type: "aquix.types.Int32Value" as const,

  encode(
    message: Int32Value,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.value !== 0) {
      writer.uint32(8).int32(message.value);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): Int32Value {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseInt32Value } as Int32Value;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.value = reader.int32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Int32Value {
    const message = { ...baseInt32Value } as Int32Value;
    message.value =
      object.value !== undefined && object.value !== null
        ? Number(object.value)
        : 0;
    return message;
  },

  toJSON(message: Int32Value): unknown {
    const obj: any = {};
    message.value !== undefined && (obj.value = message.value);
    return obj;
  },

  fromPartial(object: DeepPartial<Int32Value>): Int32Value {
    const message = { ...baseInt32Value } as Int32Value;
    message.value = object.value ?? 0;
    return message;
  },
};

messageTypeRegistry.set(Int32Value.$type, Int32Value);

const baseUInt32Value: object = { $type: "aquix.types.UInt32Value", value: 0 };

export const UInt32Value = {
  $type: "aquix.types.UInt32Value" as const,

  encode(
    message: UInt32Value,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.value !== 0) {
      writer.uint32(8).uint32(message.value);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): UInt32Value {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseUInt32Value } as UInt32Value;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.value = reader.uint32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): UInt32Value {
    const message = { ...baseUInt32Value } as UInt32Value;
    message.value =
      object.value !== undefined && object.value !== null
        ? Number(object.value)
        : 0;
    return message;
  },

  toJSON(message: UInt32Value): unknown {
    const obj: any = {};
    message.value !== undefined && (obj.value = message.value);
    return obj;
  },

  fromPartial(object: DeepPartial<UInt32Value>): UInt32Value {
    const message = { ...baseUInt32Value } as UInt32Value;
    message.value = object.value ?? 0;
    return message;
  },
};

messageTypeRegistry.set(UInt32Value.$type, UInt32Value);

declare var self: any | undefined;
declare var window: any | undefined;
declare var global: any | undefined;
var globalThis: any = (() => {
  if (typeof globalThis !== "undefined") return globalThis;
  if (typeof self !== "undefined") return self;
  if (typeof window !== "undefined") return window;
  if (typeof global !== "undefined") return global;
  throw "Unable to locate global object";
})();

type Builtin =
  | Date
  | Function
  | Uint8Array
  | string
  | number
  | boolean
  | undefined;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? { [K in Exclude<keyof T, "$type">]?: DeepPartial<T[K]> }
  : Partial<T>;

function longToNumber(long: Long): number {
  if (long.gt(Number.MAX_SAFE_INTEGER)) {
    throw new globalThis.Error("Value is larger than Number.MAX_SAFE_INTEGER");
  }
  return long.toNumber();
}

if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
