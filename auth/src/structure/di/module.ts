import { AsyncContainerModule, interfaces } from 'inversify';
import StructUsecase, { IStructUsecase } from '../usecase';
import { IStructService } from '../service/types';
import { structServiceDiKey } from '../constants';

export const StructureInfoModule = <T extends interfaces.Newable<IStructService>>(service: T) => new AsyncContainerModule(async (bind: interfaces.Bind, unbind: interfaces.Unbind) => {
  bind<IStructService>(structServiceDiKey).to(service);
  bind<IStructUsecase>(StructUsecase.diKey).to(StructUsecase).inSingletonScope();
});
