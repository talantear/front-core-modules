import User from '../models/user';
import { Observable } from 'rxjs';
import { IExternalService } from '@shared/stream/types';
import { IListOptions } from '@shared/types/dto';
import { IUserFilterModel } from '../dto';

export interface IUserService<UserType extends User = User> {
  getByAlias(alias: string): Observable<UserType>
  update(value: UserType): Observable<string>
  verifyEmail: (token: string, app: string) => Observable<boolean>,
  getById: (id: number) => Observable<UserType | undefined>,
}

export interface IAdminUserService<UserType extends User = User> extends IUserService<UserType>, IExternalService<UserType> {
  updateByAdmin: (req: UserType) => Observable<void>,
  create: (req: UserType) => Observable<void>,
  getFullById: (id: number) => Observable<UserType | undefined>,
  list(opts: IListOptions, method: string, filter?: IUserFilterModel): Observable<UserType>
}