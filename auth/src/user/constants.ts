export const authUsersListModelName = Symbol.for('AuthUsersListModelName');
export const authUsersItemModelName = Symbol.for('AuthUsersItemModelName');
export const userServiceDiKey = Symbol.for('UserServiceKey');
export const adminUserServiceDiKey = Symbol.for('AdminUserServiceKey');