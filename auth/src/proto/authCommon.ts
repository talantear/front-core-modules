/* eslint-disable */
import { messageTypeRegistry } from "./typeRegistry";
import Long from "long";
import _m0 from "protobufjs/minimal";

export const protobufPackage = "authMs";

/** Represents short user model. */
export interface ShortUserModel {
  $type: "authMs.ShortUserModel";
  id: number;
  /** / Date of creation */
  email: string;
  /** / Is user blocked */
  blocked: boolean;
  /** / Is user deleted */
  deleted: boolean;
  phone?: string | undefined;
  firstName?: string | undefined;
  middleName?: string | undefined;
  surname?: string | undefined;
  parentId?: number | undefined;
  /** / User`s alias */
  alias?: string | undefined;
  lang: string;
}

/** Represents user model. */
export interface UserModel {
  $type: "authMs.UserModel";
  id: number;
  /** / Date of creation */
  createdAt: number;
  /** / User`s alias */
  alias?: string | undefined;
  parentId?: number | undefined;
  email: string;
  /** / Is user verified */
  verified: boolean;
  /** / Is user blocked */
  blocked: boolean;
  /** / Is user deleted */
  deleted: boolean;
  firstName?: string | undefined;
  middleName?: string | undefined;
  surname?: string | undefined;
  phone?: string | undefined;
  lang: string;
  /** / User`s parents struct */
  path: string;
  /** / User`s role */
  role: string;
  organizationId: number;
}

const baseShortUserModel: object = {
  $type: "authMs.ShortUserModel",
  id: 0,
  email: "",
  blocked: false,
  deleted: false,
  lang: "",
};

export const ShortUserModel = {
  $type: "authMs.ShortUserModel" as const,

  encode(
    message: ShortUserModel,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.id !== 0) {
      writer.uint32(8).uint32(message.id);
    }
    if (message.email !== "") {
      writer.uint32(26).string(message.email);
    }
    if (message.blocked === true) {
      writer.uint32(40).bool(message.blocked);
    }
    if (message.deleted === true) {
      writer.uint32(48).bool(message.deleted);
    }
    if (message.phone !== undefined) {
      writer.uint32(58).string(message.phone);
    }
    if (message.firstName !== undefined) {
      writer.uint32(66).string(message.firstName);
    }
    if (message.middleName !== undefined) {
      writer.uint32(74).string(message.middleName);
    }
    if (message.surname !== undefined) {
      writer.uint32(82).string(message.surname);
    }
    if (message.parentId !== undefined) {
      writer.uint32(88).uint32(message.parentId);
    }
    if (message.alias !== undefined) {
      writer.uint32(98).string(message.alias);
    }
    if (message.lang !== "") {
      writer.uint32(106).string(message.lang);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ShortUserModel {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseShortUserModel } as ShortUserModel;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.uint32();
          break;
        case 3:
          message.email = reader.string();
          break;
        case 5:
          message.blocked = reader.bool();
          break;
        case 6:
          message.deleted = reader.bool();
          break;
        case 7:
          message.phone = reader.string();
          break;
        case 8:
          message.firstName = reader.string();
          break;
        case 9:
          message.middleName = reader.string();
          break;
        case 10:
          message.surname = reader.string();
          break;
        case 11:
          message.parentId = reader.uint32();
          break;
        case 12:
          message.alias = reader.string();
          break;
        case 13:
          message.lang = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ShortUserModel {
    const message = { ...baseShortUserModel } as ShortUserModel;
    message.id =
      object.id !== undefined && object.id !== null ? Number(object.id) : 0;
    message.email =
      object.email !== undefined && object.email !== null
        ? String(object.email)
        : "";
    message.blocked =
      object.blocked !== undefined && object.blocked !== null
        ? Boolean(object.blocked)
        : false;
    message.deleted =
      object.deleted !== undefined && object.deleted !== null
        ? Boolean(object.deleted)
        : false;
    message.phone =
      object.phone !== undefined && object.phone !== null
        ? String(object.phone)
        : undefined;
    message.firstName =
      object.firstName !== undefined && object.firstName !== null
        ? String(object.firstName)
        : undefined;
    message.middleName =
      object.middleName !== undefined && object.middleName !== null
        ? String(object.middleName)
        : undefined;
    message.surname =
      object.surname !== undefined && object.surname !== null
        ? String(object.surname)
        : undefined;
    message.parentId =
      object.parentId !== undefined && object.parentId !== null
        ? Number(object.parentId)
        : undefined;
    message.alias =
      object.alias !== undefined && object.alias !== null
        ? String(object.alias)
        : undefined;
    message.lang =
      object.lang !== undefined && object.lang !== null
        ? String(object.lang)
        : "";
    return message;
  },

  toJSON(message: ShortUserModel): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    message.email !== undefined && (obj.email = message.email);
    message.blocked !== undefined && (obj.blocked = message.blocked);
    message.deleted !== undefined && (obj.deleted = message.deleted);
    message.phone !== undefined && (obj.phone = message.phone);
    message.firstName !== undefined && (obj.firstName = message.firstName);
    message.middleName !== undefined && (obj.middleName = message.middleName);
    message.surname !== undefined && (obj.surname = message.surname);
    message.parentId !== undefined && (obj.parentId = message.parentId);
    message.alias !== undefined && (obj.alias = message.alias);
    message.lang !== undefined && (obj.lang = message.lang);
    return obj;
  },

  fromPartial(object: DeepPartial<ShortUserModel>): ShortUserModel {
    const message = { ...baseShortUserModel } as ShortUserModel;
    message.id = object.id ?? 0;
    message.email = object.email ?? "";
    message.blocked = object.blocked ?? false;
    message.deleted = object.deleted ?? false;
    message.phone = object.phone ?? undefined;
    message.firstName = object.firstName ?? undefined;
    message.middleName = object.middleName ?? undefined;
    message.surname = object.surname ?? undefined;
    message.parentId = object.parentId ?? undefined;
    message.alias = object.alias ?? undefined;
    message.lang = object.lang ?? "";
    return message;
  },
};

messageTypeRegistry.set(ShortUserModel.$type, ShortUserModel);

const baseUserModel: object = {
  $type: "authMs.UserModel",
  id: 0,
  createdAt: 0,
  email: "",
  verified: false,
  blocked: false,
  deleted: false,
  lang: "",
  path: "",
  role: "",
  organizationId: 0,
};

export const UserModel = {
  $type: "authMs.UserModel" as const,

  encode(
    message: UserModel,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.id !== 0) {
      writer.uint32(8).uint32(message.id);
    }
    if (message.createdAt !== 0) {
      writer.uint32(16).int64(message.createdAt);
    }
    if (message.alias !== undefined) {
      writer.uint32(26).string(message.alias);
    }
    if (message.parentId !== undefined) {
      writer.uint32(32).uint32(message.parentId);
    }
    if (message.email !== "") {
      writer.uint32(42).string(message.email);
    }
    if (message.verified === true) {
      writer.uint32(48).bool(message.verified);
    }
    if (message.blocked === true) {
      writer.uint32(56).bool(message.blocked);
    }
    if (message.deleted === true) {
      writer.uint32(64).bool(message.deleted);
    }
    if (message.firstName !== undefined) {
      writer.uint32(74).string(message.firstName);
    }
    if (message.middleName !== undefined) {
      writer.uint32(82).string(message.middleName);
    }
    if (message.surname !== undefined) {
      writer.uint32(90).string(message.surname);
    }
    if (message.phone !== undefined) {
      writer.uint32(98).string(message.phone);
    }
    if (message.lang !== "") {
      writer.uint32(106).string(message.lang);
    }
    if (message.path !== "") {
      writer.uint32(114).string(message.path);
    }
    if (message.role !== "") {
      writer.uint32(122).string(message.role);
    }
    if (message.organizationId !== 0) {
      writer.uint32(128).uint32(message.organizationId);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): UserModel {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseUserModel } as UserModel;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.uint32();
          break;
        case 2:
          message.createdAt = longToNumber(reader.int64() as Long);
          break;
        case 3:
          message.alias = reader.string();
          break;
        case 4:
          message.parentId = reader.uint32();
          break;
        case 5:
          message.email = reader.string();
          break;
        case 6:
          message.verified = reader.bool();
          break;
        case 7:
          message.blocked = reader.bool();
          break;
        case 8:
          message.deleted = reader.bool();
          break;
        case 9:
          message.firstName = reader.string();
          break;
        case 10:
          message.middleName = reader.string();
          break;
        case 11:
          message.surname = reader.string();
          break;
        case 12:
          message.phone = reader.string();
          break;
        case 13:
          message.lang = reader.string();
          break;
        case 14:
          message.path = reader.string();
          break;
        case 15:
          message.role = reader.string();
          break;
        case 16:
          message.organizationId = reader.uint32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): UserModel {
    const message = { ...baseUserModel } as UserModel;
    message.id =
      object.id !== undefined && object.id !== null ? Number(object.id) : 0;
    message.createdAt =
      object.createdAt !== undefined && object.createdAt !== null
        ? Number(object.createdAt)
        : 0;
    message.alias =
      object.alias !== undefined && object.alias !== null
        ? String(object.alias)
        : undefined;
    message.parentId =
      object.parentId !== undefined && object.parentId !== null
        ? Number(object.parentId)
        : undefined;
    message.email =
      object.email !== undefined && object.email !== null
        ? String(object.email)
        : "";
    message.verified =
      object.verified !== undefined && object.verified !== null
        ? Boolean(object.verified)
        : false;
    message.blocked =
      object.blocked !== undefined && object.blocked !== null
        ? Boolean(object.blocked)
        : false;
    message.deleted =
      object.deleted !== undefined && object.deleted !== null
        ? Boolean(object.deleted)
        : false;
    message.firstName =
      object.firstName !== undefined && object.firstName !== null
        ? String(object.firstName)
        : undefined;
    message.middleName =
      object.middleName !== undefined && object.middleName !== null
        ? String(object.middleName)
        : undefined;
    message.surname =
      object.surname !== undefined && object.surname !== null
        ? String(object.surname)
        : undefined;
    message.phone =
      object.phone !== undefined && object.phone !== null
        ? String(object.phone)
        : undefined;
    message.lang =
      object.lang !== undefined && object.lang !== null
        ? String(object.lang)
        : "";
    message.path =
      object.path !== undefined && object.path !== null
        ? String(object.path)
        : "";
    message.role =
      object.role !== undefined && object.role !== null
        ? String(object.role)
        : "";
    message.organizationId =
      object.organizationId !== undefined && object.organizationId !== null
        ? Number(object.organizationId)
        : 0;
    return message;
  },

  toJSON(message: UserModel): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    message.createdAt !== undefined && (obj.createdAt = message.createdAt);
    message.alias !== undefined && (obj.alias = message.alias);
    message.parentId !== undefined && (obj.parentId = message.parentId);
    message.email !== undefined && (obj.email = message.email);
    message.verified !== undefined && (obj.verified = message.verified);
    message.blocked !== undefined && (obj.blocked = message.blocked);
    message.deleted !== undefined && (obj.deleted = message.deleted);
    message.firstName !== undefined && (obj.firstName = message.firstName);
    message.middleName !== undefined && (obj.middleName = message.middleName);
    message.surname !== undefined && (obj.surname = message.surname);
    message.phone !== undefined && (obj.phone = message.phone);
    message.lang !== undefined && (obj.lang = message.lang);
    message.path !== undefined && (obj.path = message.path);
    message.role !== undefined && (obj.role = message.role);
    message.organizationId !== undefined &&
      (obj.organizationId = message.organizationId);
    return obj;
  },

  fromPartial(object: DeepPartial<UserModel>): UserModel {
    const message = { ...baseUserModel } as UserModel;
    message.id = object.id ?? 0;
    message.createdAt = object.createdAt ?? 0;
    message.alias = object.alias ?? undefined;
    message.parentId = object.parentId ?? undefined;
    message.email = object.email ?? "";
    message.verified = object.verified ?? false;
    message.blocked = object.blocked ?? false;
    message.deleted = object.deleted ?? false;
    message.firstName = object.firstName ?? undefined;
    message.middleName = object.middleName ?? undefined;
    message.surname = object.surname ?? undefined;
    message.phone = object.phone ?? undefined;
    message.lang = object.lang ?? "";
    message.path = object.path ?? "";
    message.role = object.role ?? "";
    message.organizationId = object.organizationId ?? 0;
    return message;
  },
};

messageTypeRegistry.set(UserModel.$type, UserModel);

declare var self: any | undefined;
declare var window: any | undefined;
declare var global: any | undefined;
var globalThis: any = (() => {
  if (typeof globalThis !== "undefined") return globalThis;
  if (typeof self !== "undefined") return self;
  if (typeof window !== "undefined") return window;
  if (typeof global !== "undefined") return global;
  throw "Unable to locate global object";
})();

type Builtin =
  | Date
  | Function
  | Uint8Array
  | string
  | number
  | boolean
  | undefined;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? { [K in Exclude<keyof T, "$type">]?: DeepPartial<T[K]> }
  : Partial<T>;

function longToNumber(long: Long): number {
  if (long.gt(Number.MAX_SAFE_INTEGER)) {
    throw new globalThis.Error("Value is larger than Number.MAX_SAFE_INTEGER");
  }
  return long.toNumber();
}

if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
