# @shared/grpc-client

## 1.1.0

### Minor Changes

- Update packages

### Patch Changes

- Updated dependencies
  - @shared/base-client@1.1.0

## 1.0.3

### Patch Changes

- Updated exported modules

## 1.0.2

### Patch Changes

- Updated paths

## 1.0.1

### Patch Changes

- Updaated exports
- Updated dependencies
  - @shared/base-client@1.0.1
