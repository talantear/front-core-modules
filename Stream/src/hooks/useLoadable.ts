import { useEffect } from 'react';
import { IBaseViewModel } from '../model';

const useLoadableVM = <LoadParamsType = void>(vm: IBaseViewModel<LoadParamsType>, params?: LoadParamsType) => {
  useEffect(() => {
    vm.load(params);
    return () => vm.unload();
  })
}

export default useLoadableVM;