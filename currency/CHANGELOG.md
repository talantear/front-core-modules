# @shared/currency

## 2.0.0

### Minor Changes

- Update packages

### Patch Changes

- Updated dependencies
  - @shared/gql-client@1.1.0

## 1.0.1

### Patch Changes

- Updaated exports
- Updated dependencies
  - @shared/gql-client@1.0.1
