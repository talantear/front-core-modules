import { inject, injectable } from 'inversify';
import HttpClient from '@shared/http-client/model';
import { ApiHttpClientKey } from '@shared/http-client/constants';
import { ResultCountRes, ResultItemRes, ResultListRes, BaseCommandProps } from '@shared/types/http';
import { from, map, mergeMap, Observable } from 'rxjs';
import { StreamHandler } from '@shared/stream/service';
import { ObjectWithId } from '@shared/types/string';
import { IInfoSectionService } from '../types';
import { ListOptions } from '@shared/stream/types';
import { IListSection, ISectionModel } from '../../dto';

@injectable()
class InfoSectionHttpService extends StreamHandler<ISectionModel> implements IInfoSectionService {
  private api: HttpClient;

  public constructor(
    @inject(ApiHttpClientKey) api: HttpClient,
  ) {
    super();
    this.add("default", this.streamDefault);
    this.add("plain", this.plainStream);
    this.api = api;
  }

  private streamDefault = {
    stream: (opts: ListOptions, method?: string, query?: IListSection): Observable<ISectionModel & ObjectWithId> => {
      const input: IListSection = {
        adminShow: query?.adminShow,
        slug: query?.slug as string,
        opts
      }

      return this.list(input);
    },
    loadCount: (): Observable<number> => {
      return this.count();
    },
  }

  private plainStream = {
    stream: (opts: ListOptions, method?: string, query?: object): Observable<ISectionModel & ObjectWithId> => {
      return this.plainList(opts);
    },
    loadCount: (): Observable<number> => this.streamDefault.loadCount(),
  }

  count(): Observable<number> {
    return this.api.$request<object, ResultCountRes>(
      `/api/v1/info/section/count`, {}, { method: 'POST' }
    ).pipe(map(i => i.count));
  }

  create(value: ISectionModel): Observable<ISectionModel> {
    return this.api.$request<object, ResultItemRes<ISectionModel>>(
      `/api/v1/info/section`, value, { method: 'POST' }
    ).pipe(map(i => i.item));
  }

  delete(id: number): Observable<void> {
    return this.api.$request<object, void & BaseCommandProps>(
      `/api/v1/info/section/${id}`, {}, { method: 'DELETE' }
    );
  }

  getById(id: number): Observable<ISectionModel> {
    return this.api.$request<object, ResultItemRes<ISectionModel>>(
      `/api/v1/info/section/${id}`, {}, { method: 'GET' }
    ).pipe(map(i => i.item));
  }

  plainList(value: ListOptions): Observable<ISectionModel> {
    return this.api.$request<object, ResultListRes<ISectionModel>>(
      `/api/v1/info/section/plain-list`, value, { method: 'POST' }
    ).pipe(mergeMap(i => from(i.list)));
  }

  list(value: IListSection): Observable<ISectionModel> {
    return this.api.$request<object, ResultListRes<ISectionModel>>(
      `/api/v1/info/section/list`, value, { method: 'POST' }
    ).pipe(mergeMap(i => from(i.list)));
  }

  update(value: ISectionModel): Observable<ISectionModel> {
    return this.api.$request<object, ResultItemRes<ISectionModel>>(
      `/api/v1/info/section`, value, { method: 'PUT' }
    ).pipe(map(i => i.item));
  }
}

export default InfoSectionHttpService;