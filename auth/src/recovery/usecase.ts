import { inject, injectable } from 'inversify';
import { Observable } from 'rxjs';
import { IRecoveryService } from './service/types';
import { recoveryServiceDiKey } from './constants';
import { IChangePassReq, IRecoveryReq } from './dto';
import { IStringRequest } from '@shared/types/dto';

export interface IRecoveryUsecase {
  recovery(request: IRecoveryReq): Observable<void>,
  checkToken(request: IStringRequest): Observable<void>,
  changePass(request: IChangePassReq): Observable<void>,
}

@injectable()
class RecoveryUsecase implements IRecoveryUsecase {
  public static diKey = Symbol.for('RecoveryUsecaseDiKey');

  private recoveryService: IRecoveryService;

  constructor(
    @inject(recoveryServiceDiKey) recoveryService: IRecoveryService,
  ) {
    this.recoveryService = recoveryService;
  }

  checkToken(request: IStringRequest): Observable<void> {
    return this.recoveryService.checkToken(request);
  }

  recovery(request: IRecoveryReq): Observable<void> {
    return this.recoveryService.recovery(request);
  }

  changePass(request: IChangePassReq): Observable<void> {
    return this.recoveryService.changePass(request);
  }
}

export default RecoveryUsecase;