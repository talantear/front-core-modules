import { ShortLinkModel } from '@modules/graphql/shortLink/shortLink-gql';

class ShortLink implements ShortLinkModel {
  code: string;
  createdAt?: number;
  description?: string;
  id: number;
  link: string;
  tags?: string;


  constructor(obj: ShortLinkModel) {
    this.code = obj.code;
    this.createdAt = obj.createdAt;
    this.description = obj.description;
    this.id = obj.id;
    this.link = obj.link;
    this.tags = obj.tags;
  }
}

export default ShortLink