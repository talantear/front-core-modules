import { AsyncContainerModule, interfaces } from 'inversify';
import ImageUsecase from '../usecase';
import { IImageService } from '../service/types';
import { imageServiceDiKey } from '../constants';

export const ImageModule = <T extends interfaces.Newable<IImageService>>(service: T) => new AsyncContainerModule(async (bind: interfaces.Bind, unbind: interfaces.Unbind) => {
  bind<IImageService>(imageServiceDiKey).to(service);
  bind<ImageUsecase>(ImageUsecase.diKey).to(ImageUsecase).inSingletonScope();
});
