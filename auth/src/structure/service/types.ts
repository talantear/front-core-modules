import { Observable } from 'rxjs';

export interface IStructService {
  getStructCount(userId: number, maxDepth?: number): Observable<number>
}