import { Container, interfaces } from 'inversify';
import { InfoModule } from './module';
import { IInfoSectionItemService } from '../item';
import { IInfoSectionService } from '../section';

const infoContainer = <ITEM extends interfaces.Newable<IInfoSectionItemService>, SECTION extends interfaces.Newable<IInfoSectionService>>(item: ITEM, section: SECTION): interfaces.Container => {
  const container = new Container();
  container.load(InfoModule(item, section));
  return container;
};

export default infoContainer;
