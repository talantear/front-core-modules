import { ObjectWithId } from '@shared/types/string';
import { Observable, Subscription } from 'rxjs';
import { ComponentClass, FunctionComponent } from 'react';
import ListMV from './model/ListMV';

export interface ListOptions {
  offset: number;
  limit: number;
  order?: string | undefined;
}

export type ItemIndexer = {
  [index: string]: any;
};

// TODO Refactor
export interface IPageService<T extends ObjectWithId, QueryType extends object = object> {
  loadTotalCount(list: ListMV<T>, method?: string, query?: QueryType): [Observable<number>, Subscription]

  loadPage(list: ListMV<T>, opts: ListOptions, method?: string, query?: QueryType): [Observable<T>, Subscription]
}

// TODO: count per page
export interface IBaseListUsecase<T extends ObjectWithId, QueryType extends object = object> {
  loadTotalCount(model: ListMV<T>, method?: string, query?: QueryType): [Observable<number>, Subscription]

  loadItems(model: ListMV<T>, opts: ListOptions, method?: string, query?: QueryType): [Observable<T>, Subscription];

  subscriber?(): Observable<T>

  pageModel?: ListMV<T>,
}

export interface IExternalService<T, QueryType extends object = object> {
  loadCount(method?: string, query?: QueryType): Observable<number>

  stream(opts: ListOptions, method?: string, query?: QueryType): Observable<T>;
}

export interface CommonDataMethods<T extends ObjectWithId> {
  create: (req: T) => Observable<T>,
  stream: (opts: ListOptions) => Observable<T>,
  update: (req: T) => Observable<T>,
  delete: (id: number) => Observable<void | null>,
  getById: (id: number) => Observable<T>,
  loadCount: () => Observable<number>,
}

export type RowRenderComponentProps<T> = {
  item: T;
  index: number;
  update: (item: T) => void
  remove: () => void
};

export type RowRenderComponentType<T extends ObjectWithId> =
  | ComponentClass<RowRenderComponentProps<T>>
  | FunctionComponent<RowRenderComponentProps<T>>;