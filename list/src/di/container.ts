import { Container, interfaces } from 'inversify';
import { ListModule } from './module';

const listContainer: interfaces.Container = new Container();
listContainer.load(ListModule);
export default listContainer;
