# @shared/list

## 2.0.0

### Minor Changes

- Update packages

### Patch Changes

- Updated dependencies
  - @shared/http-client@1.1.0
  - @shared/message@1.1.0
  - @shared/stream@1.1.0
  - @shared/types@1.1.0

## 1.0.8

### Patch Changes

- Updated JsonListVM

## 1.0.7

### Patch Changes

- Updated package

## 1.0.6

### Patch Changes

- Fixed build

## 1.0.5

### Patch Changes

- Updaated exports
- Updated dependencies
  - @shared/stream@1.0.1
  - @shared/types@1.0.1

## 1.0.4

### Patch Changes

- Update types imports

## 1.0.3

### Patch Changes

- Updated exports

## 1.0.2

### Patch Changes

- Added css to export

## 1.0.1

### Patch Changes

- Updated build
