import EditJsonListView from './Component';
import { FunctionComponent, useState } from 'react';
import { useInjection } from 'inversify-react';
import ListUsecase from '../../usecase';
import List from '../../di/model';
import JsonListVM from '../JsonListVM';

const EditJsonList: FunctionComponent = () => {
  const listUC = useInjection<ListUsecase<List>>(ListUsecase.diKey);
  const [vm] = useState(new JsonListVM(listUC));

  return (
    <EditJsonListView vm={vm} />
  );
};

export default EditJsonList;
