/* eslint-disable */
import { messageTypeRegistry } from "./typeRegistry";
import Long from "long";
import { grpc } from "@improbable-eng/grpc-web";
import _m0 from "protobufjs/minimal";
import { Observable } from "rxjs";
import { Id64Request, Int64Value } from "./commonTypes";
import { ListOptions } from "./paginatior";
import { Empty } from "./google/protobuf/empty";
import { BrowserHeaders } from "browser-headers";
import { share } from "rxjs/operators";

export const protobufPackage = "authMs";

export interface ProviderModel {
  $type: "authMs.ProviderModel";
  id: number;
  /** / Date of creation */
  createdAt: number;
  /** / String value */
  displayName: string;
  category: string;
  type: string;
  subType: string;
  method: string;
  clientId: string;
  clientSecret: string;
  clientId2: string;
  clientSecret2: string;
  cert: string;
  host: string;
  port: number;
  title: string;
  content: string;
  regionId: string;
  signName: string;
  templateCode: string;
  appId: string;
  endpoint: string;
  intranetEndpoint: string;
  domain: string;
  bucket: string;
  metadata: string;
  idP: string;
  issuerUrl: string;
  enableSignAuthnRequest: boolean;
  providerUrl: string;
  slug: string;
}

const baseProviderModel: object = {
  $type: "authMs.ProviderModel",
  id: 0,
  createdAt: 0,
  displayName: "",
  category: "",
  type: "",
  subType: "",
  method: "",
  clientId: "",
  clientSecret: "",
  clientId2: "",
  clientSecret2: "",
  cert: "",
  host: "",
  port: 0,
  title: "",
  content: "",
  regionId: "",
  signName: "",
  templateCode: "",
  appId: "",
  endpoint: "",
  intranetEndpoint: "",
  domain: "",
  bucket: "",
  metadata: "",
  idP: "",
  issuerUrl: "",
  enableSignAuthnRequest: false,
  providerUrl: "",
  slug: "",
};

export const ProviderModel = {
  $type: "authMs.ProviderModel" as const,

  encode(
    message: ProviderModel,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.id !== 0) {
      writer.uint32(8).uint32(message.id);
    }
    if (message.createdAt !== 0) {
      writer.uint32(16).int64(message.createdAt);
    }
    if (message.displayName !== "") {
      writer.uint32(26).string(message.displayName);
    }
    if (message.category !== "") {
      writer.uint32(34).string(message.category);
    }
    if (message.type !== "") {
      writer.uint32(42).string(message.type);
    }
    if (message.subType !== "") {
      writer.uint32(50).string(message.subType);
    }
    if (message.method !== "") {
      writer.uint32(58).string(message.method);
    }
    if (message.clientId !== "") {
      writer.uint32(66).string(message.clientId);
    }
    if (message.clientSecret !== "") {
      writer.uint32(74).string(message.clientSecret);
    }
    if (message.clientId2 !== "") {
      writer.uint32(82).string(message.clientId2);
    }
    if (message.clientSecret2 !== "") {
      writer.uint32(90).string(message.clientSecret2);
    }
    if (message.cert !== "") {
      writer.uint32(98).string(message.cert);
    }
    if (message.host !== "") {
      writer.uint32(106).string(message.host);
    }
    if (message.port !== 0) {
      writer.uint32(112).int32(message.port);
    }
    if (message.title !== "") {
      writer.uint32(122).string(message.title);
    }
    if (message.content !== "") {
      writer.uint32(130).string(message.content);
    }
    if (message.regionId !== "") {
      writer.uint32(138).string(message.regionId);
    }
    if (message.signName !== "") {
      writer.uint32(146).string(message.signName);
    }
    if (message.templateCode !== "") {
      writer.uint32(154).string(message.templateCode);
    }
    if (message.appId !== "") {
      writer.uint32(162).string(message.appId);
    }
    if (message.endpoint !== "") {
      writer.uint32(170).string(message.endpoint);
    }
    if (message.intranetEndpoint !== "") {
      writer.uint32(178).string(message.intranetEndpoint);
    }
    if (message.domain !== "") {
      writer.uint32(186).string(message.domain);
    }
    if (message.bucket !== "") {
      writer.uint32(194).string(message.bucket);
    }
    if (message.metadata !== "") {
      writer.uint32(202).string(message.metadata);
    }
    if (message.idP !== "") {
      writer.uint32(210).string(message.idP);
    }
    if (message.issuerUrl !== "") {
      writer.uint32(218).string(message.issuerUrl);
    }
    if (message.enableSignAuthnRequest === true) {
      writer.uint32(224).bool(message.enableSignAuthnRequest);
    }
    if (message.providerUrl !== "") {
      writer.uint32(234).string(message.providerUrl);
    }
    if (message.slug !== "") {
      writer.uint32(242).string(message.slug);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ProviderModel {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseProviderModel } as ProviderModel;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.uint32();
          break;
        case 2:
          message.createdAt = longToNumber(reader.int64() as Long);
          break;
        case 3:
          message.displayName = reader.string();
          break;
        case 4:
          message.category = reader.string();
          break;
        case 5:
          message.type = reader.string();
          break;
        case 6:
          message.subType = reader.string();
          break;
        case 7:
          message.method = reader.string();
          break;
        case 8:
          message.clientId = reader.string();
          break;
        case 9:
          message.clientSecret = reader.string();
          break;
        case 10:
          message.clientId2 = reader.string();
          break;
        case 11:
          message.clientSecret2 = reader.string();
          break;
        case 12:
          message.cert = reader.string();
          break;
        case 13:
          message.host = reader.string();
          break;
        case 14:
          message.port = reader.int32();
          break;
        case 15:
          message.title = reader.string();
          break;
        case 16:
          message.content = reader.string();
          break;
        case 17:
          message.regionId = reader.string();
          break;
        case 18:
          message.signName = reader.string();
          break;
        case 19:
          message.templateCode = reader.string();
          break;
        case 20:
          message.appId = reader.string();
          break;
        case 21:
          message.endpoint = reader.string();
          break;
        case 22:
          message.intranetEndpoint = reader.string();
          break;
        case 23:
          message.domain = reader.string();
          break;
        case 24:
          message.bucket = reader.string();
          break;
        case 25:
          message.metadata = reader.string();
          break;
        case 26:
          message.idP = reader.string();
          break;
        case 27:
          message.issuerUrl = reader.string();
          break;
        case 28:
          message.enableSignAuthnRequest = reader.bool();
          break;
        case 29:
          message.providerUrl = reader.string();
          break;
        case 30:
          message.slug = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ProviderModel {
    const message = { ...baseProviderModel } as ProviderModel;
    message.id =
      object.id !== undefined && object.id !== null ? Number(object.id) : 0;
    message.createdAt =
      object.createdAt !== undefined && object.createdAt !== null
        ? Number(object.createdAt)
        : 0;
    message.displayName =
      object.displayName !== undefined && object.displayName !== null
        ? String(object.displayName)
        : "";
    message.category =
      object.category !== undefined && object.category !== null
        ? String(object.category)
        : "";
    message.type =
      object.type !== undefined && object.type !== null
        ? String(object.type)
        : "";
    message.subType =
      object.subType !== undefined && object.subType !== null
        ? String(object.subType)
        : "";
    message.method =
      object.method !== undefined && object.method !== null
        ? String(object.method)
        : "";
    message.clientId =
      object.clientId !== undefined && object.clientId !== null
        ? String(object.clientId)
        : "";
    message.clientSecret =
      object.clientSecret !== undefined && object.clientSecret !== null
        ? String(object.clientSecret)
        : "";
    message.clientId2 =
      object.clientId2 !== undefined && object.clientId2 !== null
        ? String(object.clientId2)
        : "";
    message.clientSecret2 =
      object.clientSecret2 !== undefined && object.clientSecret2 !== null
        ? String(object.clientSecret2)
        : "";
    message.cert =
      object.cert !== undefined && object.cert !== null
        ? String(object.cert)
        : "";
    message.host =
      object.host !== undefined && object.host !== null
        ? String(object.host)
        : "";
    message.port =
      object.port !== undefined && object.port !== null
        ? Number(object.port)
        : 0;
    message.title =
      object.title !== undefined && object.title !== null
        ? String(object.title)
        : "";
    message.content =
      object.content !== undefined && object.content !== null
        ? String(object.content)
        : "";
    message.regionId =
      object.regionId !== undefined && object.regionId !== null
        ? String(object.regionId)
        : "";
    message.signName =
      object.signName !== undefined && object.signName !== null
        ? String(object.signName)
        : "";
    message.templateCode =
      object.templateCode !== undefined && object.templateCode !== null
        ? String(object.templateCode)
        : "";
    message.appId =
      object.appId !== undefined && object.appId !== null
        ? String(object.appId)
        : "";
    message.endpoint =
      object.endpoint !== undefined && object.endpoint !== null
        ? String(object.endpoint)
        : "";
    message.intranetEndpoint =
      object.intranetEndpoint !== undefined && object.intranetEndpoint !== null
        ? String(object.intranetEndpoint)
        : "";
    message.domain =
      object.domain !== undefined && object.domain !== null
        ? String(object.domain)
        : "";
    message.bucket =
      object.bucket !== undefined && object.bucket !== null
        ? String(object.bucket)
        : "";
    message.metadata =
      object.metadata !== undefined && object.metadata !== null
        ? String(object.metadata)
        : "";
    message.idP =
      object.idP !== undefined && object.idP !== null ? String(object.idP) : "";
    message.issuerUrl =
      object.issuerUrl !== undefined && object.issuerUrl !== null
        ? String(object.issuerUrl)
        : "";
    message.enableSignAuthnRequest =
      object.enableSignAuthnRequest !== undefined &&
      object.enableSignAuthnRequest !== null
        ? Boolean(object.enableSignAuthnRequest)
        : false;
    message.providerUrl =
      object.providerUrl !== undefined && object.providerUrl !== null
        ? String(object.providerUrl)
        : "";
    message.slug =
      object.slug !== undefined && object.slug !== null
        ? String(object.slug)
        : "";
    return message;
  },

  toJSON(message: ProviderModel): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    message.createdAt !== undefined && (obj.createdAt = message.createdAt);
    message.displayName !== undefined &&
      (obj.displayName = message.displayName);
    message.category !== undefined && (obj.category = message.category);
    message.type !== undefined && (obj.type = message.type);
    message.subType !== undefined && (obj.subType = message.subType);
    message.method !== undefined && (obj.method = message.method);
    message.clientId !== undefined && (obj.clientId = message.clientId);
    message.clientSecret !== undefined &&
      (obj.clientSecret = message.clientSecret);
    message.clientId2 !== undefined && (obj.clientId2 = message.clientId2);
    message.clientSecret2 !== undefined &&
      (obj.clientSecret2 = message.clientSecret2);
    message.cert !== undefined && (obj.cert = message.cert);
    message.host !== undefined && (obj.host = message.host);
    message.port !== undefined && (obj.port = message.port);
    message.title !== undefined && (obj.title = message.title);
    message.content !== undefined && (obj.content = message.content);
    message.regionId !== undefined && (obj.regionId = message.regionId);
    message.signName !== undefined && (obj.signName = message.signName);
    message.templateCode !== undefined &&
      (obj.templateCode = message.templateCode);
    message.appId !== undefined && (obj.appId = message.appId);
    message.endpoint !== undefined && (obj.endpoint = message.endpoint);
    message.intranetEndpoint !== undefined &&
      (obj.intranetEndpoint = message.intranetEndpoint);
    message.domain !== undefined && (obj.domain = message.domain);
    message.bucket !== undefined && (obj.bucket = message.bucket);
    message.metadata !== undefined && (obj.metadata = message.metadata);
    message.idP !== undefined && (obj.idP = message.idP);
    message.issuerUrl !== undefined && (obj.issuerUrl = message.issuerUrl);
    message.enableSignAuthnRequest !== undefined &&
      (obj.enableSignAuthnRequest = message.enableSignAuthnRequest);
    message.providerUrl !== undefined &&
      (obj.providerUrl = message.providerUrl);
    message.slug !== undefined && (obj.slug = message.slug);
    return obj;
  },

  fromPartial(object: DeepPartial<ProviderModel>): ProviderModel {
    const message = { ...baseProviderModel } as ProviderModel;
    message.id = object.id ?? 0;
    message.createdAt = object.createdAt ?? 0;
    message.displayName = object.displayName ?? "";
    message.category = object.category ?? "";
    message.type = object.type ?? "";
    message.subType = object.subType ?? "";
    message.method = object.method ?? "";
    message.clientId = object.clientId ?? "";
    message.clientSecret = object.clientSecret ?? "";
    message.clientId2 = object.clientId2 ?? "";
    message.clientSecret2 = object.clientSecret2 ?? "";
    message.cert = object.cert ?? "";
    message.host = object.host ?? "";
    message.port = object.port ?? 0;
    message.title = object.title ?? "";
    message.content = object.content ?? "";
    message.regionId = object.regionId ?? "";
    message.signName = object.signName ?? "";
    message.templateCode = object.templateCode ?? "";
    message.appId = object.appId ?? "";
    message.endpoint = object.endpoint ?? "";
    message.intranetEndpoint = object.intranetEndpoint ?? "";
    message.domain = object.domain ?? "";
    message.bucket = object.bucket ?? "";
    message.metadata = object.metadata ?? "";
    message.idP = object.idP ?? "";
    message.issuerUrl = object.issuerUrl ?? "";
    message.enableSignAuthnRequest = object.enableSignAuthnRequest ?? false;
    message.providerUrl = object.providerUrl ?? "";
    message.slug = object.slug ?? "";
    return message;
  },
};

messageTypeRegistry.set(ProviderModel.$type, ProviderModel);

/** Represents provider methods. */
export interface Provider {
  /** / Create provider */
  Create(
    request: DeepPartial<ProviderModel>,
    metadata?: grpc.Metadata
  ): Promise<ProviderModel>;
  /** / Update provider */
  Update(
    request: DeepPartial<ProviderModel>,
    metadata?: grpc.Metadata
  ): Promise<ProviderModel>;
  /** / GetById provider */
  GetById(
    request: DeepPartial<Id64Request>,
    metadata?: grpc.Metadata
  ): Promise<ProviderModel>;
  /** / List provider */
  List(
    request: DeepPartial<ListOptions>,
    metadata?: grpc.Metadata
  ): Observable<ProviderModel>;
  /** / Count provider */
  Count(
    request: DeepPartial<Empty>,
    metadata?: grpc.Metadata
  ): Promise<Int64Value>;
  /** / Remove provider */
  Remove(
    request: DeepPartial<Id64Request>,
    metadata?: grpc.Metadata
  ): Promise<Empty>;
}

export class ProviderClientImpl implements Provider {
  private readonly rpc: Rpc;

  constructor(rpc: Rpc) {
    this.rpc = rpc;
    this.Create = this.Create.bind(this);
    this.Update = this.Update.bind(this);
    this.GetById = this.GetById.bind(this);
    this.List = this.List.bind(this);
    this.Count = this.Count.bind(this);
    this.Remove = this.Remove.bind(this);
  }

  Create(
    request: DeepPartial<ProviderModel>,
    metadata?: grpc.Metadata
  ): Promise<ProviderModel> {
    return this.rpc.unary(
      ProviderCreateDesc,
      ProviderModel.fromPartial(request),
      metadata
    );
  }

  Update(
    request: DeepPartial<ProviderModel>,
    metadata?: grpc.Metadata
  ): Promise<ProviderModel> {
    return this.rpc.unary(
      ProviderUpdateDesc,
      ProviderModel.fromPartial(request),
      metadata
    );
  }

  GetById(
    request: DeepPartial<Id64Request>,
    metadata?: grpc.Metadata
  ): Promise<ProviderModel> {
    return this.rpc.unary(
      ProviderGetByIdDesc,
      Id64Request.fromPartial(request),
      metadata
    );
  }

  List(
    request: DeepPartial<ListOptions>,
    metadata?: grpc.Metadata
  ): Observable<ProviderModel> {
    return this.rpc.invoke(
      ProviderListDesc,
      ListOptions.fromPartial(request),
      metadata
    );
  }

  Count(
    request: DeepPartial<Empty>,
    metadata?: grpc.Metadata
  ): Promise<Int64Value> {
    return this.rpc.unary(
      ProviderCountDesc,
      Empty.fromPartial(request),
      metadata
    );
  }

  Remove(
    request: DeepPartial<Id64Request>,
    metadata?: grpc.Metadata
  ): Promise<Empty> {
    return this.rpc.unary(
      ProviderRemoveDesc,
      Id64Request.fromPartial(request),
      metadata
    );
  }
}

export const ProviderDesc = {
  serviceName: "authMs.Provider",
};

export const ProviderCreateDesc: UnaryMethodDefinitionish = {
  methodName: "Create",
  service: ProviderDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return ProviderModel.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...ProviderModel.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const ProviderUpdateDesc: UnaryMethodDefinitionish = {
  methodName: "Update",
  service: ProviderDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return ProviderModel.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...ProviderModel.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const ProviderGetByIdDesc: UnaryMethodDefinitionish = {
  methodName: "GetById",
  service: ProviderDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return Id64Request.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...ProviderModel.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const ProviderListDesc: UnaryMethodDefinitionish = {
  methodName: "List",
  service: ProviderDesc,
  requestStream: false,
  responseStream: true,
  requestType: {
    serializeBinary() {
      return ListOptions.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...ProviderModel.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const ProviderCountDesc: UnaryMethodDefinitionish = {
  methodName: "Count",
  service: ProviderDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return Empty.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...Int64Value.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const ProviderRemoveDesc: UnaryMethodDefinitionish = {
  methodName: "Remove",
  service: ProviderDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return Id64Request.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...Empty.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

interface UnaryMethodDefinitionishR
  extends grpc.UnaryMethodDefinition<any, any> {
  requestStream: any;
  responseStream: any;
}

type UnaryMethodDefinitionish = UnaryMethodDefinitionishR;

interface Rpc {
  unary<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    request: any,
    metadata: grpc.Metadata | undefined
  ): Promise<any>;
  invoke<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    request: any,
    metadata: grpc.Metadata | undefined
  ): Observable<any>;
}

export class GrpcWebImpl {
  private host: string;
  private options: {
    transport?: grpc.TransportFactory;
    streamingTransport?: grpc.TransportFactory;
    debug?: boolean;
    metadata?: grpc.Metadata;
  };

  constructor(
    host: string,
    options: {
      transport?: grpc.TransportFactory;
      streamingTransport?: grpc.TransportFactory;
      debug?: boolean;
      metadata?: grpc.Metadata;
    }
  ) {
    this.host = host;
    this.options = options;
  }

  unary<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    _request: any,
    metadata: grpc.Metadata | undefined
  ): Promise<any> {
    const request = { ..._request, ...methodDesc.requestType };
    const maybeCombinedMetadata =
      metadata && this.options.metadata
        ? new BrowserHeaders({
            ...this.options?.metadata.headersMap,
            ...metadata?.headersMap,
          })
        : metadata || this.options.metadata;
    return new Promise((resolve, reject) => {
      grpc.unary(methodDesc, {
        request,
        host: this.host,
        metadata: maybeCombinedMetadata,
        transport: this.options.transport,
        debug: this.options.debug,
        onEnd: function (response) {
          if (response.status === grpc.Code.OK) {
            resolve(response.message);
          } else {
            const err = new Error(response.statusMessage) as any;
            err.code = response.status;
            err.metadata = response.trailers;
            reject(err);
          }
        },
      });
    });
  }

  invoke<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    _request: any,
    metadata: grpc.Metadata | undefined
  ): Observable<any> {
    // Status Response Codes (https://developers.google.com/maps-booking/reference/grpc-api/status_codes)
    const upStreamCodes = [2, 4, 8, 9, 10, 13, 14, 15];
    const DEFAULT_TIMEOUT_TIME: number = 3_000;
    const request = { ..._request, ...methodDesc.requestType };
    const maybeCombinedMetadata =
      metadata && this.options.metadata
        ? new BrowserHeaders({
            ...this.options?.metadata.headersMap,
            ...metadata?.headersMap,
          })
        : metadata || this.options.metadata;
    return new Observable((observer) => {
      const upStream = () => {
        const client = grpc.invoke(methodDesc, {
          host: this.host,
          request,
          transport: this.options.streamingTransport || this.options.transport,
          metadata: maybeCombinedMetadata,
          debug: this.options.debug,
          onMessage: (next) => observer.next(next),
          onEnd: (code: grpc.Code, message: string) => {
            if (code === 0) {
              observer.complete();
            } else if (upStreamCodes.includes(code)) {
              setTimeout(upStream, DEFAULT_TIMEOUT_TIME);
            } else {
              observer.error(new Error(`Error ${code} ${message}`));
            }
          },
        });
        observer.add(() => client.close());
      };
      upStream();
    }).pipe(share());
  }
}

declare var self: any | undefined;
declare var window: any | undefined;
declare var global: any | undefined;
var globalThis: any = (() => {
  if (typeof globalThis !== "undefined") return globalThis;
  if (typeof self !== "undefined") return self;
  if (typeof window !== "undefined") return window;
  if (typeof global !== "undefined") return global;
  throw "Unable to locate global object";
})();

type Builtin =
  | Date
  | Function
  | Uint8Array
  | string
  | number
  | boolean
  | undefined;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? { [K in Exclude<keyof T, "$type">]?: DeepPartial<T[K]> }
  : Partial<T>;

function longToNumber(long: Long): number {
  if (long.gt(Number.MAX_SAFE_INTEGER)) {
    throw new globalThis.Error("Value is larger than Number.MAX_SAFE_INTEGER");
  }
  return long.toNumber();
}

if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
