import { useInjection } from 'inversify-react';
import RoleUsecase from './usecase';
import { AppConfigDiKey } from '@shared/app/constants';
import { IAppConfig } from '@shared/app/types';
import { useEffect, useState } from 'react';
import User from '../user/models/user';

// This hook need to check if you inherited role or not
//
// EXAMPLE
// This check if admin inherited from user
// useRoleCheck('user', 'admin');

const useRoleCheck = <T extends object>(session: User<T> | undefined, role: string) => {
  const [isInherit, setIsInherit] = useState<boolean>();
  const [initialized, setInitialized] = useState(false);
  const uc = useInjection<RoleUsecase>(RoleUsecase.diKey);
  const app = useInjection<IAppConfig>(AppConfigDiKey);

  useEffect(() => {
    if (!uc.isInitialized()) {
      uc.initialize().then(() => setInitialized(true));
    } else {
      setInitialized(true);
    }
  }, []);

  useEffect(() => {
    if (initialized && session) {
      setIsInherit(uc.isInherit(session.role, role, `${app.org}:${app.app}`));
    }
  }, [initialized, session]);

  return isInherit;
}

export default useRoleCheck;