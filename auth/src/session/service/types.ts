import User from '../../user/models/user';
import { Observable } from 'rxjs';
import { IUserModel } from '../../user/dto';

export interface IProfileRes {
  profile: IUserModel,
}

export interface IAccessCheckResult {
  isPublic: boolean,
}

export interface IDispose {
  dispose(): void
}

export interface ISessionService<UserType extends User = User> {
  check: (path: string, app?: string) => Promise<IAccessCheckResult>,
  clear: (app?: string) => Promise<void>,
  update: (token: string) => void,
  getCurrentUser(): UserType | undefined
  getCurrentUser$(): Observable<UserType | undefined>
}