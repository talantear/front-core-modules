import { ContainerModule, interfaces } from 'inversify';
import RecoveryUsecase, { IRecoveryUsecase } from '../usecase';
import { IRecoveryService } from '../service/types';
import { recoveryServiceDiKey } from '../constants';

export const RecoveryModule = <T extends interfaces.Newable<IRecoveryService>>(service: T) => new ContainerModule((bind: interfaces.Bind) => {
  bind<IRecoveryService>(recoveryServiceDiKey).to(service);
  bind<IRecoveryUsecase>(RecoveryUsecase.diKey).to(RecoveryUsecase).inSingletonScope();
});
