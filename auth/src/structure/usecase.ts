import { inject, injectable } from 'inversify';
import { Observable, of } from 'rxjs';
import { IStructService } from './service/types';
import { structServiceDiKey } from './constants';
import { sessionServiceDiKey } from '../session';
import { ISessionService } from '../session/service/types';

export interface IStructUsecase {
  getStructCount(): Observable<number>
}

@injectable()
class StructUsecase implements IStructUsecase {
  public static diKey = Symbol.for('StructUsecaseDiKey');

  private structureService: IStructService;
  private sessionService: ISessionService;

  constructor(
    @inject(structServiceDiKey) structureService: IStructService,
    @inject(sessionServiceDiKey) sessionService: ISessionService,
  ) {
    this.structureService = structureService;
    this.sessionService = sessionService;
  }

  getStructCount(): Observable<number> {
    const user = this.sessionService.getCurrentUser();

    if (user?.id) {
      return this.structureService.getStructCount(user.id)
    }

    return of(0)
  }
}

export default StructUsecase;