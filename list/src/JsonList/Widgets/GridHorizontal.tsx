import React from 'react';

const GridHorizontal = (props: any) => {
  const fields = Object.entries(props.formData) as (string | number)[][];

  return (
    <div style={{ display: 'grid', gridTemplateColumns: `repeat(${fields.length}, 1fr)`, gridGap: '24px' }}>
      {fields.map(i => {
        return (
          <div>
            <label>
              {props.schema.properties[i[0]].title}
            </label>
            <input
              {...props}
              style={{
                width: '100%',
                height: '30px',
                border: '1px solid #B7B7B7',
                borderRadius: '12px',
                paddingLeft: '10px',
              }}
              type={props.schema.properties[i[0]].type}
              value={i[1]} />
          </div>
        );
      })}
    </div>
  );
};

export default GridHorizontal;
