import { ListOptions } from "@shared/stream/types";

export interface ISectionModel {
  createdAt: number,
  id: number,
  index: number,
  pid?: number,
  showSupport: boolean,
  slug: string,
  title: string,
}

export type IListSection = {
  adminShow?: boolean,
  opts?: ListOptions,
  slug: string,
}