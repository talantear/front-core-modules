import { inject, injectable } from 'inversify';
import { ApiClient } from '@shared/grpc-client/model';
import { AuthLoginDesc, AuthRegisterDesc, RegAndLoginReq } from '../../../proto/auth';
import { AppConfigDiKey } from '@shared/app/constants';
import { IAppConfig } from '@shared/app/types';
import { IAuthService } from '../types';
import { ILoginResp, IRegAndLoginReq, IRegisterResp } from '../../dto';

@injectable()
class AuthService implements IAuthService {
  private api: ApiClient;
  private appConfig: IAppConfig;

  public constructor(
    @inject(ApiClient.diKey) api: ApiClient,
    @inject(AppConfigDiKey) appConfig: IAppConfig,
  ) {
    this.api = api;
    this.appConfig = appConfig;
  }

  login(req: IRegAndLoginReq): Promise<ILoginResp> {
    return this.api.unary(
      AuthLoginDesc,
      {
        obj: RegAndLoginReq.fromPartial(req),
        coder: RegAndLoginReq,
      },
      { host: this.appConfig.authUrlServer }
    );
  }

  register(req: IRegAndLoginReq): Promise<IRegisterResp> {
    return this.api.unary(
      AuthRegisterDesc,
      {
        obj: RegAndLoginReq.fromPartial(req),
        coder: RegAndLoginReq,
      },
      { host: this.appConfig.authUrlServer }
    );
  }
}

export default AuthService;