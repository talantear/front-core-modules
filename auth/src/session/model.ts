import User from '../user/models/user';
import { ISessionUsecase } from './usecase';

class SessionViewModel<Type extends User> {
  private sessionUC: ISessionUsecase;
  private readonly onUserLoaded: () => void;

	constructor(sessionUC: ISessionUsecase, onUserLoaded: () => void) {
    this.sessionUC = sessionUC;
    this.onUserLoaded = onUserLoaded;
	}

  public async checkAccess(path: string) {
    await this.sessionUC.check(path);
  }
}

export default SessionViewModel;