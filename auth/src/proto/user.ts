/* eslint-disable */
import { messageTypeRegistry } from "./typeRegistry";
import Long from "long";
import { grpc } from "@improbable-eng/grpc-web";
import _m0 from "protobufjs/minimal";
import { ListOptions } from "./paginatior";
import { Observable } from "rxjs";
import { IdRequest, BoolValue, Int64Value, StringRequest } from "./commonTypes";
import { UserModel, ShortUserModel } from "./authCommon";
import { Empty } from "./google/protobuf/empty";
import { BrowserHeaders } from "browser-headers";
import { share } from "rxjs/operators";

export const protobufPackage = "authMs";

export interface GetByStringReq {
  $type: "authMs.GetByStringReq";
  value: string;
  appSlug: string;
}

/** Represents changing string data model. */
export interface ChangeStringDataReq {
  $type: "authMs.ChangeStringDataReq";
  id: number;
  /** / String value */
  value: string;
  app: string;
}

export interface UserFilterModel {
  $type: "authMs.UserFilterModel";
  org?: string | undefined;
  role?: string | undefined;
  query?: string | undefined;
}

export interface ChangePasswordReq {
  $type: "authMs.ChangePasswordReq";
  userId: number;
  password: string;
  oldPassword: string;
}

export interface UserListReq {
  $type: "authMs.UserListReq";
  opts?: ListOptions;
  filter?: UserFilterModel | undefined;
}

export interface UserCountReq {
  $type: "authMs.UserCountReq";
  filter?: UserFilterModel | undefined;
}

export interface VerifyByTokenReq {
  $type: "authMs.VerifyByTokenReq";
  token: string;
  app: string;
}

const baseGetByStringReq: object = {
  $type: "authMs.GetByStringReq",
  value: "",
  appSlug: "",
};

export const GetByStringReq = {
  $type: "authMs.GetByStringReq" as const,

  encode(
    message: GetByStringReq,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.value !== "") {
      writer.uint32(10).string(message.value);
    }
    if (message.appSlug !== "") {
      writer.uint32(18).string(message.appSlug);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): GetByStringReq {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseGetByStringReq } as GetByStringReq;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.value = reader.string();
          break;
        case 2:
          message.appSlug = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): GetByStringReq {
    const message = { ...baseGetByStringReq } as GetByStringReq;
    message.value =
      object.value !== undefined && object.value !== null
        ? String(object.value)
        : "";
    message.appSlug =
      object.appSlug !== undefined && object.appSlug !== null
        ? String(object.appSlug)
        : "";
    return message;
  },

  toJSON(message: GetByStringReq): unknown {
    const obj: any = {};
    message.value !== undefined && (obj.value = message.value);
    message.appSlug !== undefined && (obj.appSlug = message.appSlug);
    return obj;
  },

  fromPartial(object: DeepPartial<GetByStringReq>): GetByStringReq {
    const message = { ...baseGetByStringReq } as GetByStringReq;
    message.value = object.value ?? "";
    message.appSlug = object.appSlug ?? "";
    return message;
  },
};

messageTypeRegistry.set(GetByStringReq.$type, GetByStringReq);

const baseChangeStringDataReq: object = {
  $type: "authMs.ChangeStringDataReq",
  id: 0,
  value: "",
  app: "",
};

export const ChangeStringDataReq = {
  $type: "authMs.ChangeStringDataReq" as const,

  encode(
    message: ChangeStringDataReq,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.id !== 0) {
      writer.uint32(8).uint32(message.id);
    }
    if (message.value !== "") {
      writer.uint32(18).string(message.value);
    }
    if (message.app !== "") {
      writer.uint32(26).string(message.app);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ChangeStringDataReq {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseChangeStringDataReq } as ChangeStringDataReq;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.uint32();
          break;
        case 2:
          message.value = reader.string();
          break;
        case 3:
          message.app = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ChangeStringDataReq {
    const message = { ...baseChangeStringDataReq } as ChangeStringDataReq;
    message.id =
      object.id !== undefined && object.id !== null ? Number(object.id) : 0;
    message.value =
      object.value !== undefined && object.value !== null
        ? String(object.value)
        : "";
    message.app =
      object.app !== undefined && object.app !== null ? String(object.app) : "";
    return message;
  },

  toJSON(message: ChangeStringDataReq): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    message.value !== undefined && (obj.value = message.value);
    message.app !== undefined && (obj.app = message.app);
    return obj;
  },

  fromPartial(object: DeepPartial<ChangeStringDataReq>): ChangeStringDataReq {
    const message = { ...baseChangeStringDataReq } as ChangeStringDataReq;
    message.id = object.id ?? 0;
    message.value = object.value ?? "";
    message.app = object.app ?? "";
    return message;
  },
};

messageTypeRegistry.set(ChangeStringDataReq.$type, ChangeStringDataReq);

const baseUserFilterModel: object = { $type: "authMs.UserFilterModel" };

export const UserFilterModel = {
  $type: "authMs.UserFilterModel" as const,

  encode(
    message: UserFilterModel,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.org !== undefined) {
      writer.uint32(10).string(message.org);
    }
    if (message.role !== undefined) {
      writer.uint32(18).string(message.role);
    }
    if (message.query !== undefined) {
      writer.uint32(26).string(message.query);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): UserFilterModel {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseUserFilterModel } as UserFilterModel;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.org = reader.string();
          break;
        case 2:
          message.role = reader.string();
          break;
        case 3:
          message.query = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): UserFilterModel {
    const message = { ...baseUserFilterModel } as UserFilterModel;
    message.org =
      object.org !== undefined && object.org !== null
        ? String(object.org)
        : undefined;
    message.role =
      object.role !== undefined && object.role !== null
        ? String(object.role)
        : undefined;
    message.query =
      object.query !== undefined && object.query !== null
        ? String(object.query)
        : undefined;
    return message;
  },

  toJSON(message: UserFilterModel): unknown {
    const obj: any = {};
    message.org !== undefined && (obj.org = message.org);
    message.role !== undefined && (obj.role = message.role);
    message.query !== undefined && (obj.query = message.query);
    return obj;
  },

  fromPartial(object: DeepPartial<UserFilterModel>): UserFilterModel {
    const message = { ...baseUserFilterModel } as UserFilterModel;
    message.org = object.org ?? undefined;
    message.role = object.role ?? undefined;
    message.query = object.query ?? undefined;
    return message;
  },
};

messageTypeRegistry.set(UserFilterModel.$type, UserFilterModel);

const baseChangePasswordReq: object = {
  $type: "authMs.ChangePasswordReq",
  userId: 0,
  password: "",
  oldPassword: "",
};

export const ChangePasswordReq = {
  $type: "authMs.ChangePasswordReq" as const,

  encode(
    message: ChangePasswordReq,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.userId !== 0) {
      writer.uint32(8).uint64(message.userId);
    }
    if (message.password !== "") {
      writer.uint32(18).string(message.password);
    }
    if (message.oldPassword !== "") {
      writer.uint32(26).string(message.oldPassword);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ChangePasswordReq {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseChangePasswordReq } as ChangePasswordReq;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.userId = longToNumber(reader.uint64() as Long);
          break;
        case 2:
          message.password = reader.string();
          break;
        case 3:
          message.oldPassword = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ChangePasswordReq {
    const message = { ...baseChangePasswordReq } as ChangePasswordReq;
    message.userId =
      object.userId !== undefined && object.userId !== null
        ? Number(object.userId)
        : 0;
    message.password =
      object.password !== undefined && object.password !== null
        ? String(object.password)
        : "";
    message.oldPassword =
      object.oldPassword !== undefined && object.oldPassword !== null
        ? String(object.oldPassword)
        : "";
    return message;
  },

  toJSON(message: ChangePasswordReq): unknown {
    const obj: any = {};
    message.userId !== undefined && (obj.userId = message.userId);
    message.password !== undefined && (obj.password = message.password);
    message.oldPassword !== undefined &&
      (obj.oldPassword = message.oldPassword);
    return obj;
  },

  fromPartial(object: DeepPartial<ChangePasswordReq>): ChangePasswordReq {
    const message = { ...baseChangePasswordReq } as ChangePasswordReq;
    message.userId = object.userId ?? 0;
    message.password = object.password ?? "";
    message.oldPassword = object.oldPassword ?? "";
    return message;
  },
};

messageTypeRegistry.set(ChangePasswordReq.$type, ChangePasswordReq);

const baseUserListReq: object = { $type: "authMs.UserListReq" };

export const UserListReq = {
  $type: "authMs.UserListReq" as const,

  encode(
    message: UserListReq,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.opts !== undefined) {
      ListOptions.encode(message.opts, writer.uint32(10).fork()).ldelim();
    }
    if (message.filter !== undefined) {
      UserFilterModel.encode(message.filter, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): UserListReq {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseUserListReq } as UserListReq;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.opts = ListOptions.decode(reader, reader.uint32());
          break;
        case 2:
          message.filter = UserFilterModel.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): UserListReq {
    const message = { ...baseUserListReq } as UserListReq;
    message.opts =
      object.opts !== undefined && object.opts !== null
        ? ListOptions.fromJSON(object.opts)
        : undefined;
    message.filter =
      object.filter !== undefined && object.filter !== null
        ? UserFilterModel.fromJSON(object.filter)
        : undefined;
    return message;
  },

  toJSON(message: UserListReq): unknown {
    const obj: any = {};
    message.opts !== undefined &&
      (obj.opts = message.opts ? ListOptions.toJSON(message.opts) : undefined);
    message.filter !== undefined &&
      (obj.filter = message.filter
        ? UserFilterModel.toJSON(message.filter)
        : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<UserListReq>): UserListReq {
    const message = { ...baseUserListReq } as UserListReq;
    message.opts =
      object.opts !== undefined && object.opts !== null
        ? ListOptions.fromPartial(object.opts)
        : undefined;
    message.filter =
      object.filter !== undefined && object.filter !== null
        ? UserFilterModel.fromPartial(object.filter)
        : undefined;
    return message;
  },
};

messageTypeRegistry.set(UserListReq.$type, UserListReq);

const baseUserCountReq: object = { $type: "authMs.UserCountReq" };

export const UserCountReq = {
  $type: "authMs.UserCountReq" as const,

  encode(
    message: UserCountReq,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.filter !== undefined) {
      UserFilterModel.encode(message.filter, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): UserCountReq {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseUserCountReq } as UserCountReq;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.filter = UserFilterModel.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): UserCountReq {
    const message = { ...baseUserCountReq } as UserCountReq;
    message.filter =
      object.filter !== undefined && object.filter !== null
        ? UserFilterModel.fromJSON(object.filter)
        : undefined;
    return message;
  },

  toJSON(message: UserCountReq): unknown {
    const obj: any = {};
    message.filter !== undefined &&
      (obj.filter = message.filter
        ? UserFilterModel.toJSON(message.filter)
        : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<UserCountReq>): UserCountReq {
    const message = { ...baseUserCountReq } as UserCountReq;
    message.filter =
      object.filter !== undefined && object.filter !== null
        ? UserFilterModel.fromPartial(object.filter)
        : undefined;
    return message;
  },
};

messageTypeRegistry.set(UserCountReq.$type, UserCountReq);

const baseVerifyByTokenReq: object = {
  $type: "authMs.VerifyByTokenReq",
  token: "",
  app: "",
};

export const VerifyByTokenReq = {
  $type: "authMs.VerifyByTokenReq" as const,

  encode(
    message: VerifyByTokenReq,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.token !== "") {
      writer.uint32(10).string(message.token);
    }
    if (message.app !== "") {
      writer.uint32(18).string(message.app);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): VerifyByTokenReq {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseVerifyByTokenReq } as VerifyByTokenReq;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.token = reader.string();
          break;
        case 2:
          message.app = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): VerifyByTokenReq {
    const message = { ...baseVerifyByTokenReq } as VerifyByTokenReq;
    message.token =
      object.token !== undefined && object.token !== null
        ? String(object.token)
        : "";
    message.app =
      object.app !== undefined && object.app !== null ? String(object.app) : "";
    return message;
  },

  toJSON(message: VerifyByTokenReq): unknown {
    const obj: any = {};
    message.token !== undefined && (obj.token = message.token);
    message.app !== undefined && (obj.app = message.app);
    return obj;
  },

  fromPartial(object: DeepPartial<VerifyByTokenReq>): VerifyByTokenReq {
    const message = { ...baseVerifyByTokenReq } as VerifyByTokenReq;
    message.token = object.token ?? "";
    message.app = object.app ?? "";
    return message;
  },
};

messageTypeRegistry.set(VerifyByTokenReq.$type, VerifyByTokenReq);

/** Represents user methods. */
export interface User {
  /** / Represents if user exist */
  Exist(
    request: DeepPartial<IdRequest>,
    metadata?: grpc.Metadata
  ): Promise<BoolValue>;
  /** / Represents get user by ID */
  Get(
    request: DeepPartial<IdRequest>,
    metadata?: grpc.Metadata
  ): Promise<ShortUserModel>;
  /** / Represents get user by Email */
  GetByEmail(
    request: DeepPartial<GetByStringReq>,
    metadata?: grpc.Metadata
  ): Promise<ShortUserModel>;
  /** / Represents get user by Alias */
  GetByAlias(
    request: DeepPartial<GetByStringReq>,
    metadata?: grpc.Metadata
  ): Promise<ShortUserModel>;
  /** / Represents get list of users */
  List(
    request: DeepPartial<UserListReq>,
    metadata?: grpc.Metadata
  ): Observable<UserModel>;
  /** / Represents get users count */
  Count(
    request: DeepPartial<UserCountReq>,
    metadata?: grpc.Metadata
  ): Promise<Int64Value>;
  /** / Represents update user */
  Update(
    request: DeepPartial<UserModel>,
    metadata?: grpc.Metadata
  ): Promise<StringRequest>;
  /** / Represents verifying email */
  VerifyEmail(
    request: DeepPartial<IdRequest>,
    metadata?: grpc.Metadata
  ): Promise<Empty>;
  /** / Represents changing alias */
  ChangeAlias(
    request: DeepPartial<ChangeStringDataReq>,
    metadata?: grpc.Metadata
  ): Promise<Empty>;
  /** / Represents changing email */
  ChangeEmail(
    request: DeepPartial<ChangeStringDataReq>,
    metadata?: grpc.Metadata
  ): Promise<Empty>;
  /** / Represents changing password */
  ChangePassword(
    request: DeepPartial<ChangePasswordReq>,
    metadata?: grpc.Metadata
  ): Promise<Empty>;
  /** / Represents update user */
  UpdateByAdmin(
    request: DeepPartial<UserModel>,
    metadata?: grpc.Metadata
  ): Promise<Empty>;
  VerifyByToken(
    request: DeepPartial<VerifyByTokenReq>,
    metadata?: grpc.Metadata
  ): Promise<Empty>;
  CreateByAdmin(
    request: DeepPartial<UserModel>,
    metadata?: grpc.Metadata
  ): Promise<Empty>;
  GetFullById(
    request: DeepPartial<IdRequest>,
    metadata?: grpc.Metadata
  ): Promise<UserModel>;
}

export class UserClientImpl implements User {
  private readonly rpc: Rpc;

  constructor(rpc: Rpc) {
    this.rpc = rpc;
    this.Exist = this.Exist.bind(this);
    this.Get = this.Get.bind(this);
    this.GetByEmail = this.GetByEmail.bind(this);
    this.GetByAlias = this.GetByAlias.bind(this);
    this.List = this.List.bind(this);
    this.Count = this.Count.bind(this);
    this.Update = this.Update.bind(this);
    this.VerifyEmail = this.VerifyEmail.bind(this);
    this.ChangeAlias = this.ChangeAlias.bind(this);
    this.ChangeEmail = this.ChangeEmail.bind(this);
    this.ChangePassword = this.ChangePassword.bind(this);
    this.UpdateByAdmin = this.UpdateByAdmin.bind(this);
    this.VerifyByToken = this.VerifyByToken.bind(this);
    this.CreateByAdmin = this.CreateByAdmin.bind(this);
    this.GetFullById = this.GetFullById.bind(this);
  }

  Exist(
    request: DeepPartial<IdRequest>,
    metadata?: grpc.Metadata
  ): Promise<BoolValue> {
    return this.rpc.unary(
      UserExistDesc,
      IdRequest.fromPartial(request),
      metadata
    );
  }

  Get(
    request: DeepPartial<IdRequest>,
    metadata?: grpc.Metadata
  ): Promise<ShortUserModel> {
    return this.rpc.unary(
      UserGetDesc,
      IdRequest.fromPartial(request),
      metadata
    );
  }

  GetByEmail(
    request: DeepPartial<GetByStringReq>,
    metadata?: grpc.Metadata
  ): Promise<ShortUserModel> {
    return this.rpc.unary(
      UserGetByEmailDesc,
      GetByStringReq.fromPartial(request),
      metadata
    );
  }

  GetByAlias(
    request: DeepPartial<GetByStringReq>,
    metadata?: grpc.Metadata
  ): Promise<ShortUserModel> {
    return this.rpc.unary(
      UserGetByAliasDesc,
      GetByStringReq.fromPartial(request),
      metadata
    );
  }

  List(
    request: DeepPartial<UserListReq>,
    metadata?: grpc.Metadata
  ): Observable<UserModel> {
    return this.rpc.invoke(
      UserListDesc,
      UserListReq.fromPartial(request),
      metadata
    );
  }

  Count(
    request: DeepPartial<UserCountReq>,
    metadata?: grpc.Metadata
  ): Promise<Int64Value> {
    return this.rpc.unary(
      UserCountDesc,
      UserCountReq.fromPartial(request),
      metadata
    );
  }

  Update(
    request: DeepPartial<UserModel>,
    metadata?: grpc.Metadata
  ): Promise<StringRequest> {
    return this.rpc.unary(
      UserUpdateDesc,
      UserModel.fromPartial(request),
      metadata
    );
  }

  VerifyEmail(
    request: DeepPartial<IdRequest>,
    metadata?: grpc.Metadata
  ): Promise<Empty> {
    return this.rpc.unary(
      UserVerifyEmailDesc,
      IdRequest.fromPartial(request),
      metadata
    );
  }

  ChangeAlias(
    request: DeepPartial<ChangeStringDataReq>,
    metadata?: grpc.Metadata
  ): Promise<Empty> {
    return this.rpc.unary(
      UserChangeAliasDesc,
      ChangeStringDataReq.fromPartial(request),
      metadata
    );
  }

  ChangeEmail(
    request: DeepPartial<ChangeStringDataReq>,
    metadata?: grpc.Metadata
  ): Promise<Empty> {
    return this.rpc.unary(
      UserChangeEmailDesc,
      ChangeStringDataReq.fromPartial(request),
      metadata
    );
  }

  ChangePassword(
    request: DeepPartial<ChangePasswordReq>,
    metadata?: grpc.Metadata
  ): Promise<Empty> {
    return this.rpc.unary(
      UserChangePasswordDesc,
      ChangePasswordReq.fromPartial(request),
      metadata
    );
  }

  UpdateByAdmin(
    request: DeepPartial<UserModel>,
    metadata?: grpc.Metadata
  ): Promise<Empty> {
    return this.rpc.unary(
      UserUpdateByAdminDesc,
      UserModel.fromPartial(request),
      metadata
    );
  }

  VerifyByToken(
    request: DeepPartial<VerifyByTokenReq>,
    metadata?: grpc.Metadata
  ): Promise<Empty> {
    return this.rpc.unary(
      UserVerifyByTokenDesc,
      VerifyByTokenReq.fromPartial(request),
      metadata
    );
  }

  CreateByAdmin(
    request: DeepPartial<UserModel>,
    metadata?: grpc.Metadata
  ): Promise<Empty> {
    return this.rpc.unary(
      UserCreateByAdminDesc,
      UserModel.fromPartial(request),
      metadata
    );
  }

  GetFullById(
    request: DeepPartial<IdRequest>,
    metadata?: grpc.Metadata
  ): Promise<UserModel> {
    return this.rpc.unary(
      UserGetFullByIdDesc,
      IdRequest.fromPartial(request),
      metadata
    );
  }
}

export const UserDesc = {
  serviceName: "authMs.User",
};

export const UserExistDesc: UnaryMethodDefinitionish = {
  methodName: "Exist",
  service: UserDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return IdRequest.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...BoolValue.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const UserGetDesc: UnaryMethodDefinitionish = {
  methodName: "Get",
  service: UserDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return IdRequest.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...ShortUserModel.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const UserGetByEmailDesc: UnaryMethodDefinitionish = {
  methodName: "GetByEmail",
  service: UserDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return GetByStringReq.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...ShortUserModel.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const UserGetByAliasDesc: UnaryMethodDefinitionish = {
  methodName: "GetByAlias",
  service: UserDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return GetByStringReq.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...ShortUserModel.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const UserListDesc: UnaryMethodDefinitionish = {
  methodName: "List",
  service: UserDesc,
  requestStream: false,
  responseStream: true,
  requestType: {
    serializeBinary() {
      return UserListReq.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...UserModel.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const UserCountDesc: UnaryMethodDefinitionish = {
  methodName: "Count",
  service: UserDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return UserCountReq.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...Int64Value.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const UserUpdateDesc: UnaryMethodDefinitionish = {
  methodName: "Update",
  service: UserDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return UserModel.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...StringRequest.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const UserVerifyEmailDesc: UnaryMethodDefinitionish = {
  methodName: "VerifyEmail",
  service: UserDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return IdRequest.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...Empty.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const UserChangeAliasDesc: UnaryMethodDefinitionish = {
  methodName: "ChangeAlias",
  service: UserDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return ChangeStringDataReq.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...Empty.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const UserChangeEmailDesc: UnaryMethodDefinitionish = {
  methodName: "ChangeEmail",
  service: UserDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return ChangeStringDataReq.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...Empty.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const UserChangePasswordDesc: UnaryMethodDefinitionish = {
  methodName: "ChangePassword",
  service: UserDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return ChangePasswordReq.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...Empty.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const UserUpdateByAdminDesc: UnaryMethodDefinitionish = {
  methodName: "UpdateByAdmin",
  service: UserDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return UserModel.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...Empty.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const UserVerifyByTokenDesc: UnaryMethodDefinitionish = {
  methodName: "VerifyByToken",
  service: UserDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return VerifyByTokenReq.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...Empty.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const UserCreateByAdminDesc: UnaryMethodDefinitionish = {
  methodName: "CreateByAdmin",
  service: UserDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return UserModel.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...Empty.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const UserGetFullByIdDesc: UnaryMethodDefinitionish = {
  methodName: "GetFullById",
  service: UserDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return IdRequest.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...UserModel.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

interface UnaryMethodDefinitionishR
  extends grpc.UnaryMethodDefinition<any, any> {
  requestStream: any;
  responseStream: any;
}

type UnaryMethodDefinitionish = UnaryMethodDefinitionishR;

interface Rpc {
  unary<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    request: any,
    metadata: grpc.Metadata | undefined
  ): Promise<any>;
  invoke<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    request: any,
    metadata: grpc.Metadata | undefined
  ): Observable<any>;
}

export class GrpcWebImpl {
  private host: string;
  private options: {
    transport?: grpc.TransportFactory;
    streamingTransport?: grpc.TransportFactory;
    debug?: boolean;
    metadata?: grpc.Metadata;
  };

  constructor(
    host: string,
    options: {
      transport?: grpc.TransportFactory;
      streamingTransport?: grpc.TransportFactory;
      debug?: boolean;
      metadata?: grpc.Metadata;
    }
  ) {
    this.host = host;
    this.options = options;
  }

  unary<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    _request: any,
    metadata: grpc.Metadata | undefined
  ): Promise<any> {
    const request = { ..._request, ...methodDesc.requestType };
    const maybeCombinedMetadata =
      metadata && this.options.metadata
        ? new BrowserHeaders({
            ...this.options?.metadata.headersMap,
            ...metadata?.headersMap,
          })
        : metadata || this.options.metadata;
    return new Promise((resolve, reject) => {
      grpc.unary(methodDesc, {
        request,
        host: this.host,
        metadata: maybeCombinedMetadata,
        transport: this.options.transport,
        debug: this.options.debug,
        onEnd: function (response) {
          if (response.status === grpc.Code.OK) {
            resolve(response.message);
          } else {
            const err = new Error(response.statusMessage) as any;
            err.code = response.status;
            err.metadata = response.trailers;
            reject(err);
          }
        },
      });
    });
  }

  invoke<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    _request: any,
    metadata: grpc.Metadata | undefined
  ): Observable<any> {
    // Status Response Codes (https://developers.google.com/maps-booking/reference/grpc-api/status_codes)
    const upStreamCodes = [2, 4, 8, 9, 10, 13, 14, 15];
    const DEFAULT_TIMEOUT_TIME: number = 3_000;
    const request = { ..._request, ...methodDesc.requestType };
    const maybeCombinedMetadata =
      metadata && this.options.metadata
        ? new BrowserHeaders({
            ...this.options?.metadata.headersMap,
            ...metadata?.headersMap,
          })
        : metadata || this.options.metadata;
    return new Observable((observer) => {
      const upStream = () => {
        const client = grpc.invoke(methodDesc, {
          host: this.host,
          request,
          transport: this.options.streamingTransport || this.options.transport,
          metadata: maybeCombinedMetadata,
          debug: this.options.debug,
          onMessage: (next) => observer.next(next),
          onEnd: (code: grpc.Code, message: string) => {
            if (code === 0) {
              observer.complete();
            } else if (upStreamCodes.includes(code)) {
              setTimeout(upStream, DEFAULT_TIMEOUT_TIME);
            } else {
              observer.error(new Error(`Error ${code} ${message}`));
            }
          },
        });
        observer.add(() => client.close());
      };
      upStream();
    }).pipe(share());
  }
}

declare var self: any | undefined;
declare var window: any | undefined;
declare var global: any | undefined;
var globalThis: any = (() => {
  if (typeof globalThis !== "undefined") return globalThis;
  if (typeof self !== "undefined") return self;
  if (typeof window !== "undefined") return window;
  if (typeof global !== "undefined") return global;
  throw "Unable to locate global object";
})();

type Builtin =
  | Date
  | Function
  | Uint8Array
  | string
  | number
  | boolean
  | undefined;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? { [K in Exclude<keyof T, "$type">]?: DeepPartial<T[K]> }
  : Partial<T>;

function longToNumber(long: Long): number {
  if (long.gt(Number.MAX_SAFE_INTEGER)) {
    throw new globalThis.Error("Value is larger than Number.MAX_SAFE_INTEGER");
  }
  return long.toNumber();
}

if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
