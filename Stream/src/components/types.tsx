import React, { PropsWithChildren, ReactNode } from 'react';
import { RowRenderComponentType } from '../types';
import { ObjectWithId } from '@shared/types/string';
import { IPaginatorComponent } from './Paginator';
import Pagination from '../model/Pagination';

type ExternalComponentsType = {
  beforeComponent?: React.ReactNode,
  EmptyComponent?: React.ReactNode,
};

export interface BaseListContentProps<T extends ObjectWithId, QueryType extends object = object> extends ExternalComponentsType {
  diKey: symbol,
  idKey: string,
  rowRenderComponent: RowRenderComponentType<T>,
  countPerPage?: number,
  wrapperListClass?: string,
  classNamePagination?: string,
  activeClassName?: string,
  query?: QueryType,
  method?: string,
  hidePaginator?: boolean,
  wrapper?: React.FunctionComponent<PropsWithChildren<ExternalComponentsType>>,
  order?: string,
  paginator?: React.FunctionComponent<PropsWithChildren<IPaginatorComponent>>,
  onChangePaginate?(): void
}

export interface IBaseTableContentProps<T extends ObjectWithId, QueryType extends object = object> extends ExternalComponentsType {
  diKey: symbol,
  method?: string,
  countPerPage?: number,
  order?: string,
  currentPage?: number,
  query?: QueryType,
}

export interface IBaseTableContentChildrenProps<T>{
  children(
    items: T[],
    pagination: Pagination,
    onPageChanged: (page: number, size?: number) => void,
  ): ReactNode
}

export interface ITableMethods<T> {
  removeById(id: number): void
  removeByIndex(index: number): void
  update(item: T): void
  add(item: T): void
}
