import { AsyncContainerModule, interfaces } from 'inversify';
import { IMessageHandler, MessageExternalHandlerKey, MessageItem, MessageModule } from '@shared/message';

export const ToastMessageModule = <T extends interfaces.Newable<IMessageHandler<string, MessageItem>>>(handler: T) => new AsyncContainerModule(async (bind: interfaces.Bind, unbind: interfaces.Unbind) => {
  bind<IMessageHandler<string, MessageItem>>(MessageExternalHandlerKey).to(handler);
  bind<MessageModule>(MessageModule.diKey).to(MessageModule);
});
