import { Observable } from 'rxjs';
import { inject, injectable } from 'inversify';
import { ISecurityCreate } from './dto';
import { ISecurityService } from './service/types';
import { securityServiceDiKey } from './constants';

export interface ISecurityGqlUsecase {
  create(input: ISecurityCreate): Observable<void>,
}

@injectable()
class SecurityGqlUsecase implements ISecurityGqlUsecase {
  public static diKey = Symbol.for('SecurityGqlUsecaseDiKey');

  private service: ISecurityService;

  constructor(
    @inject(securityServiceDiKey) service: ISecurityService,
  ) {
    this.service = service;
  }

  create(input: ISecurityCreate): Observable<void> {
    return this.service.create(input);
  }
}

export default SecurityGqlUsecase;