import { map, Observable, Subscription } from 'rxjs';
import { inject, injectable } from 'inversify';
import User from './models/user';
import { IAppConfig } from '@shared/app/types';
import { AppConfigDiKey } from '@shared/app/constants';
import { IBaseListUsecase, IPageService } from '@shared/stream/types';
import { PageService } from '@shared/stream/service';
import { ListMV } from '@shared/stream/model';
import { adminUserServiceDiKey, userServiceDiKey } from './constants';
import { IAdminUserService, IUserService } from './service/types';
import { ISessionService } from '../session/service/types';
import { sessionServiceDiKey } from '../session';
import { IListOptions } from '@shared/types/dto';
import { IUserFilterModel } from './dto';

export interface IBaseUserUsecase<UserType extends User = User> {
  getByAlias(alias: string): Observable<UserType>,

  update(value: UserType): Observable<UserType | undefined>

  verifyEmail: (token: string, app?: string) => Observable<boolean>,
  getById: (id: number) => Observable<UserType | undefined>,
}

export interface IAdminUserUsecase<UserType extends User = User> extends IBaseUserUsecase<UserType> {
  updateByAdmin(req: UserType): Observable<void>,
  create: (req: UserType) => Observable<void>,
  list(opts: IListOptions, method: string, filter?: IUserFilterModel): Observable<UserType>,
  getFullById: (id: number) => Observable<UserType | undefined>,
}

@injectable()
class BaseUserUsecase<UserType extends User = User> implements IBaseUserUsecase<UserType> {
  public static diKey = Symbol.for('UserUsecaseDiKey');
  protected cfg: IAppConfig;
  protected userService: IUserService<UserType>;
  protected sessionService: ISessionService<UserType>;

  constructor(
    @inject(AppConfigDiKey) cfg: IAppConfig,
    @inject(userServiceDiKey) userService: IUserService<UserType>,
    @inject(sessionServiceDiKey) sessionService: ISessionService<UserType>,
  ) {
    this.cfg = cfg;
    this.userService = userService;
    this.sessionService = sessionService;
  }

  getByAlias(alias: string): Observable<UserType> {
    return this.userService.getByAlias(alias);
  }

  update(value: UserType): Observable<UserType | undefined> {
    value.createdAt = 0;
    value.path = String(value.path);

    return this.userService.update(value)
      .pipe(
        map(token => {
          this.sessionService.update(token);
          return this.sessionService.getCurrentUser();
        }),
      );
  }

  verifyEmail(token: string, app?: string): Observable<boolean> {
    if (!app) {
      app = this.cfg.app;
    }

    return this.userService.verifyEmail(token, app);
  }

  getById(id: number): Observable<UserType | undefined> {
    return this.userService.getById(id);
  }
}

@injectable()
export class AdminUserUsecase<UserType extends User = User> extends BaseUserUsecase<UserType> implements IAdminUserUsecase<UserType>, IBaseListUsecase<UserType> {
  public static diKey = Symbol.for('AdminUserUsecaseDiKey');
  protected adminUserService: IAdminUserService<UserType>;
  private pageService: IPageService<UserType>;

  constructor(
    @inject(AppConfigDiKey) cfg: IAppConfig,
    @inject(userServiceDiKey) userService: IUserService<UserType>,
    @inject(adminUserServiceDiKey) adminUserService: IAdminUserService<UserType>,
    @inject(sessionServiceDiKey) sessionService: ISessionService<UserType>,
  ) {
    super(cfg, userService, sessionService);
    this.adminUserService = adminUserService;
    this.pageService = new PageService(this.adminUserService);
  }

  loadTotalCount(model: ListMV<UserType>, method?: string | undefined, query?: object | undefined): [Observable<number>, Subscription] {
    return this.pageService.loadTotalCount(model, method, query);
  }

  loadItems(model: ListMV<UserType>, opts: IListOptions, method?: string | undefined, query?: object | undefined): [Observable<UserType>, Subscription] {
    return this.pageService.loadPage(model, opts, method, query);
  }

  updateByAdmin(req: UserType): Observable<void> {
    return this.adminUserService.updateByAdmin(req);
  }

  create(req: UserType): Observable<void> {
    return this.adminUserService.create(req);
  }

  list(opts: IListOptions, method: string, filter?: IUserFilterModel): Observable<UserType> {
    return  this.adminUserService.list(opts, method, filter);
  }

  getFullById(id: number): Observable<UserType | undefined> {
    return this.adminUserService.getFullById(id);
  }
}

export default BaseUserUsecase;