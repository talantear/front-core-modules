export interface IPolicyModel {
  id: number,
  ptype: string,
  v0?: string | undefined,
  v1?: string | undefined,
  v2?: string | undefined,
  v3?: string | undefined,
  v4?: string | undefined,
  v5?: string | undefined,
}