import { profileUCDiKey } from './constants';
import { IProfileUsecase } from './types';

export { profileUCDiKey };
export type { IProfileUsecase };