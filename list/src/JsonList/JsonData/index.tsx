import JsonDataView from './Component';
import { JsonDataProps } from './types';
import { useLoadableVM } from '@shared/stream/hooks';
import { useInjection } from 'inversify-react';
import ListUsecase from '../../usecase';
import List from '../../di/model';
import { useMemo } from 'react';
import JsonListVM from '../JsonListVM';

const JsonData = <T extends object>(props: JsonDataProps<T>) => {
  const listUC = useInjection<ListUsecase<List>>(ListUsecase.diKey);

  const vm = useMemo(() => new JsonListVM(listUC), []);
  useLoadableVM(vm, { context: props.context });

  return (
    <JsonDataView vm={vm} {...props} />
  );
};

export default JsonData;
