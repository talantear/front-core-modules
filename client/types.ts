export type ApiMetadata = { [key: string]: string | string[] }

export interface ApiClientMeta {
  context?: string,
  asImpersonate?: boolean
}

export interface ApiClientOptions {
  noCache?: boolean
  cacheType?: string
  metadata?: ApiMetadata;
  silent?: boolean;
  meta?: ApiClientMeta;
  isFile?: boolean;
  host?: string;
  context?: string
}

export interface ApiClientMiddleware {
  ExtraMetadata?: (options?: ApiClientOptions | any) => ApiMetadata | undefined,
  OnRequestStart?: (options?: ApiClientOptions) => void,
  OnRequestFinish?: () => void,
  OnRequestComplete?: (res: any, options?: ApiClientOptions) => void,
  OnRequestError?: (err: Error, options?: ApiClientOptions) => void,
}

export interface IApiConfig {
  getHost: () => string
  getHostWs?: () => string
  getDebug: () => boolean
  getMiddlewares: () => ApiClientMiddleware[]
}