/* eslint-disable */
import { messageTypeRegistry } from "./typeRegistry";
import Long from "long";
import { grpc } from "@improbable-eng/grpc-web";
import _m0 from "protobufjs/minimal";
import { Observable } from "rxjs";
import { Id64Request, StringRequest, Int64Value } from "./commonTypes";
import { ListOptions } from "./paginatior";
import { Empty } from "./google/protobuf/empty";
import { BrowserHeaders } from "browser-headers";
import { share } from "rxjs/operators";
import { ProviderItemModel } from "./providerItem";
import { SignUpItemModel } from "./signupItems";
import { OrganizationModel } from "./organization";

export const protobufPackage = "authMs";

export interface ApplicationModel {
  $type: "authMs.ApplicationModel";
  id: number;
  createdAt: number;
  displayName: string;
  logo: string;
  description: string;
  organizationId: number;
  enableSignUp: boolean;
  providers: ProviderItemModel[];
  signupItems: SignUpItemModel[];
  clientId: string;
  redirectUris: string[];
  termsOfUse: string;
  signupHtml: string;
  signinHtml: string;
  organization?: OrganizationModel | undefined;
  slug: string;
}

const baseApplicationModel: object = {
  $type: "authMs.ApplicationModel",
  id: 0,
  createdAt: 0,
  displayName: "",
  logo: "",
  description: "",
  organizationId: 0,
  enableSignUp: false,
  clientId: "",
  redirectUris: "",
  termsOfUse: "",
  signupHtml: "",
  signinHtml: "",
  slug: "",
};

export const ApplicationModel = {
  $type: "authMs.ApplicationModel" as const,

  encode(
    message: ApplicationModel,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.id !== 0) {
      writer.uint32(8).uint32(message.id);
    }
    if (message.createdAt !== 0) {
      writer.uint32(16).int64(message.createdAt);
    }
    if (message.displayName !== "") {
      writer.uint32(26).string(message.displayName);
    }
    if (message.logo !== "") {
      writer.uint32(34).string(message.logo);
    }
    if (message.description !== "") {
      writer.uint32(42).string(message.description);
    }
    if (message.organizationId !== 0) {
      writer.uint32(48).uint32(message.organizationId);
    }
    if (message.enableSignUp === true) {
      writer.uint32(56).bool(message.enableSignUp);
    }
    for (const v of message.providers) {
      ProviderItemModel.encode(v!, writer.uint32(66).fork()).ldelim();
    }
    for (const v of message.signupItems) {
      SignUpItemModel.encode(v!, writer.uint32(74).fork()).ldelim();
    }
    if (message.clientId !== "") {
      writer.uint32(82).string(message.clientId);
    }
    for (const v of message.redirectUris) {
      writer.uint32(90).string(v!);
    }
    if (message.termsOfUse !== "") {
      writer.uint32(98).string(message.termsOfUse);
    }
    if (message.signupHtml !== "") {
      writer.uint32(106).string(message.signupHtml);
    }
    if (message.signinHtml !== "") {
      writer.uint32(114).string(message.signinHtml);
    }
    if (message.organization !== undefined) {
      OrganizationModel.encode(
        message.organization,
        writer.uint32(122).fork()
      ).ldelim();
    }
    if (message.slug !== "") {
      writer.uint32(130).string(message.slug);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ApplicationModel {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseApplicationModel } as ApplicationModel;
    message.providers = [];
    message.signupItems = [];
    message.redirectUris = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.uint32();
          break;
        case 2:
          message.createdAt = longToNumber(reader.int64() as Long);
          break;
        case 3:
          message.displayName = reader.string();
          break;
        case 4:
          message.logo = reader.string();
          break;
        case 5:
          message.description = reader.string();
          break;
        case 6:
          message.organizationId = reader.uint32();
          break;
        case 7:
          message.enableSignUp = reader.bool();
          break;
        case 8:
          message.providers.push(
            ProviderItemModel.decode(reader, reader.uint32())
          );
          break;
        case 9:
          message.signupItems.push(
            SignUpItemModel.decode(reader, reader.uint32())
          );
          break;
        case 10:
          message.clientId = reader.string();
          break;
        case 11:
          message.redirectUris.push(reader.string());
          break;
        case 12:
          message.termsOfUse = reader.string();
          break;
        case 13:
          message.signupHtml = reader.string();
          break;
        case 14:
          message.signinHtml = reader.string();
          break;
        case 15:
          message.organization = OrganizationModel.decode(
            reader,
            reader.uint32()
          );
          break;
        case 16:
          message.slug = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ApplicationModel {
    const message = { ...baseApplicationModel } as ApplicationModel;
    message.id =
      object.id !== undefined && object.id !== null ? Number(object.id) : 0;
    message.createdAt =
      object.createdAt !== undefined && object.createdAt !== null
        ? Number(object.createdAt)
        : 0;
    message.displayName =
      object.displayName !== undefined && object.displayName !== null
        ? String(object.displayName)
        : "";
    message.logo =
      object.logo !== undefined && object.logo !== null
        ? String(object.logo)
        : "";
    message.description =
      object.description !== undefined && object.description !== null
        ? String(object.description)
        : "";
    message.organizationId =
      object.organizationId !== undefined && object.organizationId !== null
        ? Number(object.organizationId)
        : 0;
    message.enableSignUp =
      object.enableSignUp !== undefined && object.enableSignUp !== null
        ? Boolean(object.enableSignUp)
        : false;
    message.providers = (object.providers ?? []).map((e: any) =>
      ProviderItemModel.fromJSON(e)
    );
    message.signupItems = (object.signupItems ?? []).map((e: any) =>
      SignUpItemModel.fromJSON(e)
    );
    message.clientId =
      object.clientId !== undefined && object.clientId !== null
        ? String(object.clientId)
        : "";
    message.redirectUris = (object.redirectUris ?? []).map((e: any) =>
      String(e)
    );
    message.termsOfUse =
      object.termsOfUse !== undefined && object.termsOfUse !== null
        ? String(object.termsOfUse)
        : "";
    message.signupHtml =
      object.signupHtml !== undefined && object.signupHtml !== null
        ? String(object.signupHtml)
        : "";
    message.signinHtml =
      object.signinHtml !== undefined && object.signinHtml !== null
        ? String(object.signinHtml)
        : "";
    message.organization =
      object.organization !== undefined && object.organization !== null
        ? OrganizationModel.fromJSON(object.organization)
        : undefined;
    message.slug =
      object.slug !== undefined && object.slug !== null
        ? String(object.slug)
        : "";
    return message;
  },

  toJSON(message: ApplicationModel): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    message.createdAt !== undefined && (obj.createdAt = message.createdAt);
    message.displayName !== undefined &&
      (obj.displayName = message.displayName);
    message.logo !== undefined && (obj.logo = message.logo);
    message.description !== undefined &&
      (obj.description = message.description);
    message.organizationId !== undefined &&
      (obj.organizationId = message.organizationId);
    message.enableSignUp !== undefined &&
      (obj.enableSignUp = message.enableSignUp);
    if (message.providers) {
      obj.providers = message.providers.map((e) =>
        e ? ProviderItemModel.toJSON(e) : undefined
      );
    } else {
      obj.providers = [];
    }
    if (message.signupItems) {
      obj.signupItems = message.signupItems.map((e) =>
        e ? SignUpItemModel.toJSON(e) : undefined
      );
    } else {
      obj.signupItems = [];
    }
    message.clientId !== undefined && (obj.clientId = message.clientId);
    if (message.redirectUris) {
      obj.redirectUris = message.redirectUris.map((e) => e);
    } else {
      obj.redirectUris = [];
    }
    message.termsOfUse !== undefined && (obj.termsOfUse = message.termsOfUse);
    message.signupHtml !== undefined && (obj.signupHtml = message.signupHtml);
    message.signinHtml !== undefined && (obj.signinHtml = message.signinHtml);
    message.organization !== undefined &&
      (obj.organization = message.organization
        ? OrganizationModel.toJSON(message.organization)
        : undefined);
    message.slug !== undefined && (obj.slug = message.slug);
    return obj;
  },

  fromPartial(object: DeepPartial<ApplicationModel>): ApplicationModel {
    const message = { ...baseApplicationModel } as ApplicationModel;
    message.id = object.id ?? 0;
    message.createdAt = object.createdAt ?? 0;
    message.displayName = object.displayName ?? "";
    message.logo = object.logo ?? "";
    message.description = object.description ?? "";
    message.organizationId = object.organizationId ?? 0;
    message.enableSignUp = object.enableSignUp ?? false;
    message.providers = (object.providers ?? []).map((e) =>
      ProviderItemModel.fromPartial(e)
    );
    message.signupItems = (object.signupItems ?? []).map((e) =>
      SignUpItemModel.fromPartial(e)
    );
    message.clientId = object.clientId ?? "";
    message.redirectUris = (object.redirectUris ?? []).map((e) => e);
    message.termsOfUse = object.termsOfUse ?? "";
    message.signupHtml = object.signupHtml ?? "";
    message.signinHtml = object.signinHtml ?? "";
    message.organization =
      object.organization !== undefined && object.organization !== null
        ? OrganizationModel.fromPartial(object.organization)
        : undefined;
    message.slug = object.slug ?? "";
    return message;
  },
};

messageTypeRegistry.set(ApplicationModel.$type, ApplicationModel);

/** Represents application methods. */
export interface Application {
  /** / Create application */
  Create(
    request: DeepPartial<ApplicationModel>,
    metadata?: grpc.Metadata
  ): Promise<ApplicationModel>;
  /** / Update application */
  Update(
    request: DeepPartial<ApplicationModel>,
    metadata?: grpc.Metadata
  ): Promise<ApplicationModel>;
  /** / GetById application */
  GetById(
    request: DeepPartial<Id64Request>,
    metadata?: grpc.Metadata
  ): Promise<ApplicationModel>;
  /** / List application */
  List(
    request: DeepPartial<ListOptions>,
    metadata?: grpc.Metadata
  ): Observable<ApplicationModel>;
  /** / Count application */
  Count(
    request: DeepPartial<Empty>,
    metadata?: grpc.Metadata
  ): Promise<Int64Value>;
  /** / Remove application */
  Remove(
    request: DeepPartial<Id64Request>,
    metadata?: grpc.Metadata
  ): Promise<Empty>;
  /** / Get by client id application */
  GetByClientId(
    request: DeepPartial<StringRequest>,
    metadata?: grpc.Metadata
  ): Promise<ApplicationModel>;
}

export class ApplicationClientImpl implements Application {
  private readonly rpc: Rpc;

  constructor(rpc: Rpc) {
    this.rpc = rpc;
    this.Create = this.Create.bind(this);
    this.Update = this.Update.bind(this);
    this.GetById = this.GetById.bind(this);
    this.List = this.List.bind(this);
    this.Count = this.Count.bind(this);
    this.Remove = this.Remove.bind(this);
    this.GetByClientId = this.GetByClientId.bind(this);
  }

  Create(
    request: DeepPartial<ApplicationModel>,
    metadata?: grpc.Metadata
  ): Promise<ApplicationModel> {
    return this.rpc.unary(
      ApplicationCreateDesc,
      ApplicationModel.fromPartial(request),
      metadata
    );
  }

  Update(
    request: DeepPartial<ApplicationModel>,
    metadata?: grpc.Metadata
  ): Promise<ApplicationModel> {
    return this.rpc.unary(
      ApplicationUpdateDesc,
      ApplicationModel.fromPartial(request),
      metadata
    );
  }

  GetById(
    request: DeepPartial<Id64Request>,
    metadata?: grpc.Metadata
  ): Promise<ApplicationModel> {
    return this.rpc.unary(
      ApplicationGetByIdDesc,
      Id64Request.fromPartial(request),
      metadata
    );
  }

  List(
    request: DeepPartial<ListOptions>,
    metadata?: grpc.Metadata
  ): Observable<ApplicationModel> {
    return this.rpc.invoke(
      ApplicationListDesc,
      ListOptions.fromPartial(request),
      metadata
    );
  }

  Count(
    request: DeepPartial<Empty>,
    metadata?: grpc.Metadata
  ): Promise<Int64Value> {
    return this.rpc.unary(
      ApplicationCountDesc,
      Empty.fromPartial(request),
      metadata
    );
  }

  Remove(
    request: DeepPartial<Id64Request>,
    metadata?: grpc.Metadata
  ): Promise<Empty> {
    return this.rpc.unary(
      ApplicationRemoveDesc,
      Id64Request.fromPartial(request),
      metadata
    );
  }

  GetByClientId(
    request: DeepPartial<StringRequest>,
    metadata?: grpc.Metadata
  ): Promise<ApplicationModel> {
    return this.rpc.unary(
      ApplicationGetByClientIdDesc,
      StringRequest.fromPartial(request),
      metadata
    );
  }
}

export const ApplicationDesc = {
  serviceName: "authMs.Application",
};

export const ApplicationCreateDesc: UnaryMethodDefinitionish = {
  methodName: "Create",
  service: ApplicationDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return ApplicationModel.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...ApplicationModel.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const ApplicationUpdateDesc: UnaryMethodDefinitionish = {
  methodName: "Update",
  service: ApplicationDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return ApplicationModel.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...ApplicationModel.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const ApplicationGetByIdDesc: UnaryMethodDefinitionish = {
  methodName: "GetById",
  service: ApplicationDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return Id64Request.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...ApplicationModel.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const ApplicationListDesc: UnaryMethodDefinitionish = {
  methodName: "List",
  service: ApplicationDesc,
  requestStream: false,
  responseStream: true,
  requestType: {
    serializeBinary() {
      return ListOptions.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...ApplicationModel.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const ApplicationCountDesc: UnaryMethodDefinitionish = {
  methodName: "Count",
  service: ApplicationDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return Empty.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...Int64Value.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const ApplicationRemoveDesc: UnaryMethodDefinitionish = {
  methodName: "Remove",
  service: ApplicationDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return Id64Request.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...Empty.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const ApplicationGetByClientIdDesc: UnaryMethodDefinitionish = {
  methodName: "GetByClientId",
  service: ApplicationDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return StringRequest.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...ApplicationModel.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

interface UnaryMethodDefinitionishR
  extends grpc.UnaryMethodDefinition<any, any> {
  requestStream: any;
  responseStream: any;
}

type UnaryMethodDefinitionish = UnaryMethodDefinitionishR;

interface Rpc {
  unary<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    request: any,
    metadata: grpc.Metadata | undefined
  ): Promise<any>;
  invoke<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    request: any,
    metadata: grpc.Metadata | undefined
  ): Observable<any>;
}

export class GrpcWebImpl {
  private host: string;
  private options: {
    transport?: grpc.TransportFactory;
    streamingTransport?: grpc.TransportFactory;
    debug?: boolean;
    metadata?: grpc.Metadata;
  };

  constructor(
    host: string,
    options: {
      transport?: grpc.TransportFactory;
      streamingTransport?: grpc.TransportFactory;
      debug?: boolean;
      metadata?: grpc.Metadata;
    }
  ) {
    this.host = host;
    this.options = options;
  }

  unary<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    _request: any,
    metadata: grpc.Metadata | undefined
  ): Promise<any> {
    const request = { ..._request, ...methodDesc.requestType };
    const maybeCombinedMetadata =
      metadata && this.options.metadata
        ? new BrowserHeaders({
            ...this.options?.metadata.headersMap,
            ...metadata?.headersMap,
          })
        : metadata || this.options.metadata;
    return new Promise((resolve, reject) => {
      grpc.unary(methodDesc, {
        request,
        host: this.host,
        metadata: maybeCombinedMetadata,
        transport: this.options.transport,
        debug: this.options.debug,
        onEnd: function (response) {
          if (response.status === grpc.Code.OK) {
            resolve(response.message);
          } else {
            const err = new Error(response.statusMessage) as any;
            err.code = response.status;
            err.metadata = response.trailers;
            reject(err);
          }
        },
      });
    });
  }

  invoke<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    _request: any,
    metadata: grpc.Metadata | undefined
  ): Observable<any> {
    // Status Response Codes (https://developers.google.com/maps-booking/reference/grpc-api/status_codes)
    const upStreamCodes = [2, 4, 8, 9, 10, 13, 14, 15];
    const DEFAULT_TIMEOUT_TIME: number = 3_000;
    const request = { ..._request, ...methodDesc.requestType };
    const maybeCombinedMetadata =
      metadata && this.options.metadata
        ? new BrowserHeaders({
            ...this.options?.metadata.headersMap,
            ...metadata?.headersMap,
          })
        : metadata || this.options.metadata;
    return new Observable((observer) => {
      const upStream = () => {
        const client = grpc.invoke(methodDesc, {
          host: this.host,
          request,
          transport: this.options.streamingTransport || this.options.transport,
          metadata: maybeCombinedMetadata,
          debug: this.options.debug,
          onMessage: (next) => observer.next(next),
          onEnd: (code: grpc.Code, message: string) => {
            if (code === 0) {
              observer.complete();
            } else if (upStreamCodes.includes(code)) {
              setTimeout(upStream, DEFAULT_TIMEOUT_TIME);
            } else {
              observer.error(new Error(`Error ${code} ${message}`));
            }
          },
        });
        observer.add(() => client.close());
      };
      upStream();
    }).pipe(share());
  }
}

declare var self: any | undefined;
declare var window: any | undefined;
declare var global: any | undefined;
var globalThis: any = (() => {
  if (typeof globalThis !== "undefined") return globalThis;
  if (typeof self !== "undefined") return self;
  if (typeof window !== "undefined") return window;
  if (typeof global !== "undefined") return global;
  throw "Unable to locate global object";
})();

type Builtin =
  | Date
  | Function
  | Uint8Array
  | string
  | number
  | boolean
  | undefined;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? { [K in Exclude<keyof T, "$type">]?: DeepPartial<T[K]> }
  : Partial<T>;

function longToNumber(long: Long): number {
  if (long.gt(Number.MAX_SAFE_INTEGER)) {
    throw new globalThis.Error("Value is larger than Number.MAX_SAFE_INTEGER");
  }
  return long.toNumber();
}

if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
