import { ApiClientMiddleware, ApiClientOptions, ApiMetadata } from '@shared/base-client/types';
import { ISessionCookieReader } from './cookie';

class SessionMiddleware implements ApiClientMiddleware {
  private storage: ISessionCookieReader;
  private readonly app: string;

  constructor(app: string, sessionService: ISessionCookieReader) {
    this.app = app;
    this.storage = sessionService;
  }

  ExtraMetadata(options: ApiClientOptions | undefined): ApiMetadata | undefined {
    return {
      ...options?.metadata,
      token: this.storage.refreshToken(this.app) || '',
    };
  }
}

export default SessionMiddleware;