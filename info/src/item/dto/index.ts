import { ListOptions } from '@shared/stream/types';

export interface ISectionItemModel {
  content?: string,
  createdAt: number,
  date?: number,
  displayLanding?: boolean,
  id: number,
  image?: string,
  index?: number,
  language?: string,
  link?: string,
  sectionId: number,
  speaker?: string,
  time?: string,
  title?: string,
  topic?: string,
}

export interface IListItems {
  landingShow?: boolean,
  opts?: ListOptions,
  slug?: string,
}