import React, { FunctionComponent, useMemo } from 'react';
import Form from '@rjsf/antd';
import { isObject } from '@s-libs/micro-dash';
import validator from '@rjsf/validator-ajv8';
import GridHorizontal from '../Widgets/GridHorizontal';

const log = (type: string) => console.log.bind(console, type);

interface IFormEdit {
  jsonSchema: any,
  uiSchema: any,
  context: string,
  data: object | undefined
  parentId?: string
  asObject?: boolean
  onSubmit(value: object): void
}

const FormEdit: FunctionComponent<IFormEdit> = (props) => {
  const { jsonSchema, uiSchema, data, parentId, onSubmit, asObject } = props;

  const widgets = {
    // 'dndUploadImage': UploadImageWidget,
  }

  const fields = {
    'gridHorizontal': GridHorizontal,
  };

  const formData = useMemo(() => {
    if (parentId && Array.isArray(data)) {
      if (asObject) {
        return data.find(i => i.parentId == parentId);
      } else {
        return data.filter(i => i.parentId == parentId);
      }
    } else {
      return data;
    }
  }, [data, parentId, asObject]);

  const mappingArrayParentId = (data: typeof jsonSchema, formData: typeof jsonSchema) => {
    const oldData = data.filter((i: { parentId: string | undefined; }) => i.parentId != parentId);
    const newData = formData.map((i: { parentId: any; }) => (parentId && !i?.parentId) ? { ...i, parentId } : i);

    return [...oldData, ...newData];
  };

  const mappingObjectParentId = (data: typeof jsonSchema, formData: typeof jsonSchema) => {
    const oldObject = data.find((i: { parentId: any; }) => i.parentId == formData.parentId);

    if (oldObject) {
      return data.map((i: { parentId: any; }) => i.parentId == formData.parentId ? formData : i);
    } else {
      return [...data, { ...formData, parentId }];
    }
  };

  const onBeforeSubmit = (formData: typeof jsonSchema) => {
    if (parentId && Array.isArray(data)) {
      if (asObject && isObject(formData)) {
        return mappingObjectParentId(data || [], formData || {});
      } else {
        return mappingArrayParentId(data || [], formData || []);
      }
    } else {
      return formData;
    }
  };

  return (
    <Form
      noHtml5Validate
      schema={jsonSchema}
      uiSchema={uiSchema}
      formData={formData}
      fields={fields}
      widgets={widgets}
      validator={validator}
      onSubmit={(_data: any) => onSubmit(onBeforeSubmit(_data.formData))}
      onChange={log('changed')}
      onError={log('errors')}
    />
  );
};

export default FormEdit;
