import { ContainerModule, interfaces } from 'inversify';
import FilesUsecase from '../usecase';
import { IFilesService } from '../service/types';
import { fileServiceDiKey } from '../constants';

export const FilesModule = <T extends interfaces.Newable<IFilesService>>(service: T) => new ContainerModule((bind: interfaces.Bind) => {
  bind<IFilesService>(fileServiceDiKey).to(service);
  bind<FilesUsecase>(FilesUsecase.diKey).to(FilesUsecase).inSingletonScope();
});
