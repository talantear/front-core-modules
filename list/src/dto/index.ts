export interface IListModel {
  context: string,
  createdAt?: number,
  id: number,
  meta?: string,
  title: string,
}

export type ListModelInput = {
  context: string;
  createdAt?: number;
  id?: number;
  meta?: string;
  title: string;
};
