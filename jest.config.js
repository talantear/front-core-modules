const customJestConfig = {
  moduleDirectories: ['node_modules', '<rootDir>/'],
  testEnvironment: 'jest-environment-jsdom',
  moduleNameMapper: {
    '^@modules/(.*)$': '<rootDir>/$1',
  }
}

module.exports = customJestConfig