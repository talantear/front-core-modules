import { Button } from 'antd';
import range from 'lodash/range';
import { observer } from 'mobx-react-lite';
import React from 'react';
import Pagination from '../model/Pagination';

export type IPaginatorComponent = {
  paginator: Pagination;
  onPageChanged: (page: number) => void;
  classNamePagination?: string
  activeClassName?: string
}

interface IPages {
  current: number;
  count: number;
  onPageChanged: (page: number) => void;
  activeClassName?: string;
  classNamePagination?: string;
}

const Pages = ({ count, current, activeClassName, onPageChanged, classNamePagination }: IPages) => (
  <div>
    {range(count).map((num) => {
      return <Button
        key={`paginator-item-${num}`}
        className={`${classNamePagination} ${current === num && activeClassName}`}
        type={(current === num) ? 'link' : 'text'}
        onClick={() => onPageChanged(num)}
      >
        {num + 1}
      </Button>;
    })}
  </div>
);

function PaginatorComponent(props: IPaginatorComponent) {
  const {
    paginator,
    onPageChanged,
    activeClassName,
    classNamePagination,
  } = props;

  if (!paginator.visible) {
    return null;
  }

  return (
    <Pages
      count={paginator.pagesCount}
      current={paginator.page}
      onPageChanged={onPageChanged}
      activeClassName={activeClassName}
      classNamePagination={classNamePagination}
    />
  );
}

export default observer(PaginatorComponent);
