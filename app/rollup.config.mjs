import typeScript from 'rollup-plugin-typescript2';
import { visualizer } from 'rollup-plugin-visualizer';
import { uglify } from 'rollup-plugin-uglify';

export default {
  input: 'constants.ts',
  output: [
    { format: 'esm', dir: 'dist', exports: 'auto', sourcemap: false, validate: true, preserveModules: true },
  ],
  plugins: [
    typeScript({
      clean: true,
      check: true,
    }),
    visualizer({ gzipSize: true, open: false }),
    uglify(),
  ]
};
