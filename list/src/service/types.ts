import { Observable, Subject } from 'rxjs';
import List from '../di/model';
import { IListEditData } from '../JsonList/types';
import { IListModel } from '../dto';

export interface IListService<MetaData> {
  update(input: IListModel): Observable<List<MetaData> | undefined>;
  create(input: IListModel): Observable<List<MetaData> | undefined>;
  listByContext(context: string): Observable<List<MetaData>>;
  getByContext(context: string): Observable<List<MetaData> | undefined>;
  listByContexts(contexts: string[]): Observable<List<MetaData>>;
  removeById(id: number): Observable<void>;
  getSubject(): Subject<IListEditData>;
  getSubscriberSubject(context: string): Subject<List<MetaData>>;
  getVisible(): Subject<boolean>;
  setVisible(value: boolean): void;
  initSubject(context: string): void;
}

export interface ISubscribeSubject<MetaData = {}> {
  [context: string]: Subject<List<MetaData>>
}