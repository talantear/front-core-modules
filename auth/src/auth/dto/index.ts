export interface IRegisterResp {
  nonce?: string | undefined;
}

export interface IRegAndLoginReq {
  type: string;
  password?: string | undefined;
  email?: string | undefined;
  applicationSlug: string;
  provider?: string | undefined;
  state?: string | undefined;
  redirectUri?: string | undefined;
  lang?: string | undefined;
  parent?: string | undefined;
  extra?: string | undefined;
  code?: string | undefined;
}

export interface ILoginResp {
  accessToken: string;
  refreshToken: string;
}