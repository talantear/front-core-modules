import { Observable } from 'rxjs';
import { IRecoveryReq, IChangePassReq } from '../dto';
import { IStringRequest } from '@shared/types/dto';

export interface IRecoveryService {
  recovery(request: IRecoveryReq): Observable<void>,
  checkToken(request: IStringRequest): Observable<void>,
  changePass(request: IChangePassReq): Observable<void>,
}