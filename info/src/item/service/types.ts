import { IExternalService } from '@shared/stream/types';
import { Observable } from 'rxjs';
import { IListItems, ISectionItemModel } from '../dto';
import { ListOptions } from '@shared/stream/types';

export interface IInfoSectionItemService extends IExternalService<ISectionItemModel> {
  create(input: ISectionItemModel): Observable<ISectionItemModel>,
  update(input: ISectionItemModel): Observable<ISectionItemModel>,
  delete(id: number): Observable<void>,
  getById(id: number): Observable<ISectionItemModel>,
  loadItems(query: IListItems): Observable<ISectionItemModel>,
  list(opts: ListOptions): Observable<ISectionItemModel>,
  bySection(sectionId: number): Observable<ISectionItemModel>,
  count(slug: string): Observable<number>,
}