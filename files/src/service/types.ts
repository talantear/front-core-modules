import { IFilesUploadRes } from '../dto';

export interface IFilesService {
  upload: (file: File) => Promise<IFilesUploadRes>,
  get: (key: string) => Promise<IFilesUploadRes>,
  getFileInfo: (key: string) => Promise<IFilesUploadRes>,
}