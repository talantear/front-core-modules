import { AsyncContainerModule, interfaces } from 'inversify';
import CurrencyUsecase from './usecase';

export const CurrencyModule = new AsyncContainerModule(async (bind: interfaces.Bind, unbind: interfaces.Unbind) => {
  bind<CurrencyUsecase>(CurrencyUsecase.diKey).to(CurrencyUsecase).inSingletonScope();
});
