import { inject, injectable } from 'inversify';
import RoleDependenciesController from './controller';
import { firstValueFrom, toArray } from 'rxjs';
import { roleServiceDiKey } from './constants';
import { IRoleService } from './service/types';

export interface IRoleUsecase {
  isInherit: (checkedRole: string, role: string, domain: string) => boolean,
  initialize: () => void,
}

@injectable()
class RoleUsecase implements IRoleUsecase {
  public static diKey = Symbol.for('RoleDependenciesUsecaseKey');

  private service: IRoleService;
  private controller: RoleDependenciesController;

  constructor(
    @inject(roleServiceDiKey) service: IRoleService,
  ) {
    this.service = service;
    this.controller = new RoleDependenciesController();
  }

  isInitialized(): boolean {
    return !!this.controller.items.length;
  }

  initialize() {
    return firstValueFrom(this.service.initialize().pipe(toArray())).then(res => this.controller.load(res));
  }

  isInherit(checkedRole: string, role: string, domain: string): boolean {
    return this.controller.check(checkedRole, role, domain);
  }
}

export default RoleUsecase;