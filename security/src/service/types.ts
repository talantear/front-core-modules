import { ISecurityCreate } from '../dto';
import { Observable } from 'rxjs';

export interface ISecurityService {
  create(input: ISecurityCreate): Observable<void>,
}