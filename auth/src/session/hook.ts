import { useInjection, useOptionalInjection } from 'inversify-react';
import SessionUsecase from './usecase';
import { useEffect, useState } from 'react';
import User from '../user/models/user';
import { profileUCDiKey, IProfileUsecase } from '../profile';
import { of, zip } from 'rxjs';

const useSession = <T extends object = {}>(loadProfile: boolean = true) => {
  const sessionUsecase = useInjection<SessionUsecase<T>>(SessionUsecase.diKey);
  const profileUsecase = useOptionalInjection<IProfileUsecase<T>>(profileUCDiKey);
  const [user, setUser] = useState<User<T>>();

  useEffect(() => {
    //TODO: why forkJoin not work?
    const sub = zip(sessionUsecase.getCurrentUser$(), loadProfile && profileUsecase ? profileUsecase.load() : of({} as T))
      .subscribe(([user, profile]) => {
        if (user) {
          loadProfile && user.setProfile(profile);
          setUser(user);
        }
      });

    return () => sub.unsubscribe();
  }, []);

  return user;
};

export default useSession;