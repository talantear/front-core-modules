import typeScript from 'rollup-plugin-typescript2';

export default [
  {
    input: 'model.ts',
    output: [
      { format: 'esm', dir: 'dist', exports: 'auto', sourcemap: false, validate: true },
    ],
    plugins: [
      typeScript({
        clean: true,
        check: true,
        sourceMap: false,
      }),
    ]
  },
];
