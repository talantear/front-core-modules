import { inject, injectable } from 'inversify';
import HttpClient from '@shared/http-client/model';
import { ApiHttpClientKey } from '@shared/http-client/constants';
import { ResultCountRes, ResultItemRes, ResultListRes, BaseCommandProps } from '@shared/types/http';
import { from, map, mergeMap, Observable } from 'rxjs';
import InfoItem from '../../model';
import { StreamHandler } from '@shared/stream/service';
import { IInfoSectionItemService } from '../types';
import { ListOptions } from '@shared/stream/types';
import { IListItems, ISectionItemModel } from '../../dto';

@injectable()
class InfoItemHttpService extends StreamHandler<InfoItem> implements IInfoSectionItemService {
  private api: HttpClient;

  public constructor(
    @inject(ApiHttpClientKey) api: HttpClient,
  ) {
    super();
    this.api = api;
    this.add('default', this.streamDefault);
    this.add('plain', this.plainStream);
  }

  private streamDefault = {
    stream: (opts?: ListOptions, method?: string, query?: IListItems): Observable<InfoItem> => {
      const input: IListItems = {
        landingShow: query?.landingShow,
        slug: query?.slug as string,
        opts: query?.opts,
      };

      if (query?.opts) {
        input.opts = query.opts;
      } else {
        input.opts = opts;
      }

      return this.loadItems(input);
    },
    loadCount: (method?: string, query?: { slug: string }): Observable<number> => {
      return this.count(query?.slug || "");
    },
  };

  private plainStream = {
    stream: (opts: ListOptions, method?: string, query?: object): Observable<InfoItem> => {
      return this.list(opts);
    },
    loadCount: (method?: string, query?: object): Observable<number> => this.streamDefault.loadCount(),
  };

  count(slug: string): Observable<number> {
    return this.api.$request<object, ResultCountRes>(
      `/api/v1/info/item/count/${slug}`, {}, { method: 'GET' }
    ).pipe(map(i => i.count));
  }

  create(value: ISectionItemModel): Observable<InfoItem> {
    return this.api.$request<object, ResultItemRes<ISectionItemModel>>(
      `/api/v1/info/item`, value, { method: 'POST' }
    ).pipe(map(i => new InfoItem(i.item)));
  }

  delete(id: number): Observable<void> {
    return this.api.$request<object, void & BaseCommandProps>(
      `/api/v1/info/item/${id}`, {}, { method: 'DELETE' }
    );
  }

  getById(id: number): Observable<InfoItem> {
    return this.api.$request<object, ResultItemRes<ISectionItemModel>>(
      `/api/v1/info/item/${id}`, {}, { method: 'GET' }
    ).pipe(map(i => new InfoItem(i.item)));
  }

  loadItems(query: IListItems): Observable<InfoItem> {
    return this.api.$request<object, ResultListRes<ISectionItemModel>>(
      `/api/v1/info/item/list`, query, { method: 'POST' }
    ).pipe(mergeMap(i => from(i.list.map(x => new InfoItem(x)))));
  }

  bySection(sectionId: number): Observable<InfoItem> {
    return this.api.$request<object, ResultListRes<ISectionItemModel>>(
      `/api/v1/info/item/load-in-section/${sectionId}`, {}, { method: 'GET' }
    ).pipe(mergeMap(i => from(i.list.map(x => new InfoItem(x)))));
  }

  list(opts: ListOptions): Observable<InfoItem> {
    return this.api.$request<object, ResultListRes<ISectionItemModel>>(
      `/api/v1/info/item/plain-list`, opts, { method: 'POST' }
    ).pipe(mergeMap(i => from(i.list.map(x => new InfoItem(x)))));
  }

  update(input: ISectionItemModel): Observable<InfoItem> {
    return this.api.$request<object, ResultItemRes<ISectionItemModel>>(
      `/api/v1/info/item`, input, { method: 'PUT' }
    ).pipe(map(i => new InfoItem(i.item)));
  }
}

export default InfoItemHttpService;