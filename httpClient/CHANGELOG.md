# @shared/http-client

## 1.1.0

### Minor Changes

- Update packages

### Patch Changes

- Updated dependencies
  - @shared/base-client@1.1.0
  - @shared/types@1.1.0

## 1.0.1

### Patch Changes

- Updaated exports
- Updated dependencies
  - @shared/base-client@1.0.1
  - @shared/types@1.0.1
