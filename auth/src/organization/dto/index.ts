export interface IOrganization {
  id: number;
  displayName: string;
  enableSoftDeletion: boolean;
  createdAt: number;
  slug: string;
  jwtSecret: string;
  jwtExpiresIn: number;
  passwordSaltSize: number;
  passwordMinSize: number;
}