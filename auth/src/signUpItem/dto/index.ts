import { ISignUpModel } from '../../signUp/dto';

export interface ISignUpItemModel {
  id: number;
  title: string;
  visible: boolean;
  required: boolean;
  prompted: boolean;
  rule: string;
  applicationId: number;
  signupId: number;
  signup?: ISignUpModel;
}