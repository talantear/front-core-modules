import { inject, injectable } from 'inversify';
import { ApiClient } from '@shared/grpc-client/model';
import {
  GetByStringReq,
  UserCountDesc, UserCountReq, UserCreateByAdminDesc, UserFilterModel,
  UserGetByAliasDesc,
  UserGetDesc, UserGetFullByIdDesc, UserListDesc, UserListReq,
  UserUpdateByAdminDesc,
  UserUpdateDesc,
  UserVerifyByTokenDesc, VerifyByTokenReq,
} from '../../../proto/user';
import { UserModel } from '../../../proto/authCommon';
import { Id64Request } from '../../../proto/commonTypes';
import { map, Observable } from 'rxjs';
import { IAppConfig } from '@shared/app/types';
import { AppConfigDiKey } from '@shared/app/constants';
import User from '../../models/user';
import { StreamHandler } from '@shared/stream/service';
import { IAdminUserService, IUserService } from '../types';
import { IListOptions } from '@shared/types/dto';
import { IUserFilterModel } from '../../dto';

@injectable()
class UserService<UserType extends User = User> extends StreamHandler<UserType> implements IUserService<UserType> {
  protected api: ApiClient;
  protected appConfig: IAppConfig;

  public constructor(
    @inject(ApiClient.diKey) api: ApiClient,
    @inject(AppConfigDiKey) appConfig: IAppConfig,
  ) {
    super();
    this.api = api;
    this.appConfig = appConfig;
  }

  getByAlias(alias: string): Observable<UserType> {
    const req = GetByStringReq.fromPartial({
      appSlug: this.appConfig.app,
      value: alias,
    })
    return this.api.unary$(
      UserGetByAliasDesc,
      {
        obj: req,
        coder: GetByStringReq,
      },
      { host: this.appConfig.authUrlServer },
    );
  }

  update(value: UserType): Observable<string> {
    const req = UserModel.fromPartial(value)
    return this.api.unary$(
      UserUpdateDesc,
      {
        obj: req,
        coder: UserModel,
      },
      { host: this.appConfig.authUrlServer },
    ).pipe(map(i => i.value));
  }

  verifyEmail(token: string, app: string): Observable<boolean> {
    return this.api.unary$(
      UserVerifyByTokenDesc,
      {
        obj: VerifyByTokenReq.fromPartial({ token, app }),
        coder: VerifyByTokenReq,
      },
      { host: this.appConfig.authUrlServer },
    );
  }

  getById(id: number): Observable<UserType | undefined> {
    return this.api.unary$(
      UserGetDesc,
      {
        obj: Id64Request.fromPartial({ id }),
        coder: Id64Request,
      },
      { host: this.appConfig.authUrlServer },
    );
  }
}

@injectable()
export class AdminUserService<UserType extends User = User> extends UserService<UserType> implements IAdminUserService<UserType> {
  public constructor(
    @inject(ApiClient.diKey) api: ApiClient,
    @inject(AppConfigDiKey) appConfig: IAppConfig,
  ) {
    super(api, appConfig);
    this.add('default', this.streamDefault);
  }

  private streamDefault = {
    stream: (opts: IListOptions, method: string, filter?: IUserFilterModel): Observable<UserType> => {
      return this.api.stream$(
        UserListDesc,
        { obj: UserListReq.fromPartial({
            opts,
            filter,
          }), coder: UserListReq },
        { host: this.appConfig.authUrlServer },
      ).pipe();
    },
    loadCount: (method: string, filter?: UserFilterModel): Observable<number> => {
      return this.api.unary$(
        UserCountDesc,
        {
          obj: UserCountReq.fromPartial({ filter }),
          coder: UserCountReq,
        },
        { host: this.appConfig.authUrlServer },
      ).pipe(map(res => res.value));
    }
  }

  updateByAdmin(req: UserType): Observable<void> {
    return this.api.unary$(
      UserUpdateByAdminDesc,
      {
        obj: UserModel.fromPartial(req.object),
        coder: UserModel,
      },
      { host: this.appConfig.authUrlServer },
    );
  }

  create(req: UserType): Observable<void> {
    return this.api.unary$(
      UserCreateByAdminDesc,
      {
        obj: UserModel.fromPartial(req.object),
        coder: UserModel,
      },
      { host: this.appConfig.authUrlServer },
    );
  }

  list(opts: IListOptions, method: string, filter?: IUserFilterModel): Observable<UserType> {
    return this.streamDefault.stream(opts, method, filter);
  }

  getFullById(id: number): Observable<UserType | undefined> {
    return this.api.unary$(
      UserGetFullByIdDesc,
      {
        obj: Id64Request.fromPartial({ id }),
        coder: Id64Request,
      },
      { host: this.appConfig.authUrlServer },
    );
  }
}

export default UserService;