import { IImageUploadRes } from '../models';

export interface IImageService {
  upload: (file: File) => Promise<IImageUploadRes>,
  get: (width: number, height: number, key: string) => Promise<File>,
}