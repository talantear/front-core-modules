import { IApplication } from '../dto';
import { Observable } from 'rxjs';
import { IId64Request, IStringRequest } from '@shared/types/dto';
import { IExternalService } from '@shared/stream/types';

export interface IApplicationService extends IExternalService<IApplication> {
  create(request: IApplication): Observable<IApplication>,
  update(request: IApplication): Observable<IApplication>,
  getById(request: IId64Request): Observable<IApplication>,
  getByClientId(request: IStringRequest): Observable<IApplication>,
  remove(request: IId64Request): Observable<void>,
  addCache(key: string, value: IApplication): void
  getCache(key: string): IApplication
}