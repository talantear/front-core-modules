import { makeAutoObservable } from 'mobx';

export interface ICacheRepoHandler<T> {
  addCache(key: string, value: T): void,
  getCache(key: string): T,
}

class CacheRepository<T extends object> implements ICacheRepoHandler<T> {
  private readonly items: { [key: string]: T } = {}

  constructor() {
    makeAutoObservable(this);
  }

  addCache(key: string, value: T): void {
    this.items[key] = value;
  }

  getCache(key: string): T {
    return this.items[key];
  }
}

export default CacheRepository;