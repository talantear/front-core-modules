import { ApiClientMiddleware, ApiClientOptions } from '@shared/base-client/types';
import { IMessageHandler, MessageItem } from '@shared/message';

class MessageMiddleware implements ApiClientMiddleware {
  private handler: IMessageHandler<string, MessageItem>;

  constructor(handler: IMessageHandler<string, MessageItem>) {
    this.handler = handler;
  }

  OnRequestError(err: Error, options?: ApiClientOptions) {
    this.handler.sendError({ title: err.message });
  }
}

export default MessageMiddleware;