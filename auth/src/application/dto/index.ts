import { IOrganization } from '../../organization/dto';
import { IProviderItem } from '../../providerItem/dto';
import { ISignUpItemModel } from '../../signUpItem/dto';

export interface IApplication {
  id: number;
  createdAt: number;
  displayName: string;
  logo: string;
  description: string;
  organizationId: number;
  enableSignUp: boolean;
  providers: IProviderItem[];
  signupItems: ISignUpItemModel[];
  clientId: string;
  redirectUris: string[];
  termsOfUse: string;
  signupHtml: string;
  signinHtml: string;
  organization?: IOrganization | undefined;
  slug: string;
}