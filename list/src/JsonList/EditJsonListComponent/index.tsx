import { FunctionComponent, useState } from 'react';
import { useInjection } from 'inversify-react';
import ListUsecase from '../../usecase';
import List from '../../di/model';
import JsonListVM from '../JsonListVM';
import EditJsonListComponentView from './Component';

const EditJsonListComponent: FunctionComponent = () => {
  const listUC = useInjection<ListUsecase<List>>(ListUsecase.diKey);
  const [vm] = useState(new JsonListVM(listUC));

  return (
    <EditJsonListComponentView
      vm={vm}
    />
  );
};

export default EditJsonListComponent;