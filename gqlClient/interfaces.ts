import { ApiClientOptions } from '@shared/base-client/types';
import { Observable } from 'rxjs';
import { AnyVariables } from '@urql/core';

export default interface IGqlClient<Doc> {
  mutation: <TData, TVariables extends AnyVariables>(mutation: Doc, variables?: TVariables, options?: ApiClientOptions) => Promise<TData | null | undefined>
  subscribe: <TData, TVariables extends AnyVariables>(query: Doc, variables?: TVariables, options?: ApiClientOptions) => Observable<TData>
  query: <TData, TVariables extends AnyVariables>(query: Doc, variables?: TVariables, options?: ApiClientOptions) => Promise<TData | null | undefined>
}
