import { inject, injectable } from 'inversify';
import { StreamHandler } from '@shared/stream/service';
import {
  InfoSectionItemCountDocument,
  InfoSectionItemCountQuery,
  InfoSectionItemCountQueryVariables,
  InfoSectionItemCreateDocument,
  InfoSectionItemCreateMutation,
  InfoSectionItemCreateMutationVariables,
  InfoSectionItemDeleteDocument,
  InfoSectionItemDeleteMutation,
  InfoSectionItemDeleteMutationVariables,
  InfoSectionItemGetByIdDocument,
  InfoSectionItemGetByIdQuery,
  InfoSectionItemGetByIdQueryVariables,
  InfoSectionItemListDocument,
  InfoSectionItemListPlainDocument,
  InfoSectionItemListPlainQuery,
  InfoSectionItemListPlainQueryVariables,
  InfoSectionItemListQuery,
  InfoSectionItemListQueryVariables,
  InfoSectionItemUpdateDocument,
  InfoSectionItemUpdateMutation,
  InfoSectionItemUpdateMutationVariables,
  ListItemsReq,
  ListOptions,
  SectionItemModelInput,
} from '../../../gql/schemas/info-gql';
import { from, map, Observable } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import InfoItem from '../../model';
import { IInfoSectionItemService } from '../types';
import GqlClient from '@shared/gql-client/client';
import { ApiGqlClientKey } from '@shared/gql-client/constants';

@injectable()
class InfoItemGqlService extends StreamHandler<InfoItem> implements IInfoSectionItemService {
  private api: GqlClient;

  public constructor(
    @inject(ApiGqlClientKey) api: GqlClient,
  ) {
    super();
    this.add('default', this.streamDefault);
    this.add('plain', this.plainStream);
    this.api = api;
  }

  list(opts: ListOptions): Observable<InfoItem> {
    throw new Error('Method not implemented.');
  }

  bySection(sectionId: number): Observable<InfoItem> {
    throw new Error('Method not implemented.');
  }

  count(slug: string): Observable<number> {
    throw new Error('Method not implemented.');
  }

  private streamDefault = {
    stream: (opts?: ListOptions, method?: string, query?: ListItemsReq): Observable<InfoItem> => {
      const input: ListItemsReq = {
        landingShow: query?.landingShow,
        slug: query?.slug as string,
        opts: query?.opts,
      };

      if (query?.opts) {
        input.opts = query.opts;
      } else {
        input.opts = opts;
      }

      return this.api.query$<InfoSectionItemListQuery, InfoSectionItemListQueryVariables>(
        InfoSectionItemListDocument,
        { input },
        {
          noCache: true,
        },
      ).pipe(mergeMap(res => res.infoSectionItemList));
    },
    loadCount: (method?: string, query?: ListItemsReq): Observable<number> => {
      return this.api.query$<InfoSectionItemCountQuery, InfoSectionItemCountQueryVariables>(
        InfoSectionItemCountDocument,
        { input: query?.slug },
      ).pipe(
        map(res => res.infoSectionItemCount as number),
      );
    },
  };

  private plainStream = {
    stream: (opts: ListOptions, method?: string, query?: object): Observable<InfoItem> => {
      return this.api.query$<InfoSectionItemListPlainQuery, InfoSectionItemListPlainQueryVariables>(
        InfoSectionItemListPlainDocument,
        { input: opts },
        {
          noCache: true,
        },
      ).pipe(mergeMap(res => {
        const items = res.infoSectionItemListPlain.map(i => new InfoItem(i));
        return from(items);
      }));
    },
    loadCount: (method?: string, query?: object): Observable<number> => this.streamDefault.loadCount(),
  };

  create(input: SectionItemModelInput): Observable<InfoItem> {
    return this.api.mutation$<InfoSectionItemCreateMutation, InfoSectionItemCreateMutationVariables>(
      InfoSectionItemCreateDocument,
      { input },
    ).pipe(map(res => new InfoItem(res.infoSectionItemCreate as InfoItem)));
  }

  update(input: SectionItemModelInput): Observable<InfoItem> {
    return this.api.mutation$<InfoSectionItemUpdateMutation, InfoSectionItemUpdateMutationVariables>(
      InfoSectionItemUpdateDocument,
      { input },
    ).pipe(map(res => new InfoItem(res.infoSectionItemUpdate as InfoItem)));
  }

  getById(id: number): Observable<InfoItem> {
    return this.api.query$<InfoSectionItemGetByIdQuery, InfoSectionItemGetByIdQueryVariables>(
      InfoSectionItemGetByIdDocument,
      { input: id },
    ).pipe(map(res => new InfoItem(res.infoSectionItemGetById as InfoItem)));
  }

  delete(id: number): Observable<void> {
    return this.api.mutation$<InfoSectionItemDeleteMutation, InfoSectionItemDeleteMutationVariables>(
      InfoSectionItemDeleteDocument,
      { input: id },
    ).pipe(map(res => res.infoSectionItemDelete));
  }

  loadItems(query: ListItemsReq): Observable<InfoItem> {
    return this.streamDefault.stream(query.opts, '', query);
  }
}

export default InfoItemGqlService;