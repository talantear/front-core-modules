import useRoleCheck from '../roles/hook';
import useSession from './hook';
import User from '../user/models/user';

const useSessionWithRoleCheck = <T extends object>(role: string): [user: User<T> | undefined, isInherit: boolean | undefined] => {
  const session = useSession<T>();
  const isInherit = useRoleCheck(session, role);

  return [session, isInherit];
}

export default useSessionWithRoleCheck;