import { ILoginResp, IRegAndLoginReq, IRegisterResp } from '../dto';

export interface IAuthService {
  login: (req: IRegAndLoginReq) => Promise<ILoginResp>,
  register: (req: IRegAndLoginReq) => Promise<IRegisterResp>,
}