import { Container, interfaces } from 'inversify';
import { SecurityGqlModule } from './module';
import { ISecurityService } from '../service/types';

const gqlSecurityContainer = <T extends interfaces.Newable<ISecurityService>>(service: T): interfaces.Container => {
  const container = new Container();
  container.load(SecurityGqlModule(service));
  return container;
}

export default gqlSecurityContainer;
