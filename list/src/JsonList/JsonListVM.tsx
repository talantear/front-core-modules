import { ObservableVM, BaseViewModel } from '@shared/stream/model';
import ListUsecase from '../usecase';
import List from '../di/model';
import { map } from 'rxjs';
import { ObservableField } from '@shared/stream/model';
import { IListModel } from '../dto';

interface SearchVMLoadParams {
  context: string;
}

class JsonListVM<MetaData = {}> extends BaseViewModel<SearchVMLoadParams> {
  private listUC: ListUsecase<MetaData>;
  @ObservableField()
  public data?: ObservableVM<List<MetaData>>;

  constructor(listUC: ListUsecase<MetaData>) {
    super();
    this.listUC = listUC;
    this.data = new ObservableVM<List<MetaData>>(new List<MetaData>({} as IListModel) as List<MetaData>);
  }

  load = (params?: SearchVMLoadParams) => {
    if (!params?.context) {
      return;
    }

    this.listUC.initSubject(params.context);

    const sub = this.listUC.getByContext(params.context)
      .subscribe(e => this.data?.setItem(e));

    const subscriberSubject = this.listUC.getSubscriberSubject(params.context)
      .subscribe(e => this.data?.setItem(e));

    this.addSubscription(sub);
    this.addSubscription(subscriberSubject);
  };


  getSubject = () => {
    return this.listUC.getSubject();
  };

  getVisible = () => {
    return this.listUC.getVisible();
  };

  setVisible = (value: boolean) => {
    this.listUC.setVisible(value);
  };

  save = (value: IListModel) => {
    return this.listUC.save(value).pipe(
      map(i => {
        if (i) {
          this.listUC.getSubscriberSubject(value.context).next(i);
        }
        return i;
      }),
    );
  };
}


export default JsonListVM;
