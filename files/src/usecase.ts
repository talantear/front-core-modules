import { inject, injectable } from 'inversify';
import { IFilesUploadRes } from './dto';
import { fileServiceDiKey } from './constants';
import { IFilesService } from './service/types';

export interface IFilesUsecase {
  upload: (file: File) => Promise<IFilesUploadRes>,
  get: (key: string) => Promise<IFilesUploadRes>,
  getFileInfo: (key: string) => Promise<IFilesUploadRes>,
}

@injectable()
class FilesUsecase implements IFilesUsecase {
  public static diKey = Symbol.for('FilesUsecaseDiKey');

  private filesService: IFilesService;

  constructor(
    @inject(fileServiceDiKey) filesService: IFilesService,
  ) {
    this.filesService = filesService;
  }

  upload(file: File): Promise<IFilesUploadRes> {
    return this.filesService.upload(file);
  }

  get(key: string): Promise<IFilesUploadRes> {
    return this.filesService.get(key);
  }

  getFileInfo(key: string): Promise<IFilesUploadRes> {
    return this.filesService.getFileInfo(key);
  }
}

export default FilesUsecase;
