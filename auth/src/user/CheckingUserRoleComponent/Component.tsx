import { FunctionComponent } from 'react';
import { cObserver } from '@shared/stream/model';
import { ICheckingUserRoleComponentView } from './index';

const CheckingUserRoleComponentView: FunctionComponent<ICheckingUserRoleComponentView> = (props) => {
  const { children, checkRole } = props;
  if (!checkRole) {
    return null;
  }
  return (
    <>
      {children}
    </>
  );
};
export default cObserver(CheckingUserRoleComponentView);