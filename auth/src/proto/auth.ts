/* eslint-disable */
import { messageTypeRegistry } from "./typeRegistry";
import Long from "long";
import { grpc } from "@improbable-eng/grpc-web";
import _m0 from "protobufjs/minimal";
import { UInt32Value } from "./commonTypes";
import { BrowserHeaders } from "browser-headers";

export const protobufPackage = "authMs";

/** Represents login response model. */
export interface LoginResp {
  $type: "authMs.LoginResp";
  /** / User`s access token */
  accessToken: string;
  /** / User`s data */
  refreshToken: string;
}

/** Represents getting token response. */
export interface GetTokenResp {
  $type: "authMs.GetTokenResp";
  /** / User`s token */
  accessToken: string;
  /** / User`s data */
  refreshToken: string;
}

export interface RegisterResp {
  $type: "authMs.RegisterResp";
  /** / Generated string for signature */
  nonce?: string | undefined;
}

export interface RegAndLoginReq {
  $type: "authMs.RegAndLoginReq";
  type: string;
  password?: string | undefined;
  email?: string | undefined;
  applicationSlug: string;
  provider?: string | undefined;
  state?: string | undefined;
  redirectUri?: string | undefined;
  lang?: string | undefined;
  parent?: string | undefined;
  extra?: string | undefined;
  code?: string | undefined;
}

const baseLoginResp: object = {
  $type: "authMs.LoginResp",
  accessToken: "",
  refreshToken: "",
};

export const LoginResp = {
  $type: "authMs.LoginResp" as const,

  encode(
    message: LoginResp,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.accessToken !== "") {
      writer.uint32(10).string(message.accessToken);
    }
    if (message.refreshToken !== "") {
      writer.uint32(18).string(message.refreshToken);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): LoginResp {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseLoginResp } as LoginResp;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.accessToken = reader.string();
          break;
        case 2:
          message.refreshToken = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): LoginResp {
    const message = { ...baseLoginResp } as LoginResp;
    message.accessToken =
      object.accessToken !== undefined && object.accessToken !== null
        ? String(object.accessToken)
        : "";
    message.refreshToken =
      object.refreshToken !== undefined && object.refreshToken !== null
        ? String(object.refreshToken)
        : "";
    return message;
  },

  toJSON(message: LoginResp): unknown {
    const obj: any = {};
    message.accessToken !== undefined &&
      (obj.accessToken = message.accessToken);
    message.refreshToken !== undefined &&
      (obj.refreshToken = message.refreshToken);
    return obj;
  },

  fromPartial(object: DeepPartial<LoginResp>): LoginResp {
    const message = { ...baseLoginResp } as LoginResp;
    message.accessToken = object.accessToken ?? "";
    message.refreshToken = object.refreshToken ?? "";
    return message;
  },
};

messageTypeRegistry.set(LoginResp.$type, LoginResp);

const baseGetTokenResp: object = {
  $type: "authMs.GetTokenResp",
  accessToken: "",
  refreshToken: "",
};

export const GetTokenResp = {
  $type: "authMs.GetTokenResp" as const,

  encode(
    message: GetTokenResp,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.accessToken !== "") {
      writer.uint32(10).string(message.accessToken);
    }
    if (message.refreshToken !== "") {
      writer.uint32(18).string(message.refreshToken);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): GetTokenResp {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseGetTokenResp } as GetTokenResp;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.accessToken = reader.string();
          break;
        case 2:
          message.refreshToken = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): GetTokenResp {
    const message = { ...baseGetTokenResp } as GetTokenResp;
    message.accessToken =
      object.accessToken !== undefined && object.accessToken !== null
        ? String(object.accessToken)
        : "";
    message.refreshToken =
      object.refreshToken !== undefined && object.refreshToken !== null
        ? String(object.refreshToken)
        : "";
    return message;
  },

  toJSON(message: GetTokenResp): unknown {
    const obj: any = {};
    message.accessToken !== undefined &&
      (obj.accessToken = message.accessToken);
    message.refreshToken !== undefined &&
      (obj.refreshToken = message.refreshToken);
    return obj;
  },

  fromPartial(object: DeepPartial<GetTokenResp>): GetTokenResp {
    const message = { ...baseGetTokenResp } as GetTokenResp;
    message.accessToken = object.accessToken ?? "";
    message.refreshToken = object.refreshToken ?? "";
    return message;
  },
};

messageTypeRegistry.set(GetTokenResp.$type, GetTokenResp);

const baseRegisterResp: object = { $type: "authMs.RegisterResp" };

export const RegisterResp = {
  $type: "authMs.RegisterResp" as const,

  encode(
    message: RegisterResp,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.nonce !== undefined) {
      writer.uint32(10).string(message.nonce);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): RegisterResp {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseRegisterResp } as RegisterResp;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.nonce = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): RegisterResp {
    const message = { ...baseRegisterResp } as RegisterResp;
    message.nonce =
      object.nonce !== undefined && object.nonce !== null
        ? String(object.nonce)
        : undefined;
    return message;
  },

  toJSON(message: RegisterResp): unknown {
    const obj: any = {};
    message.nonce !== undefined && (obj.nonce = message.nonce);
    return obj;
  },

  fromPartial(object: DeepPartial<RegisterResp>): RegisterResp {
    const message = { ...baseRegisterResp } as RegisterResp;
    message.nonce = object.nonce ?? undefined;
    return message;
  },
};

messageTypeRegistry.set(RegisterResp.$type, RegisterResp);

const baseRegAndLoginReq: object = {
  $type: "authMs.RegAndLoginReq",
  type: "",
  applicationSlug: "",
};

export const RegAndLoginReq = {
  $type: "authMs.RegAndLoginReq" as const,

  encode(
    message: RegAndLoginReq,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.type !== "") {
      writer.uint32(10).string(message.type);
    }
    if (message.password !== undefined) {
      writer.uint32(18).string(message.password);
    }
    if (message.email !== undefined) {
      writer.uint32(26).string(message.email);
    }
    if (message.applicationSlug !== "") {
      writer.uint32(34).string(message.applicationSlug);
    }
    if (message.provider !== undefined) {
      writer.uint32(42).string(message.provider);
    }
    if (message.state !== undefined) {
      writer.uint32(50).string(message.state);
    }
    if (message.redirectUri !== undefined) {
      writer.uint32(58).string(message.redirectUri);
    }
    if (message.lang !== undefined) {
      writer.uint32(66).string(message.lang);
    }
    if (message.parent !== undefined) {
      writer.uint32(74).string(message.parent);
    }
    if (message.extra !== undefined) {
      writer.uint32(82).string(message.extra);
    }
    if (message.code !== undefined) {
      writer.uint32(90).string(message.code);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): RegAndLoginReq {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseRegAndLoginReq } as RegAndLoginReq;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.type = reader.string();
          break;
        case 2:
          message.password = reader.string();
          break;
        case 3:
          message.email = reader.string();
          break;
        case 4:
          message.applicationSlug = reader.string();
          break;
        case 5:
          message.provider = reader.string();
          break;
        case 6:
          message.state = reader.string();
          break;
        case 7:
          message.redirectUri = reader.string();
          break;
        case 8:
          message.lang = reader.string();
          break;
        case 9:
          message.parent = reader.string();
          break;
        case 10:
          message.extra = reader.string();
          break;
        case 11:
          message.code = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): RegAndLoginReq {
    const message = { ...baseRegAndLoginReq } as RegAndLoginReq;
    message.type =
      object.type !== undefined && object.type !== null
        ? String(object.type)
        : "";
    message.password =
      object.password !== undefined && object.password !== null
        ? String(object.password)
        : undefined;
    message.email =
      object.email !== undefined && object.email !== null
        ? String(object.email)
        : undefined;
    message.applicationSlug =
      object.applicationSlug !== undefined && object.applicationSlug !== null
        ? String(object.applicationSlug)
        : "";
    message.provider =
      object.provider !== undefined && object.provider !== null
        ? String(object.provider)
        : undefined;
    message.state =
      object.state !== undefined && object.state !== null
        ? String(object.state)
        : undefined;
    message.redirectUri =
      object.redirectUri !== undefined && object.redirectUri !== null
        ? String(object.redirectUri)
        : undefined;
    message.lang =
      object.lang !== undefined && object.lang !== null
        ? String(object.lang)
        : undefined;
    message.parent =
      object.parent !== undefined && object.parent !== null
        ? String(object.parent)
        : undefined;
    message.extra =
      object.extra !== undefined && object.extra !== null
        ? String(object.extra)
        : undefined;
    message.code =
      object.code !== undefined && object.code !== null
        ? String(object.code)
        : undefined;
    return message;
  },

  toJSON(message: RegAndLoginReq): unknown {
    const obj: any = {};
    message.type !== undefined && (obj.type = message.type);
    message.password !== undefined && (obj.password = message.password);
    message.email !== undefined && (obj.email = message.email);
    message.applicationSlug !== undefined &&
      (obj.applicationSlug = message.applicationSlug);
    message.provider !== undefined && (obj.provider = message.provider);
    message.state !== undefined && (obj.state = message.state);
    message.redirectUri !== undefined &&
      (obj.redirectUri = message.redirectUri);
    message.lang !== undefined && (obj.lang = message.lang);
    message.parent !== undefined && (obj.parent = message.parent);
    message.extra !== undefined && (obj.extra = message.extra);
    message.code !== undefined && (obj.code = message.code);
    return obj;
  },

  fromPartial(object: DeepPartial<RegAndLoginReq>): RegAndLoginReq {
    const message = { ...baseRegAndLoginReq } as RegAndLoginReq;
    message.type = object.type ?? "";
    message.password = object.password ?? undefined;
    message.email = object.email ?? undefined;
    message.applicationSlug = object.applicationSlug ?? "";
    message.provider = object.provider ?? undefined;
    message.state = object.state ?? undefined;
    message.redirectUri = object.redirectUri ?? undefined;
    message.lang = object.lang ?? undefined;
    message.parent = object.parent ?? undefined;
    message.extra = object.extra ?? undefined;
    message.code = object.code ?? undefined;
    return message;
  },
};

messageTypeRegistry.set(RegAndLoginReq.$type, RegAndLoginReq);

/** Represents authorization methods. */
export interface Auth {
  /** / Represents getting user`s token */
  GetToken(
    request: DeepPartial<UInt32Value>,
    metadata?: grpc.Metadata
  ): Promise<GetTokenResp>;
  /** / Represents user`s login with provider */
  Login(
    request: DeepPartial<RegAndLoginReq>,
    metadata?: grpc.Metadata
  ): Promise<LoginResp>;
  /** / Represents users register */
  Register(
    request: DeepPartial<RegAndLoginReq>,
    metadata?: grpc.Metadata
  ): Promise<RegisterResp>;
}

export class AuthClientImpl implements Auth {
  private readonly rpc: Rpc;

  constructor(rpc: Rpc) {
    this.rpc = rpc;
    this.GetToken = this.GetToken.bind(this);
    this.Login = this.Login.bind(this);
    this.Register = this.Register.bind(this);
  }

  GetToken(
    request: DeepPartial<UInt32Value>,
    metadata?: grpc.Metadata
  ): Promise<GetTokenResp> {
    return this.rpc.unary(
      AuthGetTokenDesc,
      UInt32Value.fromPartial(request),
      metadata
    );
  }

  Login(
    request: DeepPartial<RegAndLoginReq>,
    metadata?: grpc.Metadata
  ): Promise<LoginResp> {
    return this.rpc.unary(
      AuthLoginDesc,
      RegAndLoginReq.fromPartial(request),
      metadata
    );
  }

  Register(
    request: DeepPartial<RegAndLoginReq>,
    metadata?: grpc.Metadata
  ): Promise<RegisterResp> {
    return this.rpc.unary(
      AuthRegisterDesc,
      RegAndLoginReq.fromPartial(request),
      metadata
    );
  }
}

export const AuthDesc = {
  serviceName: "authMs.Auth",
};

export const AuthGetTokenDesc: UnaryMethodDefinitionish = {
  methodName: "GetToken",
  service: AuthDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return UInt32Value.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...GetTokenResp.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const AuthLoginDesc: UnaryMethodDefinitionish = {
  methodName: "Login",
  service: AuthDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return RegAndLoginReq.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...LoginResp.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const AuthRegisterDesc: UnaryMethodDefinitionish = {
  methodName: "Register",
  service: AuthDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return RegAndLoginReq.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...RegisterResp.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

interface UnaryMethodDefinitionishR
  extends grpc.UnaryMethodDefinition<any, any> {
  requestStream: any;
  responseStream: any;
}

type UnaryMethodDefinitionish = UnaryMethodDefinitionishR;

interface Rpc {
  unary<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    request: any,
    metadata: grpc.Metadata | undefined
  ): Promise<any>;
}

export class GrpcWebImpl {
  private host: string;
  private options: {
    transport?: grpc.TransportFactory;

    debug?: boolean;
    metadata?: grpc.Metadata;
  };

  constructor(
    host: string,
    options: {
      transport?: grpc.TransportFactory;

      debug?: boolean;
      metadata?: grpc.Metadata;
    }
  ) {
    this.host = host;
    this.options = options;
  }

  unary<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    _request: any,
    metadata: grpc.Metadata | undefined
  ): Promise<any> {
    const request = { ..._request, ...methodDesc.requestType };
    const maybeCombinedMetadata =
      metadata && this.options.metadata
        ? new BrowserHeaders({
            ...this.options?.metadata.headersMap,
            ...metadata?.headersMap,
          })
        : metadata || this.options.metadata;
    return new Promise((resolve, reject) => {
      grpc.unary(methodDesc, {
        request,
        host: this.host,
        metadata: maybeCombinedMetadata,
        transport: this.options.transport,
        debug: this.options.debug,
        onEnd: function (response) {
          if (response.status === grpc.Code.OK) {
            resolve(response.message);
          } else {
            const err = new Error(response.statusMessage) as any;
            err.code = response.status;
            err.metadata = response.trailers;
            reject(err);
          }
        },
      });
    });
  }
}

type Builtin =
  | Date
  | Function
  | Uint8Array
  | string
  | number
  | boolean
  | undefined;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? { [K in Exclude<keyof T, "$type">]?: DeepPartial<T[K]> }
  : Partial<T>;

if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
