export interface IListOptions {
  offset: number;
  limit: number;
  order?: string | undefined;
}

export interface IStringRequest {
  value: string;
}

export interface IIdRequest {
  id: number;
}

export interface IId64Request {
  id: number;
}

export interface IBoolValue {
  value: boolean;
}

export interface IInt64Value {
  value: number;
}

export interface IUInt64Value {
  value: number;
}

export interface IInt32Value {
  value: number;
}

export interface IUInt32Value {
  value: number;
}