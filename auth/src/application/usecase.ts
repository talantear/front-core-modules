import { inject, injectable } from 'inversify';
import { Observable, Subscription } from 'rxjs';
import { IBaseListUsecase, IPageService } from '@shared/stream/types';
import { PageService } from '@shared/stream/service';
import { ListMV } from '@shared/stream/model';
import { IApplicationService } from './service/types';
import { IId64Request, IStringRequest, IListOptions } from '@shared/types/dto';
import { IApplication } from './dto';
import { applicationServiceDiKey } from './constants';

export interface IApplicationUsecase {
  create(request: IApplication): Observable<IApplication>,
  update(request: IApplication): Observable<IApplication>,
  getById(request: IId64Request): Observable<IApplication>,
  getByClientId(request: IStringRequest): Observable<IApplication>,
  remove(request: IId64Request): Observable<void>,
  addCache(key: string, value: IApplication): void,
  getCache(key: string): IApplication,
}

@injectable()
class ApplicationUsecase implements IApplicationUsecase, IBaseListUsecase<IApplication> {
  public static diKey = Symbol.for('ApplicationUsecaseDiKey');

  private applicationService: IApplicationService;
  private pageService: IPageService<IApplication>;

  constructor(
    @inject(applicationServiceDiKey) applicationService: IApplicationService,
  ) {
    this.applicationService = applicationService;
    this.pageService = new PageService(this.applicationService);
  }

  addCache(key: string, value: IApplication): void {
    this.applicationService.addCache(key, value);
  }

  getCache(key: string): IApplication {
    return this.applicationService.getCache(key) || {};
  }

  loadItems(model: ListMV<IApplication>, opts: IListOptions, method?: string, query?: object): [Observable<IApplication>, Subscription] {
    return this.pageService.loadPage(model, opts);
  }

  loadTotalCount(model: ListMV<IApplication>, method?: string, query?: object): [Observable<number>, Subscription] {
    return this.pageService.loadTotalCount(model);
  }

  create(request: IApplication): Observable<IApplication> {
    return this.applicationService.create(request);
  }

  getById(request: IId64Request): Observable<IApplication> {
    return this.applicationService.getById(request);
  }

  update(request: IApplication): Observable<IApplication> {
    return this.applicationService.update(request);
  }

  getByClientId(request: IStringRequest): Observable<IApplication> {
    return this.applicationService.getByClientId(request);
  }

  remove(request: IId64Request): Observable<void> {
    return this.applicationService.remove(request);
  }
}

export default ApplicationUsecase;
