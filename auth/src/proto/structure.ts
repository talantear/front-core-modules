/* eslint-disable */
import { messageTypeRegistry } from "./typeRegistry";
import Long from "long";
import { grpc } from "@improbable-eng/grpc-web";
import _m0 from "protobufjs/minimal";
import { ListOptions } from "./paginatior";
import { Observable } from "rxjs";
import { Empty } from "./google/protobuf/empty";
import { UInt32Value, Int64Value } from "./commonTypes";
import { BrowserHeaders } from "browser-headers";
import { share } from "rxjs/operators";

export const protobufPackage = "authMs";

/** Represents children request model. */
export interface ChildrenReq {
  $type: "authMs.ChildrenReq";
  /** / Options of list */
  opt?: ListOptions;
  /** / If user hase depth */
  useDepth: boolean;
  userId?: number | undefined;
  /** / Query */
  query?: string | undefined;
  depth?: number | undefined;
  maxDepth?: number | undefined;
}

/** Represents children response model. */
export interface ChildrenResp {
  $type: "authMs.ChildrenResp";
  /** / Children */
  children: ChildrenModel[];
  count: number;
}

/** Represents user`s depth request model. */
export interface UserWithDepthReq {
  $type: "authMs.UserWithDepthReq";
  userId: number;
  maxDepth?: number | undefined;
}

/** Represents user`s line count request model. */
export interface LineCountReq {
  $type: "authMs.LineCountReq";
  userId?: number | undefined;
}

/** Represents user`s children model. */
export interface ChildrenModel {
  $type: "authMs.ChildrenModel";
  id: number;
  email: string;
  verified: boolean;
  hasChildren: boolean;
  alias?: string | undefined;
  parentId?: number | undefined;
  firstName?: string | undefined;
  middleName?: string | undefined;
  surname?: string | undefined;
}

const baseChildrenReq: object = {
  $type: "authMs.ChildrenReq",
  useDepth: false,
};

export const ChildrenReq = {
  $type: "authMs.ChildrenReq" as const,

  encode(
    message: ChildrenReq,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.opt !== undefined) {
      ListOptions.encode(message.opt, writer.uint32(10).fork()).ldelim();
    }
    if (message.useDepth === true) {
      writer.uint32(16).bool(message.useDepth);
    }
    if (message.userId !== undefined) {
      writer.uint32(24).uint32(message.userId);
    }
    if (message.query !== undefined) {
      writer.uint32(34).string(message.query);
    }
    if (message.depth !== undefined) {
      writer.uint32(40).int32(message.depth);
    }
    if (message.maxDepth !== undefined) {
      writer.uint32(48).int32(message.maxDepth);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ChildrenReq {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseChildrenReq } as ChildrenReq;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.opt = ListOptions.decode(reader, reader.uint32());
          break;
        case 2:
          message.useDepth = reader.bool();
          break;
        case 3:
          message.userId = reader.uint32();
          break;
        case 4:
          message.query = reader.string();
          break;
        case 5:
          message.depth = reader.int32();
          break;
        case 6:
          message.maxDepth = reader.int32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ChildrenReq {
    const message = { ...baseChildrenReq } as ChildrenReq;
    message.opt =
      object.opt !== undefined && object.opt !== null
        ? ListOptions.fromJSON(object.opt)
        : undefined;
    message.useDepth =
      object.useDepth !== undefined && object.useDepth !== null
        ? Boolean(object.useDepth)
        : false;
    message.userId =
      object.userId !== undefined && object.userId !== null
        ? Number(object.userId)
        : undefined;
    message.query =
      object.query !== undefined && object.query !== null
        ? String(object.query)
        : undefined;
    message.depth =
      object.depth !== undefined && object.depth !== null
        ? Number(object.depth)
        : undefined;
    message.maxDepth =
      object.maxDepth !== undefined && object.maxDepth !== null
        ? Number(object.maxDepth)
        : undefined;
    return message;
  },

  toJSON(message: ChildrenReq): unknown {
    const obj: any = {};
    message.opt !== undefined &&
      (obj.opt = message.opt ? ListOptions.toJSON(message.opt) : undefined);
    message.useDepth !== undefined && (obj.useDepth = message.useDepth);
    message.userId !== undefined && (obj.userId = message.userId);
    message.query !== undefined && (obj.query = message.query);
    message.depth !== undefined && (obj.depth = message.depth);
    message.maxDepth !== undefined && (obj.maxDepth = message.maxDepth);
    return obj;
  },

  fromPartial(object: DeepPartial<ChildrenReq>): ChildrenReq {
    const message = { ...baseChildrenReq } as ChildrenReq;
    message.opt =
      object.opt !== undefined && object.opt !== null
        ? ListOptions.fromPartial(object.opt)
        : undefined;
    message.useDepth = object.useDepth ?? false;
    message.userId = object.userId ?? undefined;
    message.query = object.query ?? undefined;
    message.depth = object.depth ?? undefined;
    message.maxDepth = object.maxDepth ?? undefined;
    return message;
  },
};

messageTypeRegistry.set(ChildrenReq.$type, ChildrenReq);

const baseChildrenResp: object = { $type: "authMs.ChildrenResp", count: 0 };

export const ChildrenResp = {
  $type: "authMs.ChildrenResp" as const,

  encode(
    message: ChildrenResp,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    for (const v of message.children) {
      ChildrenModel.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    if (message.count !== 0) {
      writer.uint32(16).int64(message.count);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ChildrenResp {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseChildrenResp } as ChildrenResp;
    message.children = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.children.push(ChildrenModel.decode(reader, reader.uint32()));
          break;
        case 2:
          message.count = longToNumber(reader.int64() as Long);
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ChildrenResp {
    const message = { ...baseChildrenResp } as ChildrenResp;
    message.children = (object.children ?? []).map((e: any) =>
      ChildrenModel.fromJSON(e)
    );
    message.count =
      object.count !== undefined && object.count !== null
        ? Number(object.count)
        : 0;
    return message;
  },

  toJSON(message: ChildrenResp): unknown {
    const obj: any = {};
    if (message.children) {
      obj.children = message.children.map((e) =>
        e ? ChildrenModel.toJSON(e) : undefined
      );
    } else {
      obj.children = [];
    }
    message.count !== undefined && (obj.count = message.count);
    return obj;
  },

  fromPartial(object: DeepPartial<ChildrenResp>): ChildrenResp {
    const message = { ...baseChildrenResp } as ChildrenResp;
    message.children = (object.children ?? []).map((e) =>
      ChildrenModel.fromPartial(e)
    );
    message.count = object.count ?? 0;
    return message;
  },
};

messageTypeRegistry.set(ChildrenResp.$type, ChildrenResp);

const baseUserWithDepthReq: object = {
  $type: "authMs.UserWithDepthReq",
  userId: 0,
};

export const UserWithDepthReq = {
  $type: "authMs.UserWithDepthReq" as const,

  encode(
    message: UserWithDepthReq,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.userId !== 0) {
      writer.uint32(8).uint32(message.userId);
    }
    if (message.maxDepth !== undefined) {
      writer.uint32(16).int32(message.maxDepth);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): UserWithDepthReq {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseUserWithDepthReq } as UserWithDepthReq;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.userId = reader.uint32();
          break;
        case 2:
          message.maxDepth = reader.int32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): UserWithDepthReq {
    const message = { ...baseUserWithDepthReq } as UserWithDepthReq;
    message.userId =
      object.userId !== undefined && object.userId !== null
        ? Number(object.userId)
        : 0;
    message.maxDepth =
      object.maxDepth !== undefined && object.maxDepth !== null
        ? Number(object.maxDepth)
        : undefined;
    return message;
  },

  toJSON(message: UserWithDepthReq): unknown {
    const obj: any = {};
    message.userId !== undefined && (obj.userId = message.userId);
    message.maxDepth !== undefined && (obj.maxDepth = message.maxDepth);
    return obj;
  },

  fromPartial(object: DeepPartial<UserWithDepthReq>): UserWithDepthReq {
    const message = { ...baseUserWithDepthReq } as UserWithDepthReq;
    message.userId = object.userId ?? 0;
    message.maxDepth = object.maxDepth ?? undefined;
    return message;
  },
};

messageTypeRegistry.set(UserWithDepthReq.$type, UserWithDepthReq);

const baseLineCountReq: object = { $type: "authMs.LineCountReq" };

export const LineCountReq = {
  $type: "authMs.LineCountReq" as const,

  encode(
    message: LineCountReq,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.userId !== undefined) {
      writer.uint32(8).uint32(message.userId);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): LineCountReq {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseLineCountReq } as LineCountReq;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.userId = reader.uint32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): LineCountReq {
    const message = { ...baseLineCountReq } as LineCountReq;
    message.userId =
      object.userId !== undefined && object.userId !== null
        ? Number(object.userId)
        : undefined;
    return message;
  },

  toJSON(message: LineCountReq): unknown {
    const obj: any = {};
    message.userId !== undefined && (obj.userId = message.userId);
    return obj;
  },

  fromPartial(object: DeepPartial<LineCountReq>): LineCountReq {
    const message = { ...baseLineCountReq } as LineCountReq;
    message.userId = object.userId ?? undefined;
    return message;
  },
};

messageTypeRegistry.set(LineCountReq.$type, LineCountReq);

const baseChildrenModel: object = {
  $type: "authMs.ChildrenModel",
  id: 0,
  email: "",
  verified: false,
  hasChildren: false,
};

export const ChildrenModel = {
  $type: "authMs.ChildrenModel" as const,

  encode(
    message: ChildrenModel,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.id !== 0) {
      writer.uint32(8).uint32(message.id);
    }
    if (message.email !== "") {
      writer.uint32(18).string(message.email);
    }
    if (message.verified === true) {
      writer.uint32(24).bool(message.verified);
    }
    if (message.hasChildren === true) {
      writer.uint32(32).bool(message.hasChildren);
    }
    if (message.alias !== undefined) {
      writer.uint32(42).string(message.alias);
    }
    if (message.parentId !== undefined) {
      writer.uint32(48).uint32(message.parentId);
    }
    if (message.firstName !== undefined) {
      writer.uint32(58).string(message.firstName);
    }
    if (message.middleName !== undefined) {
      writer.uint32(66).string(message.middleName);
    }
    if (message.surname !== undefined) {
      writer.uint32(74).string(message.surname);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ChildrenModel {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseChildrenModel } as ChildrenModel;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.uint32();
          break;
        case 2:
          message.email = reader.string();
          break;
        case 3:
          message.verified = reader.bool();
          break;
        case 4:
          message.hasChildren = reader.bool();
          break;
        case 5:
          message.alias = reader.string();
          break;
        case 6:
          message.parentId = reader.uint32();
          break;
        case 7:
          message.firstName = reader.string();
          break;
        case 8:
          message.middleName = reader.string();
          break;
        case 9:
          message.surname = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ChildrenModel {
    const message = { ...baseChildrenModel } as ChildrenModel;
    message.id =
      object.id !== undefined && object.id !== null ? Number(object.id) : 0;
    message.email =
      object.email !== undefined && object.email !== null
        ? String(object.email)
        : "";
    message.verified =
      object.verified !== undefined && object.verified !== null
        ? Boolean(object.verified)
        : false;
    message.hasChildren =
      object.hasChildren !== undefined && object.hasChildren !== null
        ? Boolean(object.hasChildren)
        : false;
    message.alias =
      object.alias !== undefined && object.alias !== null
        ? String(object.alias)
        : undefined;
    message.parentId =
      object.parentId !== undefined && object.parentId !== null
        ? Number(object.parentId)
        : undefined;
    message.firstName =
      object.firstName !== undefined && object.firstName !== null
        ? String(object.firstName)
        : undefined;
    message.middleName =
      object.middleName !== undefined && object.middleName !== null
        ? String(object.middleName)
        : undefined;
    message.surname =
      object.surname !== undefined && object.surname !== null
        ? String(object.surname)
        : undefined;
    return message;
  },

  toJSON(message: ChildrenModel): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    message.email !== undefined && (obj.email = message.email);
    message.verified !== undefined && (obj.verified = message.verified);
    message.hasChildren !== undefined &&
      (obj.hasChildren = message.hasChildren);
    message.alias !== undefined && (obj.alias = message.alias);
    message.parentId !== undefined && (obj.parentId = message.parentId);
    message.firstName !== undefined && (obj.firstName = message.firstName);
    message.middleName !== undefined && (obj.middleName = message.middleName);
    message.surname !== undefined && (obj.surname = message.surname);
    return obj;
  },

  fromPartial(object: DeepPartial<ChildrenModel>): ChildrenModel {
    const message = { ...baseChildrenModel } as ChildrenModel;
    message.id = object.id ?? 0;
    message.email = object.email ?? "";
    message.verified = object.verified ?? false;
    message.hasChildren = object.hasChildren ?? false;
    message.alias = object.alias ?? undefined;
    message.parentId = object.parentId ?? undefined;
    message.firstName = object.firstName ?? undefined;
    message.middleName = object.middleName ?? undefined;
    message.surname = object.surname ?? undefined;
    return message;
  },
};

messageTypeRegistry.set(ChildrenModel.$type, ChildrenModel);

/** Represents structure methods. */
export interface Structure {
  /** / Represents user`s children */
  Children(
    request: DeepPartial<ChildrenReq>,
    metadata?: grpc.Metadata
  ): Promise<ChildrenResp>;
  /** / Represents user`s parents */
  Parents(
    request: DeepPartial<UserWithDepthReq>,
    metadata?: grpc.Metadata
  ): Observable<UInt32Value>;
  /** / Represents user`s first line depth */
  GetFirstLineCount(
    request: DeepPartial<LineCountReq>,
    metadata?: grpc.Metadata
  ): Promise<Int64Value>;
  /** / Represents user`s count */
  GetAllCount(
    request: DeepPartial<Empty>,
    metadata?: grpc.Metadata
  ): Promise<Int64Value>;
  /** / Represents user`s structure count */
  GetStructureCount(
    request: DeepPartial<UserWithDepthReq>,
    metadata?: grpc.Metadata
  ): Promise<Int64Value>;
}

export class StructureClientImpl implements Structure {
  private readonly rpc: Rpc;

  constructor(rpc: Rpc) {
    this.rpc = rpc;
    this.Children = this.Children.bind(this);
    this.Parents = this.Parents.bind(this);
    this.GetFirstLineCount = this.GetFirstLineCount.bind(this);
    this.GetAllCount = this.GetAllCount.bind(this);
    this.GetStructureCount = this.GetStructureCount.bind(this);
  }

  Children(
    request: DeepPartial<ChildrenReq>,
    metadata?: grpc.Metadata
  ): Promise<ChildrenResp> {
    return this.rpc.unary(
      StructureChildrenDesc,
      ChildrenReq.fromPartial(request),
      metadata
    );
  }

  Parents(
    request: DeepPartial<UserWithDepthReq>,
    metadata?: grpc.Metadata
  ): Observable<UInt32Value> {
    return this.rpc.invoke(
      StructureParentsDesc,
      UserWithDepthReq.fromPartial(request),
      metadata
    );
  }

  GetFirstLineCount(
    request: DeepPartial<LineCountReq>,
    metadata?: grpc.Metadata
  ): Promise<Int64Value> {
    return this.rpc.unary(
      StructureGetFirstLineCountDesc,
      LineCountReq.fromPartial(request),
      metadata
    );
  }

  GetAllCount(
    request: DeepPartial<Empty>,
    metadata?: grpc.Metadata
  ): Promise<Int64Value> {
    return this.rpc.unary(
      StructureGetAllCountDesc,
      Empty.fromPartial(request),
      metadata
    );
  }

  GetStructureCount(
    request: DeepPartial<UserWithDepthReq>,
    metadata?: grpc.Metadata
  ): Promise<Int64Value> {
    return this.rpc.unary(
      StructureGetStructureCountDesc,
      UserWithDepthReq.fromPartial(request),
      metadata
    );
  }
}

export const StructureDesc = {
  serviceName: "authMs.Structure",
};

export const StructureChildrenDesc: UnaryMethodDefinitionish = {
  methodName: "Children",
  service: StructureDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return ChildrenReq.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...ChildrenResp.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const StructureParentsDesc: UnaryMethodDefinitionish = {
  methodName: "Parents",
  service: StructureDesc,
  requestStream: false,
  responseStream: true,
  requestType: {
    serializeBinary() {
      return UserWithDepthReq.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...UInt32Value.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const StructureGetFirstLineCountDesc: UnaryMethodDefinitionish = {
  methodName: "GetFirstLineCount",
  service: StructureDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return LineCountReq.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...Int64Value.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const StructureGetAllCountDesc: UnaryMethodDefinitionish = {
  methodName: "GetAllCount",
  service: StructureDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return Empty.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...Int64Value.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const StructureGetStructureCountDesc: UnaryMethodDefinitionish = {
  methodName: "GetStructureCount",
  service: StructureDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return UserWithDepthReq.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...Int64Value.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

interface UnaryMethodDefinitionishR
  extends grpc.UnaryMethodDefinition<any, any> {
  requestStream: any;
  responseStream: any;
}

type UnaryMethodDefinitionish = UnaryMethodDefinitionishR;

interface Rpc {
  unary<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    request: any,
    metadata: grpc.Metadata | undefined
  ): Promise<any>;
  invoke<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    request: any,
    metadata: grpc.Metadata | undefined
  ): Observable<any>;
}

export class GrpcWebImpl {
  private host: string;
  private options: {
    transport?: grpc.TransportFactory;
    streamingTransport?: grpc.TransportFactory;
    debug?: boolean;
    metadata?: grpc.Metadata;
  };

  constructor(
    host: string,
    options: {
      transport?: grpc.TransportFactory;
      streamingTransport?: grpc.TransportFactory;
      debug?: boolean;
      metadata?: grpc.Metadata;
    }
  ) {
    this.host = host;
    this.options = options;
  }

  unary<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    _request: any,
    metadata: grpc.Metadata | undefined
  ): Promise<any> {
    const request = { ..._request, ...methodDesc.requestType };
    const maybeCombinedMetadata =
      metadata && this.options.metadata
        ? new BrowserHeaders({
            ...this.options?.metadata.headersMap,
            ...metadata?.headersMap,
          })
        : metadata || this.options.metadata;
    return new Promise((resolve, reject) => {
      grpc.unary(methodDesc, {
        request,
        host: this.host,
        metadata: maybeCombinedMetadata,
        transport: this.options.transport,
        debug: this.options.debug,
        onEnd: function (response) {
          if (response.status === grpc.Code.OK) {
            resolve(response.message);
          } else {
            const err = new Error(response.statusMessage) as any;
            err.code = response.status;
            err.metadata = response.trailers;
            reject(err);
          }
        },
      });
    });
  }

  invoke<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    _request: any,
    metadata: grpc.Metadata | undefined
  ): Observable<any> {
    // Status Response Codes (https://developers.google.com/maps-booking/reference/grpc-api/status_codes)
    const upStreamCodes = [2, 4, 8, 9, 10, 13, 14, 15];
    const DEFAULT_TIMEOUT_TIME: number = 3_000;
    const request = { ..._request, ...methodDesc.requestType };
    const maybeCombinedMetadata =
      metadata && this.options.metadata
        ? new BrowserHeaders({
            ...this.options?.metadata.headersMap,
            ...metadata?.headersMap,
          })
        : metadata || this.options.metadata;
    return new Observable((observer) => {
      const upStream = () => {
        const client = grpc.invoke(methodDesc, {
          host: this.host,
          request,
          transport: this.options.streamingTransport || this.options.transport,
          metadata: maybeCombinedMetadata,
          debug: this.options.debug,
          onMessage: (next) => observer.next(next),
          onEnd: (code: grpc.Code, message: string) => {
            if (code === 0) {
              observer.complete();
            } else if (upStreamCodes.includes(code)) {
              setTimeout(upStream, DEFAULT_TIMEOUT_TIME);
            } else {
              observer.error(new Error(`Error ${code} ${message}`));
            }
          },
        });
        observer.add(() => client.close());
      };
      upStream();
    }).pipe(share());
  }
}

declare var self: any | undefined;
declare var window: any | undefined;
declare var global: any | undefined;
var globalThis: any = (() => {
  if (typeof globalThis !== "undefined") return globalThis;
  if (typeof self !== "undefined") return self;
  if (typeof window !== "undefined") return window;
  if (typeof global !== "undefined") return global;
  throw "Unable to locate global object";
})();

type Builtin =
  | Date
  | Function
  | Uint8Array
  | string
  | number
  | boolean
  | undefined;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? { [K in Exclude<keyof T, "$type">]?: DeepPartial<T[K]> }
  : Partial<T>;

function longToNumber(long: Long): number {
  if (long.gt(Number.MAX_SAFE_INTEGER)) {
    throw new globalThis.Error("Value is larger than Number.MAX_SAFE_INTEGER");
  }
  return long.toNumber();
}

if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
