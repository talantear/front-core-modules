import { AsyncContainerModule, interfaces } from 'inversify';
import RoleUsecase, { IRoleUsecase } from '../usecase';
import { IRoleService } from '../service/types';
import { roleServiceDiKey } from '../constants';

export const RoleDependenciesModule = <T extends interfaces.Newable<IRoleService>>(service: T) => new AsyncContainerModule(async (bind: interfaces.Bind, unbind: interfaces.Unbind) => {
  bind<IRoleService>(roleServiceDiKey).to(service);
  bind<IRoleUsecase>(RoleUsecase.diKey).to(RoleUsecase).inSingletonScope();
});
