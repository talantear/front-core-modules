import { StreamHandler } from '@shared/stream/service';
import List from '../../di/model';
import HttpClient from '@shared/http-client/model';
import { inject } from 'inversify';
import { ApiHttpClientKey } from '@shared/http-client/constants';
import { from, map, mergeMap, Observable, ReplaySubject, Subject } from 'rxjs';
import { IListEditData } from '../../JsonList/types';
import { ResultItemRes, ResultListRes, BaseCommandProps } from '@shared/types/http';
import { IListService, ISubscribeSubject } from '../types';
import { IListModel, ListModelInput } from '../../dto';

class ListHttpService<MetaData = {}> extends StreamHandler<List> implements IListService<MetaData> {
  private api: HttpClient;
  private editSubject: Subject<IListEditData>;
  private visible: Subject<boolean>;
  private subscribeSubject: ISubscribeSubject<MetaData> = {};

  public constructor(
    @inject(ApiHttpClientKey) api: HttpClient,
  ) {
    super();
    this.api = api;
    this.editSubject = new ReplaySubject<IListEditData>(1);
    this.visible = new Subject<boolean>();
  }

  initSubject(context: string) {
    this.subscribeSubject[context] = new Subject<List<MetaData>>();
  }

  create(input: IListModel): Observable<List<MetaData> | undefined> {
    return this.api.$request<object, ResultItemRes<List<MetaData>>>(
      `/api/v1/list/admin`, input, { method: 'POST' }
    ).pipe(map(i => i.item));
  }

  getByContext(context: string): Observable<List<MetaData> | undefined> {
    return this.api.$request<object, ResultItemRes<List<MetaData>>>(
      `/api/v1/list/item/${context}`, {}, { method: 'GET' },
    ).pipe(map(i => i.item && new List<MetaData>(i.item)));
  }

  getSubject(): Subject<IListEditData> {
    return this.editSubject;
  }

  getSubscriberSubject(context: string): Subject<List<MetaData>> {
    return this.subscribeSubject[context];
  }

  getVisible(): Subject<boolean> {
    return this.visible;
  }

  listByContext(context: string): Observable<List<MetaData>> {
    return this.api.$request<object, ResultItemRes<List<MetaData>>>(
      `/api/v1/list/by-context`, { context }, { method: 'POST' },
    ).pipe(map(i => i.item && new List<MetaData>(i.item)));
  }

  listByContexts(contexts: string[]): Observable<List<MetaData>> {
    return this.api.$request<object, ResultListRes<List<MetaData>>>(
      `/api/v1/list/by-contexts`, { contexts }, { method: 'POST' }
    ).pipe(mergeMap(i => from(i.list.map(x => new List<MetaData>(x)))));
  }

  removeById(id: number): Observable<void> {
    return this.api.$request<object, void & BaseCommandProps>(
      `/api/v1/list/admin`, { id }, { method: 'DELETE' },
    );
  }

  setVisible(value: boolean): void {
    this.visible.next(value);
  }

  update(input: ListModelInput): Observable<List<MetaData> | undefined> {
    return this.api.$request<object, ResultItemRes<List<MetaData>>>(
      `/api/v1/list/admin`, input, { method: 'PUT' },
    ).pipe(map(i => new List<MetaData>(i.item)));
  }
}

export default ListHttpService;
