import { useEffect } from 'react';
import { fromEvent } from 'rxjs';

const useEvent = (type: string, handler: (event: Event) => any) => {
  useEffect(() => {
    const sub = fromEvent(window, type)
      .subscribe(e => handler(e));

    return () => {
      if (sub) {
        sub.unsubscribe();
      }
    }
  })
}

export default useEvent;