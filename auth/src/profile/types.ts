import { Observable } from 'rxjs';

export interface IProfileUsecase<T extends object> {
  load: () => Observable<T>,
}