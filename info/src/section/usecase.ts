import { inject, injectable } from 'inversify';
import { PageService } from '@shared/stream/service';
import { IBaseListUsecase, IPageService, ListOptions } from '@shared/stream/types';
import { Observable, Subscription } from 'rxjs';
import { ListMV } from '@shared/stream/model';
import { InfoSectionServiceKey } from '../constants';
import { ISectionModel } from './dto';
import { IInfoSectionService } from './service/types';

export interface IInfoSectionUsecase {
  delete(id: number): Observable<void>

  getById(id: number): Observable<ISectionModel>

  create(input: ISectionModel): Observable<ISectionModel>

  update(input: ISectionModel): Observable<ISectionModel>
}

@injectable()
class InfoSectionUsecase implements IInfoSectionUsecase, IBaseListUsecase<ISectionModel> {
  public static diKey = Symbol.for('InfoSectionUsecaseDiKey');
  private infoSectionService: IInfoSectionService;
  private pageService: IPageService<ISectionModel>;

  constructor(
    @inject(InfoSectionServiceKey) infoSectionService: IInfoSectionService,
  ) {
    this.infoSectionService = infoSectionService;
    this.pageService = new PageService(this.infoSectionService);
  }

  loadItems(model: ListMV<ISectionModel>, opts: ListOptions, method?: string, query?: object): [Observable<ISectionModel>, Subscription] {
    return this.pageService.loadPage(model, opts, method, query);
  }

  loadTotalCount(model: ListMV<ISectionModel>, method?: string, query?: object): [Observable<number>, Subscription] {
    return this.pageService.loadTotalCount(model, method, query);
  }

  create(input: ISectionModel): Observable<ISectionModel> {
    return this.infoSectionService.create(input);
  }

  update(input: ISectionModel): Observable<ISectionModel> {
    return this.infoSectionService.update(input);
  }

  delete(id: number): Observable<void> {
    return this.infoSectionService.delete(id);
  }

  getById(id: number): Observable<ISectionModel> {
    return this.infoSectionService.getById(id);
  }
}

export default InfoSectionUsecase;