import { ContainerModule, interfaces } from 'inversify';
import SecurityGqlUsecase, { ISecurityGqlUsecase } from '../usecase';
import { ISecurityService } from '../service/types';
import { securityServiceDiKey } from '../constants';

export const SecurityGqlModule = <T extends interfaces.Newable<ISecurityService>>(service: T) => new ContainerModule((bind: interfaces.Bind) => {
  bind<ISecurityService>(securityServiceDiKey).to(service);
  bind<ISecurityGqlUsecase>(SecurityGqlUsecase.diKey).to(SecurityGqlUsecase).inSingletonScope();
});
