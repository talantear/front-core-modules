import { Observable } from 'rxjs';
import { IListSection, ISectionModel } from '../dto';
import { IExternalService, ListOptions } from '@shared/stream/types';

export interface IInfoSectionService extends IExternalService<ISectionModel> {
  delete(id: number): Observable<void>,
  getById(id: number): Observable<ISectionModel>,
  create(input: ISectionModel): Observable<ISectionModel>,
  update(input: ISectionModel): Observable<ISectionModel>,
  count(): Observable<number>,
  plainList(opts: ListOptions): Observable<ISectionModel>,
  list(value: IListSection): Observable<ISectionModel>,
}