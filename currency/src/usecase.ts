import { inject, injectable } from 'inversify';
import { Observable } from 'rxjs';
import { ICurrencyService } from './service/types';
import { currencyGqlService } from './contstants';

export interface ICurrencyUsecase {
  getRate(currency: string): Observable<number>
}

@injectable()
class CurrencyUsecase implements ICurrencyUsecase {
  public static diKey = Symbol.for('CurrencyUsecaseDiKey');

  private currencyService: ICurrencyService;

  constructor(
    @inject(currencyGqlService) currencyService: ICurrencyService,
  ) {
    this.currencyService = currencyService;
  }

  getRate(currency: string): Observable<number> {
    return this.currencyService.getRate(currency);
  }

  subscriber(currency: string): Observable<number> {
    return this.currencyService.subscriber(currency)
  }
}

export default CurrencyUsecase;
