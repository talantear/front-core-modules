import { Container, interfaces } from 'inversify';
import { UserModule } from './module';
import { IUserService } from '../service/types';

const userContainer = <T extends interfaces.Newable<IUserService>>(service: T): interfaces.Container => {
  const container = new Container();
  container.load(UserModule(service));
  return container;
}

export default userContainer;
