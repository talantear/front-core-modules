import { inject, injectable } from 'inversify';
import { Observable, Subject, tap } from 'rxjs';
import List from './di/model';
import { IListEditData } from './JsonList';
import { IListService } from './service/types';
import { listServiceKey } from './constants';
import { IListModel } from './dto';
import { MessageModule, IMessageHandler, MessageItem } from '@shared/message';

export interface IListUsecase<MetaData> {
  save(input: IListModel): Observable<List<MetaData> | undefined>;
  listByContext(context: string): Observable<List<MetaData>>;
  getByContext(context: string): Observable<List<MetaData> | undefined>;
  listByContexts(contexts: string[]): Observable<List<MetaData>>;
  getSubject(): Subject<IListEditData>;
  getSubscriberSubject(context: string): Subject<List<MetaData>>
  getVisible(): Subject<boolean>;
  setVisible(value: boolean): void;
  initSubject(context: string): void;
}

@injectable()
class ListUsecase<MetaData = {}> implements IListUsecase<MetaData> {
  public static diKey = Symbol.for('ListServiceDiKey');

  private listService: IListService<MetaData>;
  private messageHandler: IMessageHandler<string, MessageItem>;

  constructor(
    @inject(listServiceKey) listService: IListService<MetaData>,
    @inject(MessageModule.diKey) message: IMessageHandler<string, MessageItem>,
  ) {
    this.listService = listService;
    this.messageHandler = message;
  }

  initSubject(context: string) {
    this.listService.initSubject(context);
  }

  getSubject(): Subject<IListEditData> {
    return this.listService.getSubject();
  }

  getVisible(): Subject<boolean> {
    return this.listService.getVisible();
  }

  setVisible(value: boolean) {
    this.listService.setVisible(value);
  }

  getSubscriberSubject(context: string): Subject<List<MetaData>> {
    return this.listService.getSubscriberSubject(context);
  }

  save(input: IListModel): Observable<List<MetaData> | undefined> {
    if (!!input.id) {
      return this.listService.update(input).pipe(tap(() => {
        this.messageHandler.sendSuccess({ title: "common.success" });
      }));
    } else {
      return this.listService.create(input).pipe(tap(() => {
        this.messageHandler.sendSuccess({ title: "common.success" });
      }));
    }
  }

  getByContext(context: string): Observable<List<MetaData> | undefined> {
    return this.listService.getByContext(context);
  }

  listByContexts(contexts: string[]): Observable<List<MetaData>> {
    return this.listService.listByContexts(contexts);
  }

  listByContext(context: string): Observable<List<MetaData>> {
    return this.listService.listByContext(context);
  }
}

export default ListUsecase;
