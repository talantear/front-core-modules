import React, { FC } from 'react';
import { TransField } from '@locus/translate-react';
import type { TransFieldProps } from '@locus/translate-core/translate';
import { Input } from 'antd';

export interface ITransInput extends TransFieldProps {
  value?: string;
  required?: boolean;
  onChange: () => void
}

const TransFieldWidget: FC<ITransInput> = ({ required, ...props }) => {
  return (
    <TransField
      {...props}
      locKey={props.locKey} name={props.name} required={!!required}
    >
      <Input type="text" required={required} {...props} />
    </TransField>
  );
};

export default TransFieldWidget;
