export interface CGen<T> {
  new(...args: never[]): T
}