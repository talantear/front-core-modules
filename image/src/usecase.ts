import { inject, injectable } from 'inversify';
import { IImageUploadRes } from './models';
import { AppConfigDiKey } from '@shared/app/constants';
import { IAppConfig } from '@shared/app/types';
import { IImageService } from './service/types';
import { imageServiceDiKey } from './constants';

export interface IImageUsecase {
  upload: (file: File) => Promise<IImageUploadRes>,
  get: (width: number, height: number, key: string) => Promise<File>
}

@injectable()
class ImageUsecase implements IImageUsecase {
  public static diKey = Symbol.for('ImageUsecaseDiKey');

  private imageService: IImageService;
  private appConfig: IAppConfig;

  constructor(
    @inject(imageServiceDiKey) imageService: IImageService,
    @inject(AppConfigDiKey) appConfig: IAppConfig,
  ) {
    this.imageService = imageService;
    this.appConfig = appConfig;
  }

  get(width: number, height: number, key: string): Promise<File> {
    return this.imageService.get(width, height, key);
  }

  upload(file: File): Promise<IImageUploadRes> {
    return this.imageService.upload(file);
  }

  srcByKey(key: string | undefined, width: number = 300, height: number = 300, defaultValue: string = this.appConfig.defaultImage): string {
    if (!key) {
      return defaultValue;
    }

    return `${this.appConfig.imageStorageUrl}/api/v1/image/${Math.round(width * (window.devicePixelRatio || 1))}/${Math.round(height * (window.devicePixelRatio || 1))}?key=${key}`;
  }
}

export default ImageUsecase;
