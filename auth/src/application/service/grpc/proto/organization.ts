/* eslint-disable */
import { messageTypeRegistry } from "./typeRegistry";
import Long from "long";
import { grpc } from "@improbable-eng/grpc-web";
import _m0 from "protobufjs/minimal";
import { Observable } from "rxjs";
import { Id64Request, Int64Value } from "./commonTypes";
import { ListOptions } from "./paginatior";
import { Empty } from "./google/protobuf/empty";
import { BrowserHeaders } from "browser-headers";
import { share } from "rxjs/operators";

export const protobufPackage = "authMs";

export interface OrganizationModel {
  $type: "authMs.OrganizationModel";
  id: number;
  displayName: string;
  enableSoftDeletion: boolean;
  createdAt: number;
  slug: string;
  jwtSecret: string;
  jwtExpiresIn: number;
  passwordSaltSize: number;
  passwordMinSize: number;
}

const baseOrganizationModel: object = {
  $type: "authMs.OrganizationModel",
  id: 0,
  displayName: "",
  enableSoftDeletion: false,
  createdAt: 0,
  slug: "",
  jwtSecret: "",
  jwtExpiresIn: 0,
  passwordSaltSize: 0,
  passwordMinSize: 0,
};

export const OrganizationModel = {
  $type: "authMs.OrganizationModel" as const,

  encode(
    message: OrganizationModel,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.id !== 0) {
      writer.uint32(8).uint32(message.id);
    }
    if (message.displayName !== "") {
      writer.uint32(18).string(message.displayName);
    }
    if (message.enableSoftDeletion === true) {
      writer.uint32(24).bool(message.enableSoftDeletion);
    }
    if (message.createdAt !== 0) {
      writer.uint32(32).int64(message.createdAt);
    }
    if (message.slug !== "") {
      writer.uint32(42).string(message.slug);
    }
    if (message.jwtSecret !== "") {
      writer.uint32(50).string(message.jwtSecret);
    }
    if (message.jwtExpiresIn !== 0) {
      writer.uint32(56).int64(message.jwtExpiresIn);
    }
    if (message.passwordSaltSize !== 0) {
      writer.uint32(64).int64(message.passwordSaltSize);
    }
    if (message.passwordMinSize !== 0) {
      writer.uint32(72).int64(message.passwordMinSize);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): OrganizationModel {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseOrganizationModel } as OrganizationModel;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.uint32();
          break;
        case 2:
          message.displayName = reader.string();
          break;
        case 3:
          message.enableSoftDeletion = reader.bool();
          break;
        case 4:
          message.createdAt = longToNumber(reader.int64() as Long);
          break;
        case 5:
          message.slug = reader.string();
          break;
        case 6:
          message.jwtSecret = reader.string();
          break;
        case 7:
          message.jwtExpiresIn = longToNumber(reader.int64() as Long);
          break;
        case 8:
          message.passwordSaltSize = longToNumber(reader.int64() as Long);
          break;
        case 9:
          message.passwordMinSize = longToNumber(reader.int64() as Long);
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): OrganizationModel {
    const message = { ...baseOrganizationModel } as OrganizationModel;
    message.id =
      object.id !== undefined && object.id !== null ? Number(object.id) : 0;
    message.displayName =
      object.displayName !== undefined && object.displayName !== null
        ? String(object.displayName)
        : "";
    message.enableSoftDeletion =
      object.enableSoftDeletion !== undefined &&
      object.enableSoftDeletion !== null
        ? Boolean(object.enableSoftDeletion)
        : false;
    message.createdAt =
      object.createdAt !== undefined && object.createdAt !== null
        ? Number(object.createdAt)
        : 0;
    message.slug =
      object.slug !== undefined && object.slug !== null
        ? String(object.slug)
        : "";
    message.jwtSecret =
      object.jwtSecret !== undefined && object.jwtSecret !== null
        ? String(object.jwtSecret)
        : "";
    message.jwtExpiresIn =
      object.jwtExpiresIn !== undefined && object.jwtExpiresIn !== null
        ? Number(object.jwtExpiresIn)
        : 0;
    message.passwordSaltSize =
      object.passwordSaltSize !== undefined && object.passwordSaltSize !== null
        ? Number(object.passwordSaltSize)
        : 0;
    message.passwordMinSize =
      object.passwordMinSize !== undefined && object.passwordMinSize !== null
        ? Number(object.passwordMinSize)
        : 0;
    return message;
  },

  toJSON(message: OrganizationModel): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    message.displayName !== undefined &&
      (obj.displayName = message.displayName);
    message.enableSoftDeletion !== undefined &&
      (obj.enableSoftDeletion = message.enableSoftDeletion);
    message.createdAt !== undefined && (obj.createdAt = message.createdAt);
    message.slug !== undefined && (obj.slug = message.slug);
    message.jwtSecret !== undefined && (obj.jwtSecret = message.jwtSecret);
    message.jwtExpiresIn !== undefined &&
      (obj.jwtExpiresIn = message.jwtExpiresIn);
    message.passwordSaltSize !== undefined &&
      (obj.passwordSaltSize = message.passwordSaltSize);
    message.passwordMinSize !== undefined &&
      (obj.passwordMinSize = message.passwordMinSize);
    return obj;
  },

  fromPartial(object: DeepPartial<OrganizationModel>): OrganizationModel {
    const message = { ...baseOrganizationModel } as OrganizationModel;
    message.id = object.id ?? 0;
    message.displayName = object.displayName ?? "";
    message.enableSoftDeletion = object.enableSoftDeletion ?? false;
    message.createdAt = object.createdAt ?? 0;
    message.slug = object.slug ?? "";
    message.jwtSecret = object.jwtSecret ?? "";
    message.jwtExpiresIn = object.jwtExpiresIn ?? 0;
    message.passwordSaltSize = object.passwordSaltSize ?? 0;
    message.passwordMinSize = object.passwordMinSize ?? 0;
    return message;
  },
};

messageTypeRegistry.set(OrganizationModel.$type, OrganizationModel);

/** Represents user methods. */
export interface Organization {
  /** / Create organization */
  Create(
    request: DeepPartial<OrganizationModel>,
    metadata?: grpc.Metadata
  ): Promise<OrganizationModel>;
  /** / Update organization */
  Update(
    request: DeepPartial<OrganizationModel>,
    metadata?: grpc.Metadata
  ): Promise<OrganizationModel>;
  /** / GetById organization */
  GetById(
    request: DeepPartial<Id64Request>,
    metadata?: grpc.Metadata
  ): Promise<OrganizationModel>;
  /** / List organization */
  List(
    request: DeepPartial<ListOptions>,
    metadata?: grpc.Metadata
  ): Observable<OrganizationModel>;
  /** / Count organization */
  Count(
    request: DeepPartial<Empty>,
    metadata?: grpc.Metadata
  ): Promise<Int64Value>;
  /** / Count application */
  Remove(
    request: DeepPartial<Id64Request>,
    metadata?: grpc.Metadata
  ): Promise<Empty>;
}

export class OrganizationClientImpl implements Organization {
  private readonly rpc: Rpc;

  constructor(rpc: Rpc) {
    this.rpc = rpc;
    this.Create = this.Create.bind(this);
    this.Update = this.Update.bind(this);
    this.GetById = this.GetById.bind(this);
    this.List = this.List.bind(this);
    this.Count = this.Count.bind(this);
    this.Remove = this.Remove.bind(this);
  }

  Create(
    request: DeepPartial<OrganizationModel>,
    metadata?: grpc.Metadata
  ): Promise<OrganizationModel> {
    return this.rpc.unary(
      OrganizationCreateDesc,
      OrganizationModel.fromPartial(request),
      metadata
    );
  }

  Update(
    request: DeepPartial<OrganizationModel>,
    metadata?: grpc.Metadata
  ): Promise<OrganizationModel> {
    return this.rpc.unary(
      OrganizationUpdateDesc,
      OrganizationModel.fromPartial(request),
      metadata
    );
  }

  GetById(
    request: DeepPartial<Id64Request>,
    metadata?: grpc.Metadata
  ): Promise<OrganizationModel> {
    return this.rpc.unary(
      OrganizationGetByIdDesc,
      Id64Request.fromPartial(request),
      metadata
    );
  }

  List(
    request: DeepPartial<ListOptions>,
    metadata?: grpc.Metadata
  ): Observable<OrganizationModel> {
    return this.rpc.invoke(
      OrganizationListDesc,
      ListOptions.fromPartial(request),
      metadata
    );
  }

  Count(
    request: DeepPartial<Empty>,
    metadata?: grpc.Metadata
  ): Promise<Int64Value> {
    return this.rpc.unary(
      OrganizationCountDesc,
      Empty.fromPartial(request),
      metadata
    );
  }

  Remove(
    request: DeepPartial<Id64Request>,
    metadata?: grpc.Metadata
  ): Promise<Empty> {
    return this.rpc.unary(
      OrganizationRemoveDesc,
      Id64Request.fromPartial(request),
      metadata
    );
  }
}

export const OrganizationDesc = {
  serviceName: "authMs.Organization",
};

export const OrganizationCreateDesc: UnaryMethodDefinitionish = {
  methodName: "Create",
  service: OrganizationDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return OrganizationModel.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...OrganizationModel.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const OrganizationUpdateDesc: UnaryMethodDefinitionish = {
  methodName: "Update",
  service: OrganizationDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return OrganizationModel.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...OrganizationModel.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const OrganizationGetByIdDesc: UnaryMethodDefinitionish = {
  methodName: "GetById",
  service: OrganizationDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return Id64Request.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...OrganizationModel.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const OrganizationListDesc: UnaryMethodDefinitionish = {
  methodName: "List",
  service: OrganizationDesc,
  requestStream: false,
  responseStream: true,
  requestType: {
    serializeBinary() {
      return ListOptions.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...OrganizationModel.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const OrganizationCountDesc: UnaryMethodDefinitionish = {
  methodName: "Count",
  service: OrganizationDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return Empty.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...Int64Value.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const OrganizationRemoveDesc: UnaryMethodDefinitionish = {
  methodName: "Remove",
  service: OrganizationDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return Id64Request.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...Empty.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

interface UnaryMethodDefinitionishR
  extends grpc.UnaryMethodDefinition<any, any> {
  requestStream: any;
  responseStream: any;
}

type UnaryMethodDefinitionish = UnaryMethodDefinitionishR;

interface Rpc {
  unary<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    request: any,
    metadata: grpc.Metadata | undefined
  ): Promise<any>;
  invoke<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    request: any,
    metadata: grpc.Metadata | undefined
  ): Observable<any>;
}

export class GrpcWebImpl {
  private host: string;
  private options: {
    transport?: grpc.TransportFactory;
    streamingTransport?: grpc.TransportFactory;
    debug?: boolean;
    metadata?: grpc.Metadata;
  };

  constructor(
    host: string,
    options: {
      transport?: grpc.TransportFactory;
      streamingTransport?: grpc.TransportFactory;
      debug?: boolean;
      metadata?: grpc.Metadata;
    }
  ) {
    this.host = host;
    this.options = options;
  }

  unary<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    _request: any,
    metadata: grpc.Metadata | undefined
  ): Promise<any> {
    const request = { ..._request, ...methodDesc.requestType };
    const maybeCombinedMetadata =
      metadata && this.options.metadata
        ? new BrowserHeaders({
            ...this.options?.metadata.headersMap,
            ...metadata?.headersMap,
          })
        : metadata || this.options.metadata;
    return new Promise((resolve, reject) => {
      grpc.unary(methodDesc, {
        request,
        host: this.host,
        metadata: maybeCombinedMetadata,
        transport: this.options.transport,
        debug: this.options.debug,
        onEnd: function (response) {
          if (response.status === grpc.Code.OK) {
            resolve(response.message);
          } else {
            const err = new Error(response.statusMessage) as any;
            err.code = response.status;
            err.metadata = response.trailers;
            reject(err);
          }
        },
      });
    });
  }

  invoke<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    _request: any,
    metadata: grpc.Metadata | undefined
  ): Observable<any> {
    // Status Response Codes (https://developers.google.com/maps-booking/reference/grpc-api/status_codes)
    const upStreamCodes = [2, 4, 8, 9, 10, 13, 14, 15];
    const DEFAULT_TIMEOUT_TIME: number = 3_000;
    const request = { ..._request, ...methodDesc.requestType };
    const maybeCombinedMetadata =
      metadata && this.options.metadata
        ? new BrowserHeaders({
            ...this.options?.metadata.headersMap,
            ...metadata?.headersMap,
          })
        : metadata || this.options.metadata;
    return new Observable((observer) => {
      const upStream = () => {
        const client = grpc.invoke(methodDesc, {
          host: this.host,
          request,
          transport: this.options.streamingTransport || this.options.transport,
          metadata: maybeCombinedMetadata,
          debug: this.options.debug,
          onMessage: (next) => observer.next(next),
          onEnd: (code: grpc.Code, message: string) => {
            if (code === 0) {
              observer.complete();
            } else if (upStreamCodes.includes(code)) {
              setTimeout(upStream, DEFAULT_TIMEOUT_TIME);
            } else {
              observer.error(new Error(`Error ${code} ${message}`));
            }
          },
        });
        observer.add(() => client.close());
      };
      upStream();
    }).pipe(share());
  }
}

declare var self: any | undefined;
declare var window: any | undefined;
declare var global: any | undefined;
var globalThis: any = (() => {
  if (typeof globalThis !== "undefined") return globalThis;
  if (typeof self !== "undefined") return self;
  if (typeof window !== "undefined") return window;
  if (typeof global !== "undefined") return global;
  throw "Unable to locate global object";
})();

type Builtin =
  | Date
  | Function
  | Uint8Array
  | string
  | number
  | boolean
  | undefined;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? { [K in Exclude<keyof T, "$type">]?: DeepPartial<T[K]> }
  : Partial<T>;

function longToNumber(long: Long): number {
  if (long.gt(Number.MAX_SAFE_INTEGER)) {
    throw new globalThis.Error("Value is larger than Number.MAX_SAFE_INTEGER");
  }
  return long.toNumber();
}

if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
