export interface ISignUpModel {
  id: number;
  title: string;
  name: string;
}
