export interface ILoadingEvent {
  kind: string
  context: string
  size: number
}

export interface ILoading {
  visible: boolean
}

export type ILoadingCallbackType = (value: ILoadingEvent) => void

export interface ILoadingMetaData {
  withTimeout?: boolean // for routing
  defaultValue?: boolean, // visible loading before request or after request
  changeHandler?: (value: boolean) => void, // custom set visible
}