import { ContainerModule, interfaces } from 'inversify';
import ApplicationUsecase, { IApplicationUsecase } from '../usecase';

import { IApplicationService } from '../service/types';
import { applicationServiceDiKey } from '../constants';

export const ApplicationModule = <T extends interfaces.Newable<IApplicationService>>(service: T) => new ContainerModule((bind: interfaces.Bind) => {
  bind<IApplicationService>(applicationServiceDiKey).to(service);
  bind<IApplicationUsecase>(ApplicationUsecase.diKey).to(ApplicationUsecase).inSingletonScope();
});
