import gql from 'graphql-tag';
export type Maybe<T> = T | undefined;
export type InputMaybe<T> = T | undefined;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
export type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  Int32: number;
  Int64: number;
  Uint32: number;
  Uint64: number;
  Unix: number;
  Void: void;
};

export type ListOptions = {
  limit: Scalars['Int32'];
  offset: Scalars['Int32'];
  order?: InputMaybe<Scalars['String']>;
};

export type Mutation = {
  root: Scalars['Boolean'];
  securityCreate?: Maybe<Scalars['Void']>;
};


export type MutationSecurityCreateArgs = {
  in: SecurityCreateInput;
};

export type Paginator = {
  countPerPage: Scalars['Int32'];
  offset: Scalars['Int32'];
  totalCount: Scalars['Int64'];
};

export type Query = {
  root: Scalars['Boolean'];
};

export type SecurityCreateInput = {
  type: Scalars['String'];
};

export type Subscription = {
  root: Scalars['Boolean'];
};

export type SecurityCreateMutationVariables = Exact<{
  input: SecurityCreateInput;
}>;


export type SecurityCreateMutation = { securityCreate?: void | undefined };


export const SecurityCreateDocument = gql`
    mutation SecurityCreate($input: SecurityCreateInput!) {
  securityCreate(in: $input)
}
    `;