import { inject, injectable } from 'inversify';
import { StructureGetStructureCountDesc, UserWithDepthReq } from '../../../proto/structure';
import { ApiClient } from '@shared/grpc-client/model';
import { AppConfigDiKey } from '@shared/app/constants';
import { IAppConfig } from '@shared/app/types';
import { map, Observable } from 'rxjs';
import { IStructService } from '../types';
import { IInt64Value } from '@shared/types/dist/src/dto';

@injectable()
class StructService implements IStructService {
  private api: ApiClient;
  private appConfig: IAppConfig;

  public constructor(
    @inject(ApiClient.diKey) api: ApiClient,
    @inject(AppConfigDiKey) appConfig: IAppConfig,
  ) {
    this.api = api;
    this.appConfig = appConfig;
  }

  getStructCount(userId: number, maxDepth?: number): Observable<number> {
    const req = UserWithDepthReq.fromPartial({
      userId,
      maxDepth
    })
    return this.api.stream$(
      StructureGetStructureCountDesc,
      {
        obj: req,
        coder: UserWithDepthReq,
      },
      { host: this.appConfig.authUrlServer }
    ).pipe(map<IInt64Value, number>(i => i.value))
  }
}

export default StructService;