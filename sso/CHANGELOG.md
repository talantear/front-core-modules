# @shared/sso

## 1.1.0

### Minor Changes

- Update packages

## 1.0.4

### Patch Changes

- Added lang to url query

## 1.0.3

### Patch Changes

- Updaated exports

## 1.0.2

### Patch Changes

- Updated exports paths

## 1.0.1

### Patch Changes

- Updated paths
