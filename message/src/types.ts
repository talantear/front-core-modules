export interface MessageItem {
  title: string,
  description?: string
}

export interface IMessageHandler<Type = string, Data = object> {
  send: (type: Type, data: Data) => void
  sendInfo: (data: Data) => void
  sendSuccess: (data: Data) => void
  sendError: (data: Data) => void
}
