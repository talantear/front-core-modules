/* eslint-disable */
import { messageTypeRegistry } from "./typeRegistry";
import Long from "long";
import { grpc } from "@improbable-eng/grpc-web";
import _m0 from "protobufjs/minimal";
import { ListOptions } from "./paginatior";
import { Observable } from "rxjs";
import { Id64Request, Int64Value } from "./commonTypes";
import { Empty } from "./google/protobuf/empty";
import { BrowserHeaders } from "browser-headers";
import { share } from "rxjs/operators";

export const protobufPackage = "authMs";

export interface PolicyModel {
  $type: "authMs.PolicyModel";
  id: number;
  ptype: string;
  v0?: string | undefined;
  v1?: string | undefined;
  v2?: string | undefined;
  v3?: string | undefined;
  v4?: string | undefined;
  v5?: string | undefined;
}

export interface ListRolesRes {
  $type: "authMs.ListRolesRes";
  roles: string[];
}

export interface PolicyFilterReq {
  $type: "authMs.PolicyFilterReq";
  slug: string;
  opts?: ListOptions;
}

export interface PolicyFilterModel {
  $type: "authMs.PolicyFilterModel";
  app?: string | undefined;
  org: string;
}

export interface SearchPolicyReq {
  $type: "authMs.SearchPolicyReq";
  query: string;
  opts?: ListOptions;
  filter?: PolicyFilterModel | undefined;
}

export interface SearchPolicyCountReq {
  $type: "authMs.SearchPolicyCountReq";
  query: string;
  filter?: PolicyFilterModel | undefined;
}

export interface GetGroupingPolicyReq {
  $type: "authMs.GetGroupingPolicyReq";
  opts?: ListOptions;
  filter?: PolicyFilterModel | undefined;
}

export interface GetGroupingPolicyCountReq {
  $type: "authMs.GetGroupingPolicyCountReq";
  filter?: PolicyFilterModel | undefined;
}

export interface GetPolicyReq {
  $type: "authMs.GetPolicyReq";
  opts?: ListOptions;
  filter?: PolicyFilterModel | undefined;
}

export interface GetPolicyCountReq {
  $type: "authMs.GetPolicyCountReq";
  filter?: PolicyFilterModel | undefined;
}

const basePolicyModel: object = {
  $type: "authMs.PolicyModel",
  id: 0,
  ptype: "",
};

export const PolicyModel = {
  $type: "authMs.PolicyModel" as const,

  encode(
    message: PolicyModel,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.id !== 0) {
      writer.uint32(8).uint64(message.id);
    }
    if (message.ptype !== "") {
      writer.uint32(18).string(message.ptype);
    }
    if (message.v0 !== undefined) {
      writer.uint32(26).string(message.v0);
    }
    if (message.v1 !== undefined) {
      writer.uint32(34).string(message.v1);
    }
    if (message.v2 !== undefined) {
      writer.uint32(42).string(message.v2);
    }
    if (message.v3 !== undefined) {
      writer.uint32(50).string(message.v3);
    }
    if (message.v4 !== undefined) {
      writer.uint32(58).string(message.v4);
    }
    if (message.v5 !== undefined) {
      writer.uint32(66).string(message.v5);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): PolicyModel {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...basePolicyModel } as PolicyModel;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = longToNumber(reader.uint64() as Long);
          break;
        case 2:
          message.ptype = reader.string();
          break;
        case 3:
          message.v0 = reader.string();
          break;
        case 4:
          message.v1 = reader.string();
          break;
        case 5:
          message.v2 = reader.string();
          break;
        case 6:
          message.v3 = reader.string();
          break;
        case 7:
          message.v4 = reader.string();
          break;
        case 8:
          message.v5 = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): PolicyModel {
    const message = { ...basePolicyModel } as PolicyModel;
    message.id =
      object.id !== undefined && object.id !== null ? Number(object.id) : 0;
    message.ptype =
      object.ptype !== undefined && object.ptype !== null
        ? String(object.ptype)
        : "";
    message.v0 =
      object.v0 !== undefined && object.v0 !== null
        ? String(object.v0)
        : undefined;
    message.v1 =
      object.v1 !== undefined && object.v1 !== null
        ? String(object.v1)
        : undefined;
    message.v2 =
      object.v2 !== undefined && object.v2 !== null
        ? String(object.v2)
        : undefined;
    message.v3 =
      object.v3 !== undefined && object.v3 !== null
        ? String(object.v3)
        : undefined;
    message.v4 =
      object.v4 !== undefined && object.v4 !== null
        ? String(object.v4)
        : undefined;
    message.v5 =
      object.v5 !== undefined && object.v5 !== null
        ? String(object.v5)
        : undefined;
    return message;
  },

  toJSON(message: PolicyModel): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    message.ptype !== undefined && (obj.ptype = message.ptype);
    message.v0 !== undefined && (obj.v0 = message.v0);
    message.v1 !== undefined && (obj.v1 = message.v1);
    message.v2 !== undefined && (obj.v2 = message.v2);
    message.v3 !== undefined && (obj.v3 = message.v3);
    message.v4 !== undefined && (obj.v4 = message.v4);
    message.v5 !== undefined && (obj.v5 = message.v5);
    return obj;
  },

  fromPartial(object: DeepPartial<PolicyModel>): PolicyModel {
    const message = { ...basePolicyModel } as PolicyModel;
    message.id = object.id ?? 0;
    message.ptype = object.ptype ?? "";
    message.v0 = object.v0 ?? undefined;
    message.v1 = object.v1 ?? undefined;
    message.v2 = object.v2 ?? undefined;
    message.v3 = object.v3 ?? undefined;
    message.v4 = object.v4 ?? undefined;
    message.v5 = object.v5 ?? undefined;
    return message;
  },
};

messageTypeRegistry.set(PolicyModel.$type, PolicyModel);

const baseListRolesRes: object = { $type: "authMs.ListRolesRes", roles: "" };

export const ListRolesRes = {
  $type: "authMs.ListRolesRes" as const,

  encode(
    message: ListRolesRes,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    for (const v of message.roles) {
      writer.uint32(10).string(v!);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ListRolesRes {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseListRolesRes } as ListRolesRes;
    message.roles = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.roles.push(reader.string());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ListRolesRes {
    const message = { ...baseListRolesRes } as ListRolesRes;
    message.roles = (object.roles ?? []).map((e: any) => String(e));
    return message;
  },

  toJSON(message: ListRolesRes): unknown {
    const obj: any = {};
    if (message.roles) {
      obj.roles = message.roles.map((e) => e);
    } else {
      obj.roles = [];
    }
    return obj;
  },

  fromPartial(object: DeepPartial<ListRolesRes>): ListRolesRes {
    const message = { ...baseListRolesRes } as ListRolesRes;
    message.roles = (object.roles ?? []).map((e) => e);
    return message;
  },
};

messageTypeRegistry.set(ListRolesRes.$type, ListRolesRes);

const basePolicyFilterReq: object = {
  $type: "authMs.PolicyFilterReq",
  slug: "",
};

export const PolicyFilterReq = {
  $type: "authMs.PolicyFilterReq" as const,

  encode(
    message: PolicyFilterReq,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.slug !== "") {
      writer.uint32(10).string(message.slug);
    }
    if (message.opts !== undefined) {
      ListOptions.encode(message.opts, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): PolicyFilterReq {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...basePolicyFilterReq } as PolicyFilterReq;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.slug = reader.string();
          break;
        case 2:
          message.opts = ListOptions.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): PolicyFilterReq {
    const message = { ...basePolicyFilterReq } as PolicyFilterReq;
    message.slug =
      object.slug !== undefined && object.slug !== null
        ? String(object.slug)
        : "";
    message.opts =
      object.opts !== undefined && object.opts !== null
        ? ListOptions.fromJSON(object.opts)
        : undefined;
    return message;
  },

  toJSON(message: PolicyFilterReq): unknown {
    const obj: any = {};
    message.slug !== undefined && (obj.slug = message.slug);
    message.opts !== undefined &&
      (obj.opts = message.opts ? ListOptions.toJSON(message.opts) : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<PolicyFilterReq>): PolicyFilterReq {
    const message = { ...basePolicyFilterReq } as PolicyFilterReq;
    message.slug = object.slug ?? "";
    message.opts =
      object.opts !== undefined && object.opts !== null
        ? ListOptions.fromPartial(object.opts)
        : undefined;
    return message;
  },
};

messageTypeRegistry.set(PolicyFilterReq.$type, PolicyFilterReq);

const basePolicyFilterModel: object = {
  $type: "authMs.PolicyFilterModel",
  org: "",
};

export const PolicyFilterModel = {
  $type: "authMs.PolicyFilterModel" as const,

  encode(
    message: PolicyFilterModel,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.app !== undefined) {
      writer.uint32(10).string(message.app);
    }
    if (message.org !== "") {
      writer.uint32(18).string(message.org);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): PolicyFilterModel {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...basePolicyFilterModel } as PolicyFilterModel;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.app = reader.string();
          break;
        case 2:
          message.org = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): PolicyFilterModel {
    const message = { ...basePolicyFilterModel } as PolicyFilterModel;
    message.app =
      object.app !== undefined && object.app !== null
        ? String(object.app)
        : undefined;
    message.org =
      object.org !== undefined && object.org !== null ? String(object.org) : "";
    return message;
  },

  toJSON(message: PolicyFilterModel): unknown {
    const obj: any = {};
    message.app !== undefined && (obj.app = message.app);
    message.org !== undefined && (obj.org = message.org);
    return obj;
  },

  fromPartial(object: DeepPartial<PolicyFilterModel>): PolicyFilterModel {
    const message = { ...basePolicyFilterModel } as PolicyFilterModel;
    message.app = object.app ?? undefined;
    message.org = object.org ?? "";
    return message;
  },
};

messageTypeRegistry.set(PolicyFilterModel.$type, PolicyFilterModel);

const baseSearchPolicyReq: object = {
  $type: "authMs.SearchPolicyReq",
  query: "",
};

export const SearchPolicyReq = {
  $type: "authMs.SearchPolicyReq" as const,

  encode(
    message: SearchPolicyReq,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.query !== "") {
      writer.uint32(10).string(message.query);
    }
    if (message.opts !== undefined) {
      ListOptions.encode(message.opts, writer.uint32(18).fork()).ldelim();
    }
    if (message.filter !== undefined) {
      PolicyFilterModel.encode(
        message.filter,
        writer.uint32(26).fork()
      ).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): SearchPolicyReq {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseSearchPolicyReq } as SearchPolicyReq;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.query = reader.string();
          break;
        case 2:
          message.opts = ListOptions.decode(reader, reader.uint32());
          break;
        case 3:
          message.filter = PolicyFilterModel.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): SearchPolicyReq {
    const message = { ...baseSearchPolicyReq } as SearchPolicyReq;
    message.query =
      object.query !== undefined && object.query !== null
        ? String(object.query)
        : "";
    message.opts =
      object.opts !== undefined && object.opts !== null
        ? ListOptions.fromJSON(object.opts)
        : undefined;
    message.filter =
      object.filter !== undefined && object.filter !== null
        ? PolicyFilterModel.fromJSON(object.filter)
        : undefined;
    return message;
  },

  toJSON(message: SearchPolicyReq): unknown {
    const obj: any = {};
    message.query !== undefined && (obj.query = message.query);
    message.opts !== undefined &&
      (obj.opts = message.opts ? ListOptions.toJSON(message.opts) : undefined);
    message.filter !== undefined &&
      (obj.filter = message.filter
        ? PolicyFilterModel.toJSON(message.filter)
        : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<SearchPolicyReq>): SearchPolicyReq {
    const message = { ...baseSearchPolicyReq } as SearchPolicyReq;
    message.query = object.query ?? "";
    message.opts =
      object.opts !== undefined && object.opts !== null
        ? ListOptions.fromPartial(object.opts)
        : undefined;
    message.filter =
      object.filter !== undefined && object.filter !== null
        ? PolicyFilterModel.fromPartial(object.filter)
        : undefined;
    return message;
  },
};

messageTypeRegistry.set(SearchPolicyReq.$type, SearchPolicyReq);

const baseSearchPolicyCountReq: object = {
  $type: "authMs.SearchPolicyCountReq",
  query: "",
};

export const SearchPolicyCountReq = {
  $type: "authMs.SearchPolicyCountReq" as const,

  encode(
    message: SearchPolicyCountReq,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.query !== "") {
      writer.uint32(10).string(message.query);
    }
    if (message.filter !== undefined) {
      PolicyFilterModel.encode(
        message.filter,
        writer.uint32(18).fork()
      ).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): SearchPolicyCountReq {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseSearchPolicyCountReq } as SearchPolicyCountReq;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.query = reader.string();
          break;
        case 2:
          message.filter = PolicyFilterModel.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): SearchPolicyCountReq {
    const message = { ...baseSearchPolicyCountReq } as SearchPolicyCountReq;
    message.query =
      object.query !== undefined && object.query !== null
        ? String(object.query)
        : "";
    message.filter =
      object.filter !== undefined && object.filter !== null
        ? PolicyFilterModel.fromJSON(object.filter)
        : undefined;
    return message;
  },

  toJSON(message: SearchPolicyCountReq): unknown {
    const obj: any = {};
    message.query !== undefined && (obj.query = message.query);
    message.filter !== undefined &&
      (obj.filter = message.filter
        ? PolicyFilterModel.toJSON(message.filter)
        : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<SearchPolicyCountReq>): SearchPolicyCountReq {
    const message = { ...baseSearchPolicyCountReq } as SearchPolicyCountReq;
    message.query = object.query ?? "";
    message.filter =
      object.filter !== undefined && object.filter !== null
        ? PolicyFilterModel.fromPartial(object.filter)
        : undefined;
    return message;
  },
};

messageTypeRegistry.set(SearchPolicyCountReq.$type, SearchPolicyCountReq);

const baseGetGroupingPolicyReq: object = {
  $type: "authMs.GetGroupingPolicyReq",
};

export const GetGroupingPolicyReq = {
  $type: "authMs.GetGroupingPolicyReq" as const,

  encode(
    message: GetGroupingPolicyReq,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.opts !== undefined) {
      ListOptions.encode(message.opts, writer.uint32(10).fork()).ldelim();
    }
    if (message.filter !== undefined) {
      PolicyFilterModel.encode(
        message.filter,
        writer.uint32(18).fork()
      ).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): GetGroupingPolicyReq {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseGetGroupingPolicyReq } as GetGroupingPolicyReq;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.opts = ListOptions.decode(reader, reader.uint32());
          break;
        case 2:
          message.filter = PolicyFilterModel.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): GetGroupingPolicyReq {
    const message = { ...baseGetGroupingPolicyReq } as GetGroupingPolicyReq;
    message.opts =
      object.opts !== undefined && object.opts !== null
        ? ListOptions.fromJSON(object.opts)
        : undefined;
    message.filter =
      object.filter !== undefined && object.filter !== null
        ? PolicyFilterModel.fromJSON(object.filter)
        : undefined;
    return message;
  },

  toJSON(message: GetGroupingPolicyReq): unknown {
    const obj: any = {};
    message.opts !== undefined &&
      (obj.opts = message.opts ? ListOptions.toJSON(message.opts) : undefined);
    message.filter !== undefined &&
      (obj.filter = message.filter
        ? PolicyFilterModel.toJSON(message.filter)
        : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<GetGroupingPolicyReq>): GetGroupingPolicyReq {
    const message = { ...baseGetGroupingPolicyReq } as GetGroupingPolicyReq;
    message.opts =
      object.opts !== undefined && object.opts !== null
        ? ListOptions.fromPartial(object.opts)
        : undefined;
    message.filter =
      object.filter !== undefined && object.filter !== null
        ? PolicyFilterModel.fromPartial(object.filter)
        : undefined;
    return message;
  },
};

messageTypeRegistry.set(GetGroupingPolicyReq.$type, GetGroupingPolicyReq);

const baseGetGroupingPolicyCountReq: object = {
  $type: "authMs.GetGroupingPolicyCountReq",
};

export const GetGroupingPolicyCountReq = {
  $type: "authMs.GetGroupingPolicyCountReq" as const,

  encode(
    message: GetGroupingPolicyCountReq,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.filter !== undefined) {
      PolicyFilterModel.encode(
        message.filter,
        writer.uint32(10).fork()
      ).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): GetGroupingPolicyCountReq {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseGetGroupingPolicyCountReq,
    } as GetGroupingPolicyCountReq;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.filter = PolicyFilterModel.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): GetGroupingPolicyCountReq {
    const message = {
      ...baseGetGroupingPolicyCountReq,
    } as GetGroupingPolicyCountReq;
    message.filter =
      object.filter !== undefined && object.filter !== null
        ? PolicyFilterModel.fromJSON(object.filter)
        : undefined;
    return message;
  },

  toJSON(message: GetGroupingPolicyCountReq): unknown {
    const obj: any = {};
    message.filter !== undefined &&
      (obj.filter = message.filter
        ? PolicyFilterModel.toJSON(message.filter)
        : undefined);
    return obj;
  },

  fromPartial(
    object: DeepPartial<GetGroupingPolicyCountReq>
  ): GetGroupingPolicyCountReq {
    const message = {
      ...baseGetGroupingPolicyCountReq,
    } as GetGroupingPolicyCountReq;
    message.filter =
      object.filter !== undefined && object.filter !== null
        ? PolicyFilterModel.fromPartial(object.filter)
        : undefined;
    return message;
  },
};

messageTypeRegistry.set(
  GetGroupingPolicyCountReq.$type,
  GetGroupingPolicyCountReq
);

const baseGetPolicyReq: object = { $type: "authMs.GetPolicyReq" };

export const GetPolicyReq = {
  $type: "authMs.GetPolicyReq" as const,

  encode(
    message: GetPolicyReq,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.opts !== undefined) {
      ListOptions.encode(message.opts, writer.uint32(10).fork()).ldelim();
    }
    if (message.filter !== undefined) {
      PolicyFilterModel.encode(
        message.filter,
        writer.uint32(18).fork()
      ).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): GetPolicyReq {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseGetPolicyReq } as GetPolicyReq;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.opts = ListOptions.decode(reader, reader.uint32());
          break;
        case 2:
          message.filter = PolicyFilterModel.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): GetPolicyReq {
    const message = { ...baseGetPolicyReq } as GetPolicyReq;
    message.opts =
      object.opts !== undefined && object.opts !== null
        ? ListOptions.fromJSON(object.opts)
        : undefined;
    message.filter =
      object.filter !== undefined && object.filter !== null
        ? PolicyFilterModel.fromJSON(object.filter)
        : undefined;
    return message;
  },

  toJSON(message: GetPolicyReq): unknown {
    const obj: any = {};
    message.opts !== undefined &&
      (obj.opts = message.opts ? ListOptions.toJSON(message.opts) : undefined);
    message.filter !== undefined &&
      (obj.filter = message.filter
        ? PolicyFilterModel.toJSON(message.filter)
        : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<GetPolicyReq>): GetPolicyReq {
    const message = { ...baseGetPolicyReq } as GetPolicyReq;
    message.opts =
      object.opts !== undefined && object.opts !== null
        ? ListOptions.fromPartial(object.opts)
        : undefined;
    message.filter =
      object.filter !== undefined && object.filter !== null
        ? PolicyFilterModel.fromPartial(object.filter)
        : undefined;
    return message;
  },
};

messageTypeRegistry.set(GetPolicyReq.$type, GetPolicyReq);

const baseGetPolicyCountReq: object = { $type: "authMs.GetPolicyCountReq" };

export const GetPolicyCountReq = {
  $type: "authMs.GetPolicyCountReq" as const,

  encode(
    message: GetPolicyCountReq,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.filter !== undefined) {
      PolicyFilterModel.encode(
        message.filter,
        writer.uint32(10).fork()
      ).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): GetPolicyCountReq {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseGetPolicyCountReq } as GetPolicyCountReq;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.filter = PolicyFilterModel.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): GetPolicyCountReq {
    const message = { ...baseGetPolicyCountReq } as GetPolicyCountReq;
    message.filter =
      object.filter !== undefined && object.filter !== null
        ? PolicyFilterModel.fromJSON(object.filter)
        : undefined;
    return message;
  },

  toJSON(message: GetPolicyCountReq): unknown {
    const obj: any = {};
    message.filter !== undefined &&
      (obj.filter = message.filter
        ? PolicyFilterModel.toJSON(message.filter)
        : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<GetPolicyCountReq>): GetPolicyCountReq {
    const message = { ...baseGetPolicyCountReq } as GetPolicyCountReq;
    message.filter =
      object.filter !== undefined && object.filter !== null
        ? PolicyFilterModel.fromPartial(object.filter)
        : undefined;
    return message;
  },
};

messageTypeRegistry.set(GetPolicyCountReq.$type, GetPolicyCountReq);

export interface Policy {
  GetGroupingPolicy(
    request: DeepPartial<GetGroupingPolicyReq>,
    metadata?: grpc.Metadata
  ): Observable<PolicyModel>;
  PolicyGroupCount(
    request: DeepPartial<GetGroupingPolicyCountReq>,
    metadata?: grpc.Metadata
  ): Promise<Int64Value>;
  GetPolicy(
    request: DeepPartial<GetPolicyReq>,
    metadata?: grpc.Metadata
  ): Observable<PolicyModel>;
  PolicyCount(
    request: DeepPartial<GetPolicyCountReq>,
    metadata?: grpc.Metadata
  ): Promise<Int64Value>;
  AddPolicy(
    request: DeepPartial<PolicyModel>,
    metadata?: grpc.Metadata
  ): Promise<PolicyModel>;
  RemovePolicy(
    request: DeepPartial<Id64Request>,
    metadata?: grpc.Metadata
  ): Promise<Empty>;
  UpdatePolicy(
    request: DeepPartial<PolicyModel>,
    metadata?: grpc.Metadata
  ): Promise<Empty>;
  GetAllRoles(
    request: DeepPartial<Empty>,
    metadata?: grpc.Metadata
  ): Promise<ListRolesRes>;
  SearchPolicy(
    request: DeepPartial<SearchPolicyReq>,
    metadata?: grpc.Metadata
  ): Observable<PolicyModel>;
  SearchPolicyCount(
    request: DeepPartial<SearchPolicyCountReq>,
    metadata?: grpc.Metadata
  ): Promise<Int64Value>;
  GetById(
    request: DeepPartial<Id64Request>,
    metadata?: grpc.Metadata
  ): Promise<PolicyModel>;
}

export class PolicyClientImpl implements Policy {
  private readonly rpc: Rpc;

  constructor(rpc: Rpc) {
    this.rpc = rpc;
    this.GetGroupingPolicy = this.GetGroupingPolicy.bind(this);
    this.PolicyGroupCount = this.PolicyGroupCount.bind(this);
    this.GetPolicy = this.GetPolicy.bind(this);
    this.PolicyCount = this.PolicyCount.bind(this);
    this.AddPolicy = this.AddPolicy.bind(this);
    this.RemovePolicy = this.RemovePolicy.bind(this);
    this.UpdatePolicy = this.UpdatePolicy.bind(this);
    this.GetAllRoles = this.GetAllRoles.bind(this);
    this.SearchPolicy = this.SearchPolicy.bind(this);
    this.SearchPolicyCount = this.SearchPolicyCount.bind(this);
    this.GetById = this.GetById.bind(this);
  }

  GetGroupingPolicy(
    request: DeepPartial<GetGroupingPolicyReq>,
    metadata?: grpc.Metadata
  ): Observable<PolicyModel> {
    return this.rpc.invoke(
      PolicyGetGroupingPolicyDesc,
      GetGroupingPolicyReq.fromPartial(request),
      metadata
    );
  }

  PolicyGroupCount(
    request: DeepPartial<GetGroupingPolicyCountReq>,
    metadata?: grpc.Metadata
  ): Promise<Int64Value> {
    return this.rpc.unary(
      PolicyPolicyGroupCountDesc,
      GetGroupingPolicyCountReq.fromPartial(request),
      metadata
    );
  }

  GetPolicy(
    request: DeepPartial<GetPolicyReq>,
    metadata?: grpc.Metadata
  ): Observable<PolicyModel> {
    return this.rpc.invoke(
      PolicyGetPolicyDesc,
      GetPolicyReq.fromPartial(request),
      metadata
    );
  }

  PolicyCount(
    request: DeepPartial<GetPolicyCountReq>,
    metadata?: grpc.Metadata
  ): Promise<Int64Value> {
    return this.rpc.unary(
      PolicyPolicyCountDesc,
      GetPolicyCountReq.fromPartial(request),
      metadata
    );
  }

  AddPolicy(
    request: DeepPartial<PolicyModel>,
    metadata?: grpc.Metadata
  ): Promise<PolicyModel> {
    return this.rpc.unary(
      PolicyAddPolicyDesc,
      PolicyModel.fromPartial(request),
      metadata
    );
  }

  RemovePolicy(
    request: DeepPartial<Id64Request>,
    metadata?: grpc.Metadata
  ): Promise<Empty> {
    return this.rpc.unary(
      PolicyRemovePolicyDesc,
      Id64Request.fromPartial(request),
      metadata
    );
  }

  UpdatePolicy(
    request: DeepPartial<PolicyModel>,
    metadata?: grpc.Metadata
  ): Promise<Empty> {
    return this.rpc.unary(
      PolicyUpdatePolicyDesc,
      PolicyModel.fromPartial(request),
      metadata
    );
  }

  GetAllRoles(
    request: DeepPartial<Empty>,
    metadata?: grpc.Metadata
  ): Promise<ListRolesRes> {
    return this.rpc.unary(
      PolicyGetAllRolesDesc,
      Empty.fromPartial(request),
      metadata
    );
  }

  SearchPolicy(
    request: DeepPartial<SearchPolicyReq>,
    metadata?: grpc.Metadata
  ): Observable<PolicyModel> {
    return this.rpc.invoke(
      PolicySearchPolicyDesc,
      SearchPolicyReq.fromPartial(request),
      metadata
    );
  }

  SearchPolicyCount(
    request: DeepPartial<SearchPolicyCountReq>,
    metadata?: grpc.Metadata
  ): Promise<Int64Value> {
    return this.rpc.unary(
      PolicySearchPolicyCountDesc,
      SearchPolicyCountReq.fromPartial(request),
      metadata
    );
  }

  GetById(
    request: DeepPartial<Id64Request>,
    metadata?: grpc.Metadata
  ): Promise<PolicyModel> {
    return this.rpc.unary(
      PolicyGetByIdDesc,
      Id64Request.fromPartial(request),
      metadata
    );
  }
}

export const PolicyDesc = {
  serviceName: "authMs.Policy",
};

export const PolicyGetGroupingPolicyDesc: UnaryMethodDefinitionish = {
  methodName: "GetGroupingPolicy",
  service: PolicyDesc,
  requestStream: false,
  responseStream: true,
  requestType: {
    serializeBinary() {
      return GetGroupingPolicyReq.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...PolicyModel.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const PolicyPolicyGroupCountDesc: UnaryMethodDefinitionish = {
  methodName: "PolicyGroupCount",
  service: PolicyDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return GetGroupingPolicyCountReq.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...Int64Value.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const PolicyGetPolicyDesc: UnaryMethodDefinitionish = {
  methodName: "GetPolicy",
  service: PolicyDesc,
  requestStream: false,
  responseStream: true,
  requestType: {
    serializeBinary() {
      return GetPolicyReq.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...PolicyModel.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const PolicyPolicyCountDesc: UnaryMethodDefinitionish = {
  methodName: "PolicyCount",
  service: PolicyDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return GetPolicyCountReq.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...Int64Value.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const PolicyAddPolicyDesc: UnaryMethodDefinitionish = {
  methodName: "AddPolicy",
  service: PolicyDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return PolicyModel.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...PolicyModel.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const PolicyRemovePolicyDesc: UnaryMethodDefinitionish = {
  methodName: "RemovePolicy",
  service: PolicyDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return Id64Request.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...Empty.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const PolicyUpdatePolicyDesc: UnaryMethodDefinitionish = {
  methodName: "UpdatePolicy",
  service: PolicyDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return PolicyModel.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...Empty.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const PolicyGetAllRolesDesc: UnaryMethodDefinitionish = {
  methodName: "GetAllRoles",
  service: PolicyDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return Empty.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...ListRolesRes.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const PolicySearchPolicyDesc: UnaryMethodDefinitionish = {
  methodName: "SearchPolicy",
  service: PolicyDesc,
  requestStream: false,
  responseStream: true,
  requestType: {
    serializeBinary() {
      return SearchPolicyReq.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...PolicyModel.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const PolicySearchPolicyCountDesc: UnaryMethodDefinitionish = {
  methodName: "SearchPolicyCount",
  service: PolicyDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return SearchPolicyCountReq.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...Int64Value.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const PolicyGetByIdDesc: UnaryMethodDefinitionish = {
  methodName: "GetById",
  service: PolicyDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return Id64Request.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...PolicyModel.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

interface UnaryMethodDefinitionishR
  extends grpc.UnaryMethodDefinition<any, any> {
  requestStream: any;
  responseStream: any;
}

type UnaryMethodDefinitionish = UnaryMethodDefinitionishR;

interface Rpc {
  unary<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    request: any,
    metadata: grpc.Metadata | undefined
  ): Promise<any>;
  invoke<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    request: any,
    metadata: grpc.Metadata | undefined
  ): Observable<any>;
}

export class GrpcWebImpl {
  private host: string;
  private options: {
    transport?: grpc.TransportFactory;
    streamingTransport?: grpc.TransportFactory;
    debug?: boolean;
    metadata?: grpc.Metadata;
  };

  constructor(
    host: string,
    options: {
      transport?: grpc.TransportFactory;
      streamingTransport?: grpc.TransportFactory;
      debug?: boolean;
      metadata?: grpc.Metadata;
    }
  ) {
    this.host = host;
    this.options = options;
  }

  unary<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    _request: any,
    metadata: grpc.Metadata | undefined
  ): Promise<any> {
    const request = { ..._request, ...methodDesc.requestType };
    const maybeCombinedMetadata =
      metadata && this.options.metadata
        ? new BrowserHeaders({
            ...this.options?.metadata.headersMap,
            ...metadata?.headersMap,
          })
        : metadata || this.options.metadata;
    return new Promise((resolve, reject) => {
      grpc.unary(methodDesc, {
        request,
        host: this.host,
        metadata: maybeCombinedMetadata,
        transport: this.options.transport,
        debug: this.options.debug,
        onEnd: function (response) {
          if (response.status === grpc.Code.OK) {
            resolve(response.message);
          } else {
            const err = new Error(response.statusMessage) as any;
            err.code = response.status;
            err.metadata = response.trailers;
            reject(err);
          }
        },
      });
    });
  }

  invoke<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    _request: any,
    metadata: grpc.Metadata | undefined
  ): Observable<any> {
    // Status Response Codes (https://developers.google.com/maps-booking/reference/grpc-api/status_codes)
    const upStreamCodes = [2, 4, 8, 9, 10, 13, 14, 15];
    const DEFAULT_TIMEOUT_TIME: number = 3_000;
    const request = { ..._request, ...methodDesc.requestType };
    const maybeCombinedMetadata =
      metadata && this.options.metadata
        ? new BrowserHeaders({
            ...this.options?.metadata.headersMap,
            ...metadata?.headersMap,
          })
        : metadata || this.options.metadata;
    return new Observable((observer) => {
      const upStream = () => {
        const client = grpc.invoke(methodDesc, {
          host: this.host,
          request,
          transport: this.options.streamingTransport || this.options.transport,
          metadata: maybeCombinedMetadata,
          debug: this.options.debug,
          onMessage: (next) => observer.next(next),
          onEnd: (code: grpc.Code, message: string) => {
            if (code === 0) {
              observer.complete();
            } else if (upStreamCodes.includes(code)) {
              setTimeout(upStream, DEFAULT_TIMEOUT_TIME);
            } else {
              observer.error(new Error(`Error ${code} ${message}`));
            }
          },
        });
        observer.add(() => client.close());
      };
      upStream();
    }).pipe(share());
  }
}

declare var self: any | undefined;
declare var window: any | undefined;
declare var global: any | undefined;
var globalThis: any = (() => {
  if (typeof globalThis !== "undefined") return globalThis;
  if (typeof self !== "undefined") return self;
  if (typeof window !== "undefined") return window;
  if (typeof global !== "undefined") return global;
  throw "Unable to locate global object";
})();

type Builtin =
  | Date
  | Function
  | Uint8Array
  | string
  | number
  | boolean
  | undefined;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? { [K in Exclude<keyof T, "$type">]?: DeepPartial<T[K]> }
  : Partial<T>;

function longToNumber(long: Long): number {
  if (long.gt(Number.MAX_SAFE_INTEGER)) {
    throw new globalThis.Error("Value is larger than Number.MAX_SAFE_INTEGER");
  }
  return long.toNumber();
}

if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
