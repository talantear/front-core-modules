import axios, { AxiosInstance } from 'axios';
import { ApiClientOptions, StatusError } from './types';
import { inject, injectable } from 'inversify';
import { IApiConfig } from '@shared/base-client/types';
import BaseClient from '@shared/base-client/model';
import { HttpApiConfigDiKey } from './constants';
import { from, Observable } from 'rxjs';
import { BaseCommandProps } from '@shared/types/http';

@injectable()
export class HttpClient extends BaseClient {
  private MAX_REQUESTS_COUNT = 5;
  private INTERVAL_MS = 10;
  private PENDING_REQUESTS = 0;

  clientApi: AxiosInstance;

  constructor(
    @inject(HttpApiConfigDiKey) config: IApiConfig,
  ) {
    super(config);
    const host = config.getHost();

    if (!host) {
      throw new Error('Host is not undefined');
    }

    this.clientApi = axios.create({
      baseURL: host,
      validateStatus: () => true,
    });

    this.clientApi.interceptors.request.use((config) => {
      return new Promise((resolve, reject) => {
        let interval = setInterval(() => {
          if (this.PENDING_REQUESTS < this.MAX_REQUESTS_COUNT) {
            this.PENDING_REQUESTS++;
            clearInterval(interval);
            resolve(config);
          }
        }, this.INTERVAL_MS);
      });
    });


    this.clientApi.interceptors.response.use((response) => {
      this.PENDING_REQUESTS = Math.max(0, this.PENDING_REQUESTS - 1);
      return Promise.resolve(response);
    }, (error) => {
      this.PENDING_REQUESTS = Math.max(0, this.PENDING_REQUESTS - 1);
      return Promise.reject(error);
    });
  }

  commonApi = (api: AxiosInstance) => <P extends object | null, T extends object>(url: string, params?: P, options?: ApiClientOptions) => {
    const method = options?.method || 'post';
    const headers = { 'Content-Type': 'application/json', ...(options?.headers || {}) };
    const data = (method === 'get') ? { params } : { data: params };
    const extra = options?.baseURL ? { baseURL: options!.baseURL } : {};

    return api.request<T>({
      method,
      url,
      headers,
      ...data,
      ...extra,
      transformResponse: options?.transformResponse,
    }).then(res => {
      return {
        statusCode: res.status,
        ...res.data,
      };
    });
  };

  request = <P extends object | null, T extends BaseCommandProps>(url: string, params?: P, options?: ApiClientOptions) => {
    const headers = {
      ...(options?.headers || {}),
    };

    const clientOptions = { ...(options || {}), headers };
    this.middlewares.forEach((m) => {
      if (m.ExtraMetadata) {
        clientOptions.headers = { ...clientOptions.headers, ...m.ExtraMetadata(clientOptions)}
      }
    });

    return this.commonApi(this.clientApi)<P, T>(url, params, clientOptions)
      .then((res: T) => {
        this.middlewares.forEach((m) => m.OnRequestComplete && m.OnRequestComplete(res, options));

        if (options?.isFile) {
          return res;
        }

        if (res.statusCode && res.statusCode !== 200 && res.statusCode !== 201) {
          const { message } = res;

          // if (process.env.NODE_ENV === 'production') {
          const e = new StatusError(message ? `${message}` : `No success`, res.statusCode);
          this.middlewares.forEach((m) => m.OnRequestError && m.OnRequestError(e as Error, options));
          throw e;
          // } else {
          //   throw new StatusError(message ? `${message} url: ${url}` : `No success, url: ${url}`, res.statusCode);
          // }
        }

        return res;
      });
  }

  $request = <P extends object | null, T extends BaseCommandProps>(url: string, params?: P, options?: ApiClientOptions): Observable<T> => {
    return from(this.request<P, T>(url, params, options));
  }
}

export default HttpClient;