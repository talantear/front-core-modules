import { AsyncContainerModule, interfaces } from 'inversify';
import InfoSectionUsecase from '../section/usecase';
import InfoSectionItemUsecase from '../item/usecase';
import { IInfoSectionItemService } from '../item';
import { IInfoSectionService } from '../section';
import { InfoSectionItemServiceKey, InfoSectionServiceKey } from '../constants';

export const InfoModule = <ITEM extends interfaces.Newable<IInfoSectionItemService>, SECTION extends interfaces.Newable<IInfoSectionService>>(item: ITEM, section: SECTION) => new AsyncContainerModule(async (bind: interfaces.Bind, unbind: interfaces.Unbind) => {
  bind<IInfoSectionService>(InfoSectionServiceKey).to(section);
  bind<IInfoSectionItemService>(InfoSectionItemServiceKey).to(item);

  bind<InfoSectionUsecase>(InfoSectionUsecase.diKey).to(InfoSectionUsecase).inSingletonScope();
  bind<InfoSectionItemUsecase>(InfoSectionItemUsecase.diKey).to(InfoSectionItemUsecase).inSingletonScope();
});
