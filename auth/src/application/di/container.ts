import { Container, interfaces } from 'inversify';
import { ApplicationModule } from './module';
import { IApplicationService } from '../service/types';

const applicationContainer = <T extends interfaces.Newable<IApplicationService>>(service: T): interfaces.Container => {
  const container = new Container();
  container.load(ApplicationModule(service));
  return container;
}

export default applicationContainer;
