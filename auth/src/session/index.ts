import { sessionServiceDiKey } from './constants';
import SessionCookieStorage, { ISessionCookieManager, ISessionCookieReader, ISessionCookieStorage } from './cookie';
import { deadlineExceeded, permissionDeniedCode, unauthorizedCode, unknownErrorCode } from './errors';
import useSession from './hook';
import useSessionWithRoleCheck from './hook-with-role-check';
import SessionMiddleware from './middleware';
import SessionViewModel from './model';
import SessionUsecase, { ISessionUsecase } from './usecase';
import { IAccessCheckResult, IDispose, IProfileRes, ISessionService } from './service/types';

export {
  sessionServiceDiKey,
  SessionCookieStorage,
  unauthorizedCode,
  permissionDeniedCode,
  unknownErrorCode,
  deadlineExceeded,
  useSession,
  useSessionWithRoleCheck,
  SessionMiddleware,
  SessionViewModel,
  SessionUsecase,
};

export type {
  IProfileRes,
  IAccessCheckResult,
  IDispose,
  ISessionService,
  ISessionCookieStorage,
  ISessionCookieReader,
  ISessionCookieManager,
  ISessionUsecase,
};