import { Container, interfaces } from 'inversify';
import { RecoveryModule } from './module';
import { IRecoveryService } from '../service/types';

const recoveryContainer = <T extends interfaces.Newable<IRecoveryService>>(service: T): interfaces.Container => {
  const container = new Container();
  container.load(RecoveryModule(service));
  return container;
}

export default recoveryContainer;
