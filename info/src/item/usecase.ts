import { inject, injectable } from 'inversify';
import { IBaseListUsecase, IPageService } from '@shared/stream/types';
import { PageService } from '@shared/stream/service';
import { ListMV } from '@shared/stream/model';
import { Observable, Subscription } from 'rxjs';
import InfoItem from './model';
import { InfoSectionItemServiceKey } from '../constants';
import { IListItems, ISectionItemModel } from './dto';
import { IInfoSectionItemService } from './service/types';
import { ListOptions } from '@shared/stream/types';

export interface IInfoItemUsecase {
  create(input: ISectionItemModel): Observable<InfoItem>
  update(input: ISectionItemModel): Observable<InfoItem>
  delete(id: number): Observable<void>
  getById(id: number): Observable<InfoItem>
  loadDefaultItems(query: IListItems): Observable<InfoItem>
}

@injectable()
class InfoSectionItemUsecase implements IInfoItemUsecase, IBaseListUsecase<InfoItem> {
  public static diKey = Symbol.for('InfoItemUsecaseDiKey');
  private infoSectionItemService: IInfoSectionItemService;
  private pageService: IPageService<InfoItem>;

  constructor(
    @inject(InfoSectionItemServiceKey) infoSectionItemService: IInfoSectionItemService,
  ) {
    this.infoSectionItemService = infoSectionItemService;
    this.pageService = new PageService(this.infoSectionItemService);
  }

  loadItems(model: ListMV<InfoItem>, opts: ListOptions, method?: string, query?: object): [Observable<InfoItem>, Subscription] {
    return this.pageService.loadPage(model, opts, method, query);
  }

  loadTotalCount(model: ListMV<InfoItem>, method?: string, query?: object): [Observable<number>, Subscription] {
    return this.pageService.loadTotalCount(model, method, query);
  }

  create(input: ISectionItemModel): Observable<InfoItem> {
    return this.infoSectionItemService.create(input)
  }

  update(input: ISectionItemModel): Observable<InfoItem> {
    return this.infoSectionItemService.update(input)
  }

  delete(id: number): Observable<void> {
    return this.infoSectionItemService.delete(id);
  }

  getById(id: number): Observable<InfoItem> {
    return this.infoSectionItemService.getById(id)
  }

  loadDefaultItems(query: IListItems): Observable<InfoItem> {
    return this.infoSectionItemService.loadItems(query);
  }
}

export default InfoSectionItemUsecase;