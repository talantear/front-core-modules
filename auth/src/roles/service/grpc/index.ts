import { inject, injectable } from 'inversify';
import { ApiClient } from '@shared/grpc-client/model';
import { Observable } from 'rxjs';
import { GetGroupingPolicyReq, PolicyGetGroupingPolicyDesc } from '../../../proto/policy';
import { AppConfigDiKey } from '@shared/app/constants';
import { IAppConfig } from '@shared/app/types';
import { IRoleService } from '../types';
import { IPolicyModel } from '../../dto';

@injectable()
class RoleService implements IRoleService {
  private api: ApiClient;
  private appConfig: IAppConfig;

  constructor(
    @inject(ApiClient.diKey) api: ApiClient,
    @inject(AppConfigDiKey) appConfig: IAppConfig,
  ) {
    this.api = api;
    this.appConfig = appConfig;
  }

  initialize(): Observable<IPolicyModel> {
    return this.api.stream$(
      PolicyGetGroupingPolicyDesc,
      {
        obj: GetGroupingPolicyReq.fromPartial({ opts: { offset: 0, limit: 1000, order: 'id asc' } }),
        coder: GetGroupingPolicyReq,
      },
      { host: this.appConfig.authUrlServer }
    )
  }
}

export default RoleService;