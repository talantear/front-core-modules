import typeScript from 'rollup-plugin-typescript2';

export default [
  {
    external: ['tslib'],
    input: 'index.ts',
    output: [
      { format: 'esm', dir: 'dist', exports: 'auto', sourcemap: false, validate: true, preserveModules: true },
    ],
    plugins: [
      typeScript({
        clean: true,
        check: true,
      }),
    ]
  },
];
