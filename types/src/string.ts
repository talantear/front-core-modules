declare global {
  interface String {
    hashCode(): number;
  }
}

String.prototype.hashCode = function() {
  let hash = 0, i, chr;
  if (this.length === 0) return hash;
  for (i = 0; i < this.length; i++) {
    chr   = this.charCodeAt(i);
    hash  = ((hash << 5) - hash) + chr;
    hash |= 0;
  }
  return hash;
};

export interface ObjectWithId {
  id: number,
}

export type StringWithId = string & ObjectWithId;

export const CreateStringWithId = (str: String): StringWithId => {
  const val = {} as StringWithId;
  val.id = str.hashCode();
  return (val as StringWithId)
}