import typeScript from 'rollup-plugin-typescript2';
import { visualizer } from 'rollup-plugin-visualizer';
import { uglify } from 'rollup-plugin-uglify';
import packageJson from './package.json' assert { type: "json" };
import graphql from '@rollup/plugin-graphql';
import postcss from 'rollup-plugin-postcss';

export default [
  {
    external: ['tslib', ...(Object.keys(packageJson.peerDependencies || {}) || [])],
    input: 'index.tsx',
    output: [
      { format: 'esm', dir: 'dist', exports: 'auto', sourcemap: false, validate: true, preserveModules: true },
    ],
    plugins: [
      graphql(),
      typeScript({
        clean: true,
        check: true,
      }),
      postcss({
        extract: 'styles.css',
        autoModules: true,
      }),
      visualizer({ gzipSize: true, open: false }),
      uglify(),
    ]
  },
];
