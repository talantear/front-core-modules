import { inject, injectable } from 'inversify';
import SessionCookieStorage from '../session/cookie';
import { ISessionService } from '../session/service/types';
import { authServiceDiKey } from './constants';
import { ILoginResp, IRegAndLoginReq, IRegisterResp } from './dto';
import { IAuthService } from './service/types';
import { sessionServiceDiKey } from '../session';

export interface IAuthUsecase {
  login: (req: IRegAndLoginReq) => Promise<ILoginResp>,
  register: (req: IRegAndLoginReq) => Promise<IRegisterResp>,
}

@injectable()
class AuthUsecase implements IAuthUsecase {
  public static diKey = Symbol.for('AuthUsecaseDiKey');

  private sessionService: ISessionService;
  private sessionCookieStorage: SessionCookieStorage;
  private authService: IAuthService;

  constructor(
    @inject(authServiceDiKey) authService: IAuthService,
    @inject(sessionServiceDiKey) sessionService: ISessionService,
    @inject(SessionCookieStorage.diKey) sessionCookieStorage: SessionCookieStorage,
  ) {
    this.sessionService = sessionService;
    this.authService = authService;
    this.sessionCookieStorage = sessionCookieStorage;
  }

  login(req: IRegAndLoginReq): Promise<ILoginResp> {
    return this.authService.login(req).then((res) => {
      this.sessionCookieStorage.setAccessToken(res.accessToken, req.applicationSlug);
      this.sessionCookieStorage.setRefreshToken(res.refreshToken, req.applicationSlug);

      return res;
    });
  }

  register(req: IRegAndLoginReq): Promise<IRegisterResp> {
    return this.authService.register(req);
  }
}

export default AuthUsecase;