import { Observable } from 'rxjs';
import { IPolicyModel } from '../dto';

export interface IRoleService {
  initialize: () => Observable<IPolicyModel>,
}