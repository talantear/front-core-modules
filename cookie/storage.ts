import { inject, injectable } from 'inversify';

export const CookieHandlerDiKey = Symbol.for('CookieHandlerDiKey');

export interface ICookieStorage {
	set: (key: string, value: string, duration?: number) => void,
	get: (key: string) => string,
	delete: (key: string) => void,
}

@injectable()
class CookieStorage implements ICookieStorage {
	public static diKey = Symbol.for('BaseCookieStorageDiKey');
	private handler: ICookieStorage;

	public constructor(
		@inject(CookieHandlerDiKey) handler: ICookieStorage,
	) {
		this.handler = handler;
	}

	delete(key: string): void {
		this.handler.delete(key);
	}

	get(key: string): string {
		return this.handler.get(key);
	}

	set(key: string, value: string, duration: number | undefined): void {
		this.handler.set(key, value, duration);
	}
}

export default CookieStorage;