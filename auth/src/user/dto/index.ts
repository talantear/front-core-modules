export interface IUserModel {
  id: number;
  createdAt: number;
  alias?: string | undefined;
  parentId?: number | undefined;
  email: string;
  verified: boolean;
  blocked: boolean;
  deleted: boolean;
  firstName?: string | undefined;
  middleName?: string | undefined;
  surname?: string | undefined;
  phone?: string | undefined;
  lang: string;
  path: string;
  role: string;
  organizationId: number;
}

export interface IShortUserModel {
  id: number;
  email: string;
  blocked: boolean;
  deleted: boolean;
  phone?: string | undefined;
  firstName?: string | undefined;
  middleName?: string | undefined;
  surname?: string | undefined;
  parentId?: number | undefined;
  alias?: string | undefined;
  lang: string;
}

export interface IUserFilterModel {
  org?: string | undefined;
  role?: string | undefined;
  query?: string | undefined;
}