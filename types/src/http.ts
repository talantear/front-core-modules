export interface ListResp<T> {
  list: T[],
}

export interface ItemResp<T> {
  item: T,
}

export interface ListPaginatorResp<T> {
  list: T[],
  paginator: {
    offset: number;
    totalCount: number;
    countPerPage: number;
  },
}

export interface ResultIdRes extends BaseCommandProps {
  id: number,
}

export interface ResultCountRes extends BaseCommandProps {
  count: number,
}

export interface ResultListRes<P> extends BaseCommandProps {
  list: P[],
}

export interface ResultItemRes<P> extends BaseCommandProps {
  item: P
}

export interface BaseCommandProps {
  success: string
  statusCode?: number
  message?: string
}