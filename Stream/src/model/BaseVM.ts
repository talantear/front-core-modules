import { action, makeAutoObservable, observable } from 'mobx';
import { Subscription } from 'rxjs';
import { ObjectObserverImp, ObservableField } from './decorator';

export interface IBaseViewModel<LoadParamsType> {
  addSubscription(subscription: Subscription): void
  onLoadComplete(): void
  load(params?: LoadParamsType): void
  unload(): void
}

export class BaseViewModel<LoadParamsType = void> extends ObjectObserverImp implements IBaseViewModel<LoadParamsType>{
  private subscriptions: Subscription[] = [];
  private loading: ObservableVM<boolean>  = new ObservableVM<boolean>(true);

  addSubscription = (subscription: Subscription) => {
    this.subscriptions.push(subscription);
  }

  load = (params?: LoadParamsType) => {}

  unload = () => {
    for (const sub of this.subscriptions) {
      sub.unsubscribe();
    }
  }

  onLoadComplete(): void {
    this.loading.setItem(false);
  }
}

// TODO: Deprecated or not
class ObservableVM<T> {
  constructor(model: T | undefined) {
    makeAutoObservable(this)
    this.model = model;
  }

  @observable
  protected model: T | undefined;

  @action
  public setItem = (value: T | undefined) => {
    this.model = value;
  }

  @action
  public getItem = () => {
    return this.model;
  }

  get exist() {
    return !!this.model;
  }
}

export default ObservableVM