import { FC, useEffect, useState } from 'react';

interface ICallbackRes {
  accessToken: string,
  refreshToken: string,
}

interface IComponent {
  parentElementId: string,
  clientId: string,
  redirectUri: string,
  autoLoad?: boolean,
  callback?: (res: ICallbackRes) => void,
  render?: (onClick: () => string) => JSX.Element,
  promo?: string,
  state?: string,
  code?: string,
  recoveryToken?: string,
  activateToken?: string,
  host: string,
  lang?: string,
}

const isBrowser = typeof window !== 'undefined';

const IAuthComponent: FC<IComponent> = (props) => {
  const {
    clientId,
    autoLoad,
    callback,
    render, // WTF?
    redirectUri,
    parentElementId,
    promo,
    recoveryToken,
    activateToken,
    host,
    state,
    code,
    lang,
  } = props;

  const [enabled, setEnabled] = useState<boolean>(!!render || !!autoLoad)
  const [authUrl] = useState(() => {
    let page = 'login/oauth/authorize';
    let token;

    if (recoveryToken) {
      page = 'recovery';
      token = recoveryToken;
    } else if (activateToken) {
      page = 'activate';
      token = activateToken;
    } else if (promo) {
      page = 'login/oauth/registration';
    }

    return `${host}/${page}?client_id=${clientId}&redirect_uri=${redirectUri}${promo ? `&promo=${promo}` : ''}${token ? `&token=${token}` : ''}${state ? `&state=${state}` : ''}${code ? `&code=${code}` : ''}&type=${render ? 'redirect' : 'message'}${lang ? `&lang=${lang}` : ''}`;
  });

  useEffect(() => {
    if (!authUrl.length || !enabled) {
      return;
    }

    if (!autoLoad || !isBrowser) {
      return;
    }

    const iframe = document.createElement('iframe');
    iframe.id = 'oauth-iframe-id';
    iframe.style.setProperty('width', '100%');
    iframe.style.setProperty('height', '100%');
    iframe.style.setProperty('background-color', '#fff');
    iframe.src = authUrl;

    const handlerMessage = (e: MessageEvent) => {
      const { accessToken, refreshToken, url } = e.data;
      if (url) {
        window.location.href = url;
        return;
      }
      const origin = e.origin;
      if (origin !== host) {
        console.error('Origin URL doesnt equal to IFrame URL.');
        return;
      }
      iframe.remove();

      if (callback) {
        callback({ accessToken, refreshToken });
      }
    };

    const parent = document.getElementById(parentElementId);
    const loadingEle = document.getElementById('loading');

    const onLoad = () => {
      if (loadingEle) {
        loadingEle.style.display = 'none';

        iframe.style.opacity = '1';
      }
    }

    if (parent) {
      iframe.addEventListener('load', onLoad);
      parent.appendChild(iframe);

      if (window.addEventListener) {
        window.addEventListener('message', handlerMessage);
      }
    }

    return () => {
      iframe.remove();
      window.removeEventListener('message', handlerMessage)
      window.removeEventListener('load', onLoad)
    }
  }, [authUrl, enabled, autoLoad, isBrowser]);


  if (!isBrowser) {
    return null;
  }

  if (!enabled) {
    return (
      <button onClick={() => setEnabled(true)}>
        Login
      </button>
    )
  }

  if (render) {
    return render(() => authUrl);
  }

  return null;
};

export default IAuthComponent;
