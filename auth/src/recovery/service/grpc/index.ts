import { StringRequest } from './proto/commonTypes';
import { inject, injectable } from 'inversify';
import { ApiClient } from '@shared/grpc-client/model';
import {
  ChangePassReq,
  RecoveryPasswordChangePassDesc,
  RecoveryPasswordCheckTokenDesc,
  RecoveryPasswordRecoveryDesc,
  RecoveryReq,
} from './proto/recovery';
import { Observable } from 'rxjs';
import { IRecoveryService } from '../types';
import { IStringRequest } from '@shared/types/dto';
import { IChangePassReq, IRecoveryReq } from '../../dto';

@injectable()
class RecoveryService implements IRecoveryService {
  private api: ApiClient;

  public constructor(
    @inject(ApiClient.diKey) api: ApiClient
  ) {
    this.api = api;
  }

  checkToken(request: IStringRequest): Observable<void> {
    return this.api.unary$(
      RecoveryPasswordCheckTokenDesc,
      {
        obj: StringRequest.fromPartial(request),
        coder: StringRequest,
      }
    )
  }

  recovery(request: IRecoveryReq): Observable<void> {
    return this.api.unary$(
      RecoveryPasswordRecoveryDesc,
      {
        obj: RecoveryReq.fromPartial(request),
        coder: RecoveryReq,
      }
    )
  }

  changePass(request: IChangePassReq): Observable<void> {
    return this.api.unary$(
      RecoveryPasswordChangePassDesc,
      {
        obj: ChangePassReq.fromPartial(request),
        coder: ChangePassReq,
      }
    )
  }
}

export default RecoveryService;