import { inject, injectable } from 'inversify';
import { ApiClient } from '@shared/grpc-client/model';
import {
  ApplicationCountDesc,
  ApplicationCreateDesc, ApplicationGetByClientIdDesc,
  ApplicationGetByIdDesc, ApplicationListDesc,
  ApplicationModel, ApplicationRemoveDesc, ApplicationUpdateDesc,
} from './proto/application';
import { AppConfigDiKey } from '@shared/app/constants';
import { IAppConfig } from '@shared/app/types';
import { map, Observable } from 'rxjs';
import { IId64Request, IStringRequest, IListOptions } from '@shared/types/dto';
import { Empty } from './proto/google/protobuf/empty';
import { StreamHandler } from '@shared/stream/service';
import { IApplicationService } from '../types';
import { IApplication } from '../../dto';
import { Id64Request, StringRequest } from './proto/commonTypes';
import { ListOptions } from './proto/paginatior';
import { IInt64Value } from '@shared/types/dist/src/dto';

@injectable()
class ApplicationService extends StreamHandler<IApplication> implements IApplicationService {
  private api: ApiClient;
  private appConfig: IAppConfig;
  private cache: Record<string, IApplication> = {};

  public constructor(
    @inject(ApiClient.diKey) api: ApiClient,
    @inject(AppConfigDiKey) appConfig: IAppConfig,
  ) {
    super();
    this.add('default', this.streamDefault)
    this.api = api;
    this.appConfig = appConfig;
  }

  private streamDefault = {
    stream: (opts: IListOptions): Observable<IApplication> => {
      return this.list(opts);
    },
    loadCount: (): Observable<number> => {
      return this.count();
    },
  }

  count(): Observable<number> {
    return this.api.unary$(
      ApplicationCountDesc,
      {
        obj: Empty.fromPartial({}),
        coder: Empty,
      },
      { host: this.appConfig.authUrlServer }
    ).pipe(map<IInt64Value, number>(res => res.value));
  }

  create(request: IApplication): Observable<IApplication> {
    return this.api.unary$(
      ApplicationCreateDesc,
      {
        obj: ApplicationModel.fromPartial(request),
        coder: ApplicationModel,
      },
      { host: this.appConfig.authUrlServer }
    );
  }

  getById(request: IId64Request): Observable<IApplication> {
    return this.api.unary$(
      ApplicationGetByIdDesc,
      {
        obj: Id64Request.fromPartial(request),
        coder: Id64Request,
      },
      { host: this.appConfig.authUrlServer }
    );
  }

  list(request: IListOptions): Observable<IApplication> {
    return this.api.stream$(
      ApplicationListDesc,
      { obj: ListOptions.fromPartial(request), coder: ListOptions },
      { host: this.appConfig.authUrlServer, noCache: true },
    ).pipe();
  }

  update(request: IApplication): Observable<IApplication> {
    return this.api.unary$(
      ApplicationUpdateDesc,
      {
        obj: ApplicationModel.fromPartial(request),
        coder: ApplicationModel,
      },
      { host: this.appConfig.authUrlServer }
    );
  }

  getByClientId(clientId: IStringRequest): Observable<IApplication> {
    return this.api.unary$(
      ApplicationGetByClientIdDesc,
      {
        obj: StringRequest.fromPartial(clientId),
        coder: StringRequest,
      },
      { host: this.appConfig.authUrlServer }
    );
  }

  addCache(key: string, value: IApplication): void {
    this.cache[key] = value;
  }

  getCache(key: string): IApplication {
    return this.cache[key]
  }

  remove(request: IId64Request): Observable<void> {
    return this.api.unary$(
      ApplicationRemoveDesc,
      {
        obj: Id64Request.fromPartial(request),
        coder: Id64Request,
      },
      { host: this.appConfig.authUrlServer }
    );
  }
}

export default ApplicationService;
