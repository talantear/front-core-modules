import { inject, injectable } from 'inversify';
import HttpClient from '@shared/http-client/model';
import { ApiHttpClientKey } from '@shared/http-client/constants';
import { BaseCommandProps } from '@shared/types/http';
import { IFilesUploadRes } from '../../dto';
import { IFilesService } from '../types';

@injectable()
class FilesService implements IFilesService {
  private api: HttpClient;

  public constructor(
    @inject(ApiHttpClientKey) api: HttpClient,
  ) {
    this.api = api;
  }

  getFileInfo(key: string): Promise<IFilesUploadRes> {
    return this.api.request<object, IFilesUploadRes & BaseCommandProps>(
      `/api/v1/file/info/${key}`, undefined, { method: 'get' },
    );
  }

  upload(file: File): Promise<IFilesUploadRes> {
    const formData = new FormData();

    formData.append('file', file);

    return this.api.request<object, IFilesUploadRes & BaseCommandProps>(
      `/api/v1/files/upload`,
      formData,
      {
        method: 'post',
        headers: {
          'Content-Type': 'multipart/form-data',
        },
        isFile: true,
      },
    );
  }

  get(key: string): Promise<IFilesUploadRes> {
    return this.api.request<object, IFilesUploadRes & BaseCommandProps>(
      `/api/v1/file/${key}`, undefined, { method: 'get' },
    );
  }
}

export default FilesService;
