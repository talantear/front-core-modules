import typeScript from 'rollup-plugin-typescript2';
import { visualizer } from 'rollup-plugin-visualizer';
import { uglify } from 'rollup-plugin-uglify';
import packageJson from './package.json' assert { type: "json" };

export default {
  external: ['tslib', ...(Object.keys(packageJson.peerDependencies) || [])],
  input: 'index.ts',
  output: [
    { format: 'esm', dir: 'dist', exports: 'auto', sourcemap: false, validate: true, preserveModules: true },
  ],
  plugins: [
    typeScript({
      clean: true,
      check: true,
    }),
    visualizer({ gzipSize: true, open: false }),
    uglify(),
  ]
};
