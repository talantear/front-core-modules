import { inject, injectable } from 'inversify';
import GqlClient from '@shared/gql-client/client';
import { map, Observable } from 'rxjs';
import { ApiGqlClientKey } from '@shared/gql-client/constants';
import {
  CurrencyGetRateDocument,
  CurrencyGetRateQuery,
  CurrencyGetRateQueryVariables, CurrencyRateSubscriberDocument,
  CurrencyRateSubscriberSubscription,
  CurrencyRateSubscriberSubscriptionVariables,
} from '../../gql/schemas/currency-gql';
import { ICurrencyService } from '../types';

@injectable()
class CurrencyService implements ICurrencyService {
  public static diKey = Symbol.for('CurrencyGqlServiceKey');
  private api: GqlClient;

  public constructor(
    @inject(ApiGqlClientKey) api: GqlClient,
  ) {
    this.api = api;
  }


  getRate(currency: string): Observable<number> {
    return this.api.query$<CurrencyGetRateQuery, CurrencyGetRateQueryVariables>(
      CurrencyGetRateDocument,
      { input: currency.toUpperCase() },
      { noCache: true },
    ).pipe(map(res => res.currencyGetRate));
  }

  subscriber(currency: string): Observable<number> {
    return this.api.subscribe<CurrencyRateSubscriberSubscription, CurrencyRateSubscriberSubscriptionVariables>(
      CurrencyRateSubscriberDocument,
      { input: currency.toUpperCase() },
      { noCache: true },
    ).pipe(map(res => res.currencyRateSubscriber));
  }
}

export default CurrencyService;
