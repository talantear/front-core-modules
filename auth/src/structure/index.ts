import { structServiceDiKey } from './constants';
import StructUsecase, { IStructUsecase } from './usecase';
import { IStructService } from './service/types';

export { structServiceDiKey, StructUsecase };
export type { IStructService, IStructUsecase };