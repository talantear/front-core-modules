/* eslint-disable */
import { messageTypeRegistry } from "./typeRegistry";
import Long from "long";
import { grpc } from "@improbable-eng/grpc-web";
import _m0 from "protobufjs/minimal";
import { StringRequest, BoolValue } from "./commonTypes";
import { Empty } from "./google/protobuf/empty";
import { ShortUserModel } from "./authCommon";
import { BrowserHeaders } from "browser-headers";

export const protobufPackage = "authMs";

/** Represents clearing request model. */
export interface ClearReq {
  $type: "authMs.ClearReq";
  token: string;
}

/** Represents checking request model. */
export interface CheckReq {
  $type: "authMs.CheckReq";
  /** / Request method */
  method: string;
  refreshToken?: string | undefined;
  appSlug: string;
  url: string;
}

/** Represents checking response model. */
export interface CheckResp {
  $type: "authMs.CheckResp";
  /** / Is response public */
  isPublic: boolean;
  accessToken?: string | undefined;
}

/** Represents method request. */
export interface MethodReq {
  $type: "authMs.MethodReq";
  /** / Request method */
  method: string;
  appSlug: string;
}

const baseClearReq: object = { $type: "authMs.ClearReq", token: "" };

export const ClearReq = {
  $type: "authMs.ClearReq" as const,

  encode(
    message: ClearReq,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.token !== "") {
      writer.uint32(10).string(message.token);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ClearReq {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseClearReq } as ClearReq;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.token = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ClearReq {
    const message = { ...baseClearReq } as ClearReq;
    message.token =
      object.token !== undefined && object.token !== null
        ? String(object.token)
        : "";
    return message;
  },

  toJSON(message: ClearReq): unknown {
    const obj: any = {};
    message.token !== undefined && (obj.token = message.token);
    return obj;
  },

  fromPartial(object: DeepPartial<ClearReq>): ClearReq {
    const message = { ...baseClearReq } as ClearReq;
    message.token = object.token ?? "";
    return message;
  },
};

messageTypeRegistry.set(ClearReq.$type, ClearReq);

const baseCheckReq: object = {
  $type: "authMs.CheckReq",
  method: "",
  appSlug: "",
  url: "",
};

export const CheckReq = {
  $type: "authMs.CheckReq" as const,

  encode(
    message: CheckReq,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.method !== "") {
      writer.uint32(10).string(message.method);
    }
    if (message.refreshToken !== undefined) {
      writer.uint32(18).string(message.refreshToken);
    }
    if (message.appSlug !== "") {
      writer.uint32(26).string(message.appSlug);
    }
    if (message.url !== "") {
      writer.uint32(34).string(message.url);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): CheckReq {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseCheckReq } as CheckReq;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.method = reader.string();
          break;
        case 2:
          message.refreshToken = reader.string();
          break;
        case 3:
          message.appSlug = reader.string();
          break;
        case 4:
          message.url = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): CheckReq {
    const message = { ...baseCheckReq } as CheckReq;
    message.method =
      object.method !== undefined && object.method !== null
        ? String(object.method)
        : "";
    message.refreshToken =
      object.refreshToken !== undefined && object.refreshToken !== null
        ? String(object.refreshToken)
        : undefined;
    message.appSlug =
      object.appSlug !== undefined && object.appSlug !== null
        ? String(object.appSlug)
        : "";
    message.url =
      object.url !== undefined && object.url !== null ? String(object.url) : "";
    return message;
  },

  toJSON(message: CheckReq): unknown {
    const obj: any = {};
    message.method !== undefined && (obj.method = message.method);
    message.refreshToken !== undefined &&
      (obj.refreshToken = message.refreshToken);
    message.appSlug !== undefined && (obj.appSlug = message.appSlug);
    message.url !== undefined && (obj.url = message.url);
    return obj;
  },

  fromPartial(object: DeepPartial<CheckReq>): CheckReq {
    const message = { ...baseCheckReq } as CheckReq;
    message.method = object.method ?? "";
    message.refreshToken = object.refreshToken ?? undefined;
    message.appSlug = object.appSlug ?? "";
    message.url = object.url ?? "";
    return message;
  },
};

messageTypeRegistry.set(CheckReq.$type, CheckReq);

const baseCheckResp: object = { $type: "authMs.CheckResp", isPublic: false };

export const CheckResp = {
  $type: "authMs.CheckResp" as const,

  encode(
    message: CheckResp,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.isPublic === true) {
      writer.uint32(8).bool(message.isPublic);
    }
    if (message.accessToken !== undefined) {
      writer.uint32(18).string(message.accessToken);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): CheckResp {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseCheckResp } as CheckResp;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.isPublic = reader.bool();
          break;
        case 2:
          message.accessToken = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): CheckResp {
    const message = { ...baseCheckResp } as CheckResp;
    message.isPublic =
      object.isPublic !== undefined && object.isPublic !== null
        ? Boolean(object.isPublic)
        : false;
    message.accessToken =
      object.accessToken !== undefined && object.accessToken !== null
        ? String(object.accessToken)
        : undefined;
    return message;
  },

  toJSON(message: CheckResp): unknown {
    const obj: any = {};
    message.isPublic !== undefined && (obj.isPublic = message.isPublic);
    message.accessToken !== undefined &&
      (obj.accessToken = message.accessToken);
    return obj;
  },

  fromPartial(object: DeepPartial<CheckResp>): CheckResp {
    const message = { ...baseCheckResp } as CheckResp;
    message.isPublic = object.isPublic ?? false;
    message.accessToken = object.accessToken ?? undefined;
    return message;
  },
};

messageTypeRegistry.set(CheckResp.$type, CheckResp);

const baseMethodReq: object = {
  $type: "authMs.MethodReq",
  method: "",
  appSlug: "",
};

export const MethodReq = {
  $type: "authMs.MethodReq" as const,

  encode(
    message: MethodReq,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.method !== "") {
      writer.uint32(10).string(message.method);
    }
    if (message.appSlug !== "") {
      writer.uint32(18).string(message.appSlug);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MethodReq {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMethodReq } as MethodReq;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.method = reader.string();
          break;
        case 2:
          message.appSlug = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MethodReq {
    const message = { ...baseMethodReq } as MethodReq;
    message.method =
      object.method !== undefined && object.method !== null
        ? String(object.method)
        : "";
    message.appSlug =
      object.appSlug !== undefined && object.appSlug !== null
        ? String(object.appSlug)
        : "";
    return message;
  },

  toJSON(message: MethodReq): unknown {
    const obj: any = {};
    message.method !== undefined && (obj.method = message.method);
    message.appSlug !== undefined && (obj.appSlug = message.appSlug);
    return obj;
  },

  fromPartial(object: DeepPartial<MethodReq>): MethodReq {
    const message = { ...baseMethodReq } as MethodReq;
    message.method = object.method ?? "";
    message.appSlug = object.appSlug ?? "";
    return message;
  },
};

messageTypeRegistry.set(MethodReq.$type, MethodReq);

/** Represents session methods. */
export interface Session {
  /** / Represents cleaning session */
  Clear(
    request: DeepPartial<ClearReq>,
    metadata?: grpc.Metadata
  ): Promise<Empty>;
  /** / Represents checking session */
  Check(
    request: DeepPartial<CheckReq>,
    metadata?: grpc.Metadata
  ): Promise<CheckResp>;
  /** / Represents checking public session */
  CheckPublic(
    request: DeepPartial<MethodReq>,
    metadata?: grpc.Metadata
  ): Promise<CheckResp>;
  /** / Represents getting is public session */
  IsPublic(
    request: DeepPartial<MethodReq>,
    metadata?: grpc.Metadata
  ): Promise<BoolValue>;
  /** / Represents getting user by token */
  GetUserByToken(
    request: DeepPartial<StringRequest>,
    metadata?: grpc.Metadata
  ): Promise<ShortUserModel>;
}

export class SessionClientImpl implements Session {
  private readonly rpc: Rpc;

  constructor(rpc: Rpc) {
    this.rpc = rpc;
    this.Clear = this.Clear.bind(this);
    this.Check = this.Check.bind(this);
    this.CheckPublic = this.CheckPublic.bind(this);
    this.IsPublic = this.IsPublic.bind(this);
    this.GetUserByToken = this.GetUserByToken.bind(this);
  }

  Clear(
    request: DeepPartial<ClearReq>,
    metadata?: grpc.Metadata
  ): Promise<Empty> {
    return this.rpc.unary(
      SessionClearDesc,
      ClearReq.fromPartial(request),
      metadata
    );
  }

  Check(
    request: DeepPartial<CheckReq>,
    metadata?: grpc.Metadata
  ): Promise<CheckResp> {
    return this.rpc.unary(
      SessionCheckDesc,
      CheckReq.fromPartial(request),
      metadata
    );
  }

  CheckPublic(
    request: DeepPartial<MethodReq>,
    metadata?: grpc.Metadata
  ): Promise<CheckResp> {
    return this.rpc.unary(
      SessionCheckPublicDesc,
      MethodReq.fromPartial(request),
      metadata
    );
  }

  IsPublic(
    request: DeepPartial<MethodReq>,
    metadata?: grpc.Metadata
  ): Promise<BoolValue> {
    return this.rpc.unary(
      SessionIsPublicDesc,
      MethodReq.fromPartial(request),
      metadata
    );
  }

  GetUserByToken(
    request: DeepPartial<StringRequest>,
    metadata?: grpc.Metadata
  ): Promise<ShortUserModel> {
    return this.rpc.unary(
      SessionGetUserByTokenDesc,
      StringRequest.fromPartial(request),
      metadata
    );
  }
}

export const SessionDesc = {
  serviceName: "authMs.Session",
};

export const SessionClearDesc: UnaryMethodDefinitionish = {
  methodName: "Clear",
  service: SessionDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return ClearReq.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...Empty.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const SessionCheckDesc: UnaryMethodDefinitionish = {
  methodName: "Check",
  service: SessionDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return CheckReq.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...CheckResp.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const SessionCheckPublicDesc: UnaryMethodDefinitionish = {
  methodName: "CheckPublic",
  service: SessionDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return MethodReq.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...CheckResp.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const SessionIsPublicDesc: UnaryMethodDefinitionish = {
  methodName: "IsPublic",
  service: SessionDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return MethodReq.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...BoolValue.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const SessionGetUserByTokenDesc: UnaryMethodDefinitionish = {
  methodName: "GetUserByToken",
  service: SessionDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return StringRequest.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...ShortUserModel.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

interface UnaryMethodDefinitionishR
  extends grpc.UnaryMethodDefinition<any, any> {
  requestStream: any;
  responseStream: any;
}

type UnaryMethodDefinitionish = UnaryMethodDefinitionishR;

interface Rpc {
  unary<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    request: any,
    metadata: grpc.Metadata | undefined
  ): Promise<any>;
}

export class GrpcWebImpl {
  private host: string;
  private options: {
    transport?: grpc.TransportFactory;

    debug?: boolean;
    metadata?: grpc.Metadata;
  };

  constructor(
    host: string,
    options: {
      transport?: grpc.TransportFactory;

      debug?: boolean;
      metadata?: grpc.Metadata;
    }
  ) {
    this.host = host;
    this.options = options;
  }

  unary<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    _request: any,
    metadata: grpc.Metadata | undefined
  ): Promise<any> {
    const request = { ..._request, ...methodDesc.requestType };
    const maybeCombinedMetadata =
      metadata && this.options.metadata
        ? new BrowserHeaders({
            ...this.options?.metadata.headersMap,
            ...metadata?.headersMap,
          })
        : metadata || this.options.metadata;
    return new Promise((resolve, reject) => {
      grpc.unary(methodDesc, {
        request,
        host: this.host,
        metadata: maybeCombinedMetadata,
        transport: this.options.transport,
        debug: this.options.debug,
        onEnd: function (response) {
          if (response.status === grpc.Code.OK) {
            resolve(response.message);
          } else {
            const err = new Error(response.statusMessage) as any;
            err.code = response.status;
            err.metadata = response.trailers;
            reject(err);
          }
        },
      });
    });
  }
}

type Builtin =
  | Date
  | Function
  | Uint8Array
  | string
  | number
  | boolean
  | undefined;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? { [K in Exclude<keyof T, "$type">]?: DeepPartial<T[K]> }
  : Partial<T>;

if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
