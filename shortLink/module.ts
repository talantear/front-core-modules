import { AsyncContainerModule, interfaces } from 'inversify';
import { ShortLinkService } from '@modules/shortLink/service';
import ShortLinkUsecase from '@modules/shortLink/usecase';

export const ShortLinkModule = new AsyncContainerModule(async (bind: interfaces.Bind, unbind: interfaces.Unbind) => {
  bind<ShortLinkService>(ShortLinkService.diKey).to(ShortLinkService)
  bind<ShortLinkUsecase>(ShortLinkUsecase.diKey).to(ShortLinkUsecase).inSingletonScope();
});