export interface IRecoveryReq {
  email: string;
  app: string;
}

export interface IChangePassReq {
  token: string;
  pass: string;
  app: string;
}