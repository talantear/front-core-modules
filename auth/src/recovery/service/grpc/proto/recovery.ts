/* eslint-disable */
import { messageTypeRegistry } from "./typeRegistry";
import Long from "long";
import { grpc } from "@improbable-eng/grpc-web";
import _m0 from "protobufjs/minimal";
import { StringRequest } from "./commonTypes";
import { Empty } from "./google/protobuf/empty";
import { BrowserHeaders } from "browser-headers";

export const protobufPackage = "authMs";

export interface RecoveryReq {
  $type: "authMs.RecoveryReq";
  email: string;
  app: string;
}

export interface ChangePassReq {
  $type: "authMs.ChangePassReq";
  token: string;
  pass: string;
  app: string;
}

const baseRecoveryReq: object = {
  $type: "authMs.RecoveryReq",
  email: "",
  app: "",
};

export const RecoveryReq = {
  $type: "authMs.RecoveryReq" as const,

  encode(
    message: RecoveryReq,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.email !== "") {
      writer.uint32(10).string(message.email);
    }
    if (message.app !== "") {
      writer.uint32(18).string(message.app);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): RecoveryReq {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseRecoveryReq } as RecoveryReq;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.email = reader.string();
          break;
        case 2:
          message.app = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): RecoveryReq {
    const message = { ...baseRecoveryReq } as RecoveryReq;
    message.email =
      object.email !== undefined && object.email !== null
        ? String(object.email)
        : "";
    message.app =
      object.app !== undefined && object.app !== null ? String(object.app) : "";
    return message;
  },

  toJSON(message: RecoveryReq): unknown {
    const obj: any = {};
    message.email !== undefined && (obj.email = message.email);
    message.app !== undefined && (obj.app = message.app);
    return obj;
  },

  fromPartial(object: DeepPartial<RecoveryReq>): RecoveryReq {
    const message = { ...baseRecoveryReq } as RecoveryReq;
    message.email = object.email ?? "";
    message.app = object.app ?? "";
    return message;
  },
};

messageTypeRegistry.set(RecoveryReq.$type, RecoveryReq);

const baseChangePassReq: object = {
  $type: "authMs.ChangePassReq",
  token: "",
  pass: "",
  app: "",
};

export const ChangePassReq = {
  $type: "authMs.ChangePassReq" as const,

  encode(
    message: ChangePassReq,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.token !== "") {
      writer.uint32(10).string(message.token);
    }
    if (message.pass !== "") {
      writer.uint32(18).string(message.pass);
    }
    if (message.app !== "") {
      writer.uint32(26).string(message.app);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ChangePassReq {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseChangePassReq } as ChangePassReq;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.token = reader.string();
          break;
        case 2:
          message.pass = reader.string();
          break;
        case 3:
          message.app = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ChangePassReq {
    const message = { ...baseChangePassReq } as ChangePassReq;
    message.token =
      object.token !== undefined && object.token !== null
        ? String(object.token)
        : "";
    message.pass =
      object.pass !== undefined && object.pass !== null
        ? String(object.pass)
        : "";
    message.app =
      object.app !== undefined && object.app !== null ? String(object.app) : "";
    return message;
  },

  toJSON(message: ChangePassReq): unknown {
    const obj: any = {};
    message.token !== undefined && (obj.token = message.token);
    message.pass !== undefined && (obj.pass = message.pass);
    message.app !== undefined && (obj.app = message.app);
    return obj;
  },

  fromPartial(object: DeepPartial<ChangePassReq>): ChangePassReq {
    const message = { ...baseChangePassReq } as ChangePassReq;
    message.token = object.token ?? "";
    message.pass = object.pass ?? "";
    message.app = object.app ?? "";
    return message;
  },
};

messageTypeRegistry.set(ChangePassReq.$type, ChangePassReq);

export interface RecoveryPassword {
  Recovery(
    request: DeepPartial<RecoveryReq>,
    metadata?: grpc.Metadata
  ): Promise<Empty>;
  CheckToken(
    request: DeepPartial<StringRequest>,
    metadata?: grpc.Metadata
  ): Promise<Empty>;
  ChangePass(
    request: DeepPartial<ChangePassReq>,
    metadata?: grpc.Metadata
  ): Promise<Empty>;
}

export class RecoveryPasswordClientImpl implements RecoveryPassword {
  private readonly rpc: Rpc;

  constructor(rpc: Rpc) {
    this.rpc = rpc;
    this.Recovery = this.Recovery.bind(this);
    this.CheckToken = this.CheckToken.bind(this);
    this.ChangePass = this.ChangePass.bind(this);
  }

  Recovery(
    request: DeepPartial<RecoveryReq>,
    metadata?: grpc.Metadata
  ): Promise<Empty> {
    return this.rpc.unary(
      RecoveryPasswordRecoveryDesc,
      RecoveryReq.fromPartial(request),
      metadata
    );
  }

  CheckToken(
    request: DeepPartial<StringRequest>,
    metadata?: grpc.Metadata
  ): Promise<Empty> {
    return this.rpc.unary(
      RecoveryPasswordCheckTokenDesc,
      StringRequest.fromPartial(request),
      metadata
    );
  }

  ChangePass(
    request: DeepPartial<ChangePassReq>,
    metadata?: grpc.Metadata
  ): Promise<Empty> {
    return this.rpc.unary(
      RecoveryPasswordChangePassDesc,
      ChangePassReq.fromPartial(request),
      metadata
    );
  }
}

export const RecoveryPasswordDesc = {
  serviceName: "authMs.RecoveryPassword",
};

export const RecoveryPasswordRecoveryDesc: UnaryMethodDefinitionish = {
  methodName: "Recovery",
  service: RecoveryPasswordDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return RecoveryReq.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...Empty.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const RecoveryPasswordCheckTokenDesc: UnaryMethodDefinitionish = {
  methodName: "CheckToken",
  service: RecoveryPasswordDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return StringRequest.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...Empty.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const RecoveryPasswordChangePassDesc: UnaryMethodDefinitionish = {
  methodName: "ChangePass",
  service: RecoveryPasswordDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return ChangePassReq.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...Empty.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

interface UnaryMethodDefinitionishR
  extends grpc.UnaryMethodDefinition<any, any> {
  requestStream: any;
  responseStream: any;
}

type UnaryMethodDefinitionish = UnaryMethodDefinitionishR;

interface Rpc {
  unary<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    request: any,
    metadata: grpc.Metadata | undefined
  ): Promise<any>;
}

export class GrpcWebImpl {
  private host: string;
  private options: {
    transport?: grpc.TransportFactory;

    debug?: boolean;
    metadata?: grpc.Metadata;
  };

  constructor(
    host: string,
    options: {
      transport?: grpc.TransportFactory;

      debug?: boolean;
      metadata?: grpc.Metadata;
    }
  ) {
    this.host = host;
    this.options = options;
  }

  unary<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    _request: any,
    metadata: grpc.Metadata | undefined
  ): Promise<any> {
    const request = { ..._request, ...methodDesc.requestType };
    const maybeCombinedMetadata =
      metadata && this.options.metadata
        ? new BrowserHeaders({
            ...this.options?.metadata.headersMap,
            ...metadata?.headersMap,
          })
        : metadata || this.options.metadata;
    return new Promise((resolve, reject) => {
      grpc.unary(methodDesc, {
        request,
        host: this.host,
        metadata: maybeCombinedMetadata,
        transport: this.options.transport,
        debug: this.options.debug,
        onEnd: function (response) {
          if (response.status === grpc.Code.OK) {
            resolve(response.message);
          } else {
            const err = new Error(response.statusMessage) as any;
            err.code = response.status;
            err.metadata = response.trailers;
            reject(err);
          }
        },
      });
    });
  }
}

type Builtin =
  | Date
  | Function
  | Uint8Array
  | string
  | number
  | boolean
  | undefined;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? { [K in Exclude<keyof T, "$type">]?: DeepPartial<T[K]> }
  : Partial<T>;

if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
