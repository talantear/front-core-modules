export const DirectionUp = -1;
export const DirectionDown = 1;

export type DirectionType = typeof DirectionUp | typeof DirectionDown ;