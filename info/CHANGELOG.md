# @shared/info

## 2.0.0

### Minor Changes

- Update packages

### Patch Changes

- Updated dependencies
  - @shared/http-client@1.1.0
  - @shared/gql-client@1.1.0
  - @shared/stream@1.1.0
  - @shared/image@1.1.0
  - @shared/types@1.1.0

## 1.0.2

### Patch Changes

- Updated exported modules
- Updated dependencies
  - @shared/stream@1.0.3
  - @shared/image@1.0.2
  - @shared/types@1.0.2

## 1.0.1

### Patch Changes

- Updaated exports
- Updated dependencies
  - @shared/http-client@1.0.1
  - @shared/gql-client@1.0.1
  - @shared/stream@1.0.1
  - @shared/image@1.0.1
  - @shared/types@1.0.1
