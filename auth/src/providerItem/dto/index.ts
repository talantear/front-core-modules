import { IProvider } from '../../provider/dto';

export interface IProviderItem {
  id: number;
  canSignUp: boolean;
  canSignIn: boolean;
  canUnlink: boolean;
  prompted: boolean;
  alertType: string;
  providerId: number;
  provider?: IProvider;
  applicationId: number;
}